 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
 	<title>Spoto</title>
 	
 	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/styles/css/themes/lite-purple.min.css')}}">
 </head>
 <body>
 	<div class="auth-layout-wrap" style="background-image: url({{asset('assets/images/photo-wide-4.jpg')}})">
	 	<div class="container" style="height: auto;">
			 <div class="row" style="margin-top:-23%;">
			  <div class="col-sm-2"></div>
			      <div class="col-sm-8">
			          <div class="card card-login card-hidden mb-3">
			            <div class="card-header card-header-primary text-center">
			              <p class="card-title"><strong>{{ __('Verify Your Email Address') }}</strong></p>
			            </div>
			            <div class="card-body">
			              <p class="card-description text-center"></p>
			              
			              	
			                @if (session('resent'))
			                    <div class="alert alert-success" role="alert">
			                        {{ __('A fresh verification link has been sent to your email address.') }}
			                    </div>
			                @endif
			                
			                {{ __('Before proceeding, please check your email for a verification link.') }}
			                
			                @if (Route::has('verification.resend'))
			                    {{ __('If you did not receive the email ') }}  
			                    <form class="d-inline" method="GET" action="{{ route('verification.resend') }}">
			                        @csrf
			                        
			                        <button type="submit" class="btn btn-primary">{{ __('Re-send') }}</button>
			                    </form>
			                @endif
			              </p>
			            </div>
			          </div>
			      </div>
			  <div class="col-sm-2"></div>
			  </div>
			</div>
	</div>

 	
 	<script src="{{asset('assets/js/common-bundle-script.js')}}"></script>

    <script src="{{asset('assets/js/script.js')}}"></script>
 </body>
 </html>