<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Spoto</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/styles/css/themes/lite-purple.min.css')}}">
</head>

<body>
    <div class="auth-layout-wrap" style="background-image: url({{asset('assets/images/photo-wide-4.jpg')}})">
        <div class="auth-content">
            <div class="card o-hidden">
                   <div class="row">
                    <div class="col-md-6">
                        <div class="p-4">
                            <div class="auth-logo text-center mb-4">
                                <img src="{{asset('assets/images/logo.png')}}" alt="">
                            </div>
                             
                           <!--  <h1 class="mb-3 text-18">Forgot Password</h1> -->
                          <!--   <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                @if (session('status'))
                              <div class="row">
                                <div class="col-sm-12">
                                  <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                      <i class="nav-icon i-Close-Window"></i>
                                    </button>
                                    <span>{{ session('status') }}</span>
                                  </div>
                                </div>
                              </div>
                            @endif
                                <div class="form-group">
                                    <label for="email">Email address</label>
                                    <input id="email" class="form-control form-control-rounded" type="email">
                                </div>
                                <button class="btn btn-primary btn-block btn-rounded mt-3">Reset Password</button>

                            </form> -->

                            <form class="form" method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="card card-login card-hidden mb-3">
                              <div class="card-header card-header-primary text-center">
                                <h4 class="card-title"><strong>{{ __('Forgot Password') }}</strong></h4>
                              </div>
                              <div class="card-body">
                                @if (session('status'))
                                  <div class="row">
                                    <div class="col-sm-12">
                                      <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <i class="nav-icon i-Close-Window"></i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                      </div>
                                    </div>
                                  </div>
                                @endif
                                <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text">
                                        <i class="i-Mail"></i>
                                      </span>
                                    </div>
                                    <input type="email" name="email" class="form-control" placeholder="{{ __('Email...') }}" value="{{ old('email') }}" required>
                                  </div>
                                  @if ($errors->has('email'))
                                    <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                                      <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                  @endif
                                </div>
                              </div>
                              <div class="card-footer justify-content-center">
                                <button class="btn btn-primary btn-block btn-rounded mt-3">Send Password Reset Link</button>
                              </div>
                            </div>
                          </form>
                            <div class="mt-3 text-center">
                                <a class="text-muted" href="/login"><u>Sign in</u></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 text-center " style="background-size: cover;background-image: url({{asset('assets/images/photo-long-3.jpg')}}">
                        <div class="pr-3 auth-right">
                            <a class="btn btn-outline-primary btn-outline-email btn-block btn-icon-text btn-rounded" href="{{ route('register') }}">
                                <i class="i-Mail-with-At-Sign"></i> Sign up with Email
                            </a>
                            <a class="btn btn-outline-primary btn-outline-email btn-block btn-icon-text btn-rounded" href="/seller">
                                <i class="i-Mail-with-At-Sign"></i> Sign up with Seller account 
                            </a>
                            <!-- <a class="btn btn-outline-primary btn-outline-email btn-block btn-icon-text btn-rounded" href="/login">
                                <i class="i-Mail-with-At-Sign"></i> Sign In
                            </a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('assets/js/common-bundle-script.js')}}"></script>

    <script src="{{asset('assets/js/script.js')}}"></script>
</body>

</html>
