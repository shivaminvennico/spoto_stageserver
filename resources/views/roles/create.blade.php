@extends('layouts.master')
@section('main-content')
	<div class="breadcrumb">
        <h1>Roles</h1>
        <ul>
            <li><a href=""></a></li>
            
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">

                <div class="col-md-8">
  
                    <div class="card mb-5">

                    	
                        <div class="card-body">
                            <form method="post" action="{{ route('role.store') }}" autocomplete="off" class="form-horizontal">
                    @csrf
                    @method('post')

                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ __('Create New Role') }}</h4>
                            <p class="card-category"></p>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="{{ route('role.index') }}"
                                        class="btn btn-sm btn-primary">{{ __('BACK TO LIST') }}</a>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Name') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                            name="name" id="input-name" type="text" placeholder="{{ __('Name') }}"
                                            value="{{ old('name') }}" required="true" aria-required="true" />
                                        @if ($errors->has('name'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-name">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Permission') }}</label>
                                <ul style=" columns: 3 12em;">
                                    @foreach($permission as $value)
                                    <li><input type="checkbox" name="permission[]" value="{{ $value->id }}" /><label
                                            for="permission">{{ $value->name }}
                                        </label></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">{{ __('Add Role') }}</button>
                        </div>
                    </div>
                </form>
                        </div>
                    </div>
                </div>
            </div>
    

@endsection


