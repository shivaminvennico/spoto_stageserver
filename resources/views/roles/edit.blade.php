{!! Form::model($role, ['method' => 'PATCH','route' => ['role.update', $role->id]]) !!}

    <div class="col-xs-12 col-sm-12 col-md-12">
                <strong>Permissions:</strong>
            <ul class="row">
                @foreach($permission as $value)
                <div style="float:left;width:33.33%;">
                <li class="mb-1"><label class="switch switch-primary mr-1">
                <span>{{ ucwords(str_replace('-',' ',$value->name)) }}</span>                   
               <input type="checkbox" name="permission[]" value="{{ $value->id }}" <?php if (in_array($value->id, $rolePermissions)) {?>checked="checked"<?php }?> /><span class="slider"></span></label></li>
               </div>
                @endforeach
            </ul>
        </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
{!! Form::close() !!}
                   
