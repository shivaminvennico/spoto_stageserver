@extends('layouts.master')
@section('main-content')
          <div class="breadcrumb">
                <h1>Role Permissions</h1>
          </div>

            <div class="separator-breadcrumb border-top"></div>

        @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="nav-icon i-Close-Window"></i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif

                <div class="col-md-12">
                    <div class="card text-left">

                        <div class="card-body">
                            <h4 class="card-title mb-3">Role Permissions</h4>
                            <p>Here you can manage Permissions</p>
                            <div class="row">
                              
                              <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('role') ? ' has-danger' : '' }}">
                                    {{ Form::select('role', $roles,null, array('required','class'=>'form-control', 'placeholder'=>'Select Role','id' => 'role','style'=>'-webkit-appearance: menulist;')) }}
                                    @if ($errors->has('role'))
                                    <span id="role-error" class="error text-danger" for="input-role">{{ $errors->first('role') }}</span>
                                    @endif
                                </div>
                              </div>

                              
                            </div>

                            <div id="loadPermissions"></div>
                     
            </div>
                

            </div>
          </div>
      
        

            <!-- end of row-->
@endsection