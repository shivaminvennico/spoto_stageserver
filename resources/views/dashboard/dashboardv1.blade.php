@extends('layouts.master')
@section('main-content')
           <div class="breadcrumb">
                <h1>Spoto</h1>
                <ul>
                    <li><a href="">Dashboard</a></li>
                    
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                <!-- ICON BG -->
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Add-User"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">New Leads</p>
                                <p class="text-primary text-24 line-height-1 mb-2">205</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Financial"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Sales</p>
                                <p class="text-primary text-24 line-height-1 mb-2">$4021</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Checkout-Basket"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Orders</p>
                                <p class="text-primary text-24 line-height-1 mb-2">80</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Money-2"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Expense</p>
                                <p class="text-primary text-24 line-height-1 mb-2">$1200</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title">This Year Sales</div>
                            <div id="echartBar" style="height: 300px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title">Sales by Countries</div>
                            <div id="echartPie" style="height: 300px;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-12">

                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="card card-chart-bottom o-hidden mb-4">
                                <div class="card-body">
                                    <div class="text-muted">Last Month Sales</div>
                                    <p class="mb-4 text-primary text-24">$40250</p>
                                </div>
                                <div id="echart1" style="height: 260px;"></div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <div class="card card-chart-bottom o-hidden mb-4">
                                <div class="card-body">
                                    <div class="text-muted">Last Week Sales</div>
                                    <p class="mb-4 text-warning text-24">$10250</p>
                                </div>
                                <div id="echart2" style="height: 260px;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="card o-hidden mb-4">
                                <div class="card-header d-flex align-items-center border-0">
                                    <h3 class="float-left card-title m-0">Category Wise Product Sales</h3>
                                    
                                </div>

                                <div class="">
                                    <div class="table-responsive">
                                        <table id="user_table" class="table  text-center">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Category Name</th>
                                                    <th scope="col">Sales</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Car Care</td>
                                                    <td>$900,000</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Electronics & Accessories</td>
                                                    <td>
                                                        $500,000
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Interior Accessories</td>
                                                    <td>
                                                        $400,000
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th scope="row">4</th>
                                                    <td>Light & Accessories</td>
                                                    <td>
                                                        $600,000
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>


                <div class="col-lg-6 col-md-12">

                 <div class="col-md-16 mb-4">
                    <div class="card">
                        <div class="card-body">
                    <div class="card-title mb-1">Products</div>
                    <div class="ul-widget-app__header mb-4">
                        <h5 class="text-mute">Figures</h5>
                        <!-- <h6 class="text-mute font-weight-600">Your mobile app daily usage</h6> -->
                    </div>

                    <div class="ul-widget-app__poll-list mb-4">
                        <div class="d-flex">
                            <h5 class="heading mr-2">My Published Products</h5>
                            <!-- <span class="text-muted">20 Minutes</span> -->
                            <span class="t-font-boldest ml-auto">102</span>
                        </div>
                        <div class="progress progress--height-2 mb-3">
                            <div class="progress-bar bg-danger" role="progressbar" style="width: 25%" aria-valuenow="25"
                                aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="ul-widget-app__poll-list mb-4">
                        <div class="d-flex">
                            <h5 class="heading mr-2">Total My Products</h5>
                            <!-- <span class="text-muted">Greater than 30 Minutes</span> -->
                            <span class="t-font-boldest ml-auto">50</span>
                        </div>
                        <div class="progress progress--height-2 mb-3">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 55%"
                                aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>

                    <div class="ul-widget-app__poll-list mb-4">
                        <div class="d-flex">
                            <h5 class="heading mr-2">Total Affiliate Products</h5>
                            <!-- <span class="text-muted">2 Hour</span> -->
                            <span class="t-font-boldest ml-auto">52</span>
                        </div>
                        <div class="progress progress--height-2 mb-3">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 73%"
                                aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>

                    
                </div>
                    </div>
                </div>

                   
                    <div class="card o-hidden mb-4">
                                <div class="card-header d-flex align-items-center border-0">
                                    <h3 class="float-left card-title m-0">Category Wise Product Stock</h3>
                                    
                                </div>

                                <div class="">
                                    <div class="table-responsive">
                                        <table id="user_table" class="table  text-center">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Category Name</th>
                                                    <th scope="col">Stock</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Car Care</td>
                                                    <td>500</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Electronics & Accessories</td>
                                                    <td>
                                                        300
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Interior Accessories</td>
                                                    <td>
                                                        400
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th scope="row">4</th>
                                                    <td>Light & Accessories</td>
                                                    <td>
                                                        100
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                </div>

                <div class="col-md-12">
                       <div class="col-md-16 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <!-- <h4 class="card-title mb-3">Basic Tab With Icon</h4> -->

                            <ul class="nav nav-tabs" id="myIconTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-icon-tab" data-toggle="tab" href="#homeIcon" role="tab" aria-controls="homeIcon" aria-selected="true"><i class="nav-icon i-Money-Bag mr-1"></i>Best Selling Products</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-icon-tab" data-toggle="tab" href="#profileIcon" role="tab" aria-controls="profileIcon" aria-selected="false"><i class="nav-icon i-Love-User mr-1"></i> Customer Acquisition</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-icon-tab" data-toggle="tab" href="#contactIcon" role="tab" aria-controls="contactIcon" aria-selected="false"><i class="nav-icon i-Line-Chart mr-1"></i> Sales Graph</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myIconTabContent">
                                <div class="tab-pane fade show active" id="homeIcon" role="tabpanel" aria-labelledby="home-icon-tab">
                                    <div class="table-responsive">
                                        <table id="user_table" class="table  text-center">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Product Name</th>
                                                    <th scope="col">Qty.Ordered</th>
                                                    <th scope="col">Total Profit</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Microfiber Wheel Brush with Drill</td>
                                                    <td>102</td>
                                                    <td>$900,000</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Microfiber Wheel Brush with Drill</td>
                                                    <td>69</td>
                                                    <td>
                                                        $500,000
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Microfiber Wheel Brush with Drill</td>
                                                    <td>72</td>
                                                    <td>
                                                        $400,000
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th scope="row">4</th>
                                                    <td>Microfiber Wheel Brush with Drill</td>
                                                    <td>45</td>
                                                    <td>
                                                        $600,000
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profileIcon" role="tabpanel" aria-labelledby="profile-icon-tab">
                                    Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore.
                                </div>
                                <div class="tab-pane fade" id="contactIcon" role="tabpanel" aria-labelledby="contact-icon-tab">
                                    Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore.</div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>

            </div>


@endsection

@section('page-js')
     <script src="{{asset('assets/js/vendor/echarts.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/echart.options.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/dashboard.v1.script.js')}}"></script>

@endsection
