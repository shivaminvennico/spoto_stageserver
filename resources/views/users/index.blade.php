@extends('layouts.master')
@section('main-content')
          <div class="breadcrumb">
                <h1>User Management</h1>
          </div>

            <div class="separator-breadcrumb border-top"></div>


				@if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="nav-icon i-Close-Window"></i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif

                <div class="col-md-12">
                    <div class="card text-left">

                        <div class="card-body">
                            <h4 class="card-title mb-3">Customer Corner</h4>
                            <p>Here you can manage customers</p>
                            <div class="table-responsive">
                            	@can('user-create')
                              <a class="btn btn-raised ripple btn-primary m-1 float-right" href="{{ route('user.create') }}">ADD USER</a>
                              @endcan
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">{{ __('No') }}</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Mobile</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Role</th>
                                            <th scope="col">State</th>
                                            @can('user-edit')
                                            <th scope="col">Action</th>
                                            @endcan
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php $counter=1; ?>
                                      @foreach($users as $user)
                                        <tr class="h5">
                                         
                                            <th scope="row">{{$counter++}}</th>
                                            <td>
                                            	{{ $user->first_name }}
                            					{{ $user->last_name }}
                            				</td>
                                            <td>
                                            	{{ $user->mobile_no }}
                                            </td>

                                            <td>{{ $user->email }}</td>
                                            <td>
                                            	@if(!empty($user->getRoleNames()))
                              						@foreach($user->getRoleNames() as $role)
                                						<label class="badge badge-success">{{ $role }}</label>
                              						@endforeach
                            					@endif
                            				</td>
                                    <td>{{ $user->state }}</td>
                                    @can('user-edit')
                                            <td>
                                                @if ($user->id != auth()->id() && auth()->id() ==1 && false)
                                                  <form action="{{ route('user.destroy', $user) }}" method="post">
                                                      @csrf
                                                      @method('delete')
                                                  
                                                      <a rel="tooltip" class="btn btn-success" href="{{ route('user.edit', $user) }}" data-original-title="" title="">
                                                        <i class="nav-icon i-Pen-2"></i>
                                                        <div class="ripple-container"></div>
                                                      </a>
                                                      <button type="button" class="btn btn-danger" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this user?") }}') ? this.parentElement.submit() : ''">
                                                          <i class="nav-icon i-Close-Window"></i>
                                                          <div class="ripple-container"></div>
                                                      </button>
                                                  </form>
                                                @else
                                                <a rel="tooltip" class="btn btn-success" href="{{ route('user.edit', $user) }}" data-original-title="" title="">
                                                        <i class="nav-icon i-Pen-2"></i>
                                                        <div class="ripple-container"></div>
                                                </a>
                                                @endif
                                            </td>
                                    @endcan
                                        </tr>
                                      @endforeach
                                       
                                    </tbody>
                                </table>
                                {{$users->links()}}
                            </div>


                        </div>
                    </div>
                </div>
                

         
            <!-- end of row-->
@endsection