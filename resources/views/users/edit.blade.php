@extends('layouts.master')
@section('main-content')
          <div class="breadcrumb">
                <h1>User Management</h1>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
            <div class="col-md-12">
                <form method="post" action="{{ route('user.update', $user) }}" autocomplete="off"
                    class="form-horizontal">
                    @method('PUT')
                    @csrf
                    

                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ __('Edit User') }}</h4>
                            <p class="card-category"></p>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="{{ route('user.index') }}"
                                        class="btn btn-sm btn-primary">{{ __('BACK TO LIST') }}</a>
                                </div>
                            </div>
                            <section class="col-md-6" style="float: left">
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{ __('First Name') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
                                            <input
                                                class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                                name="first_name" id="input-first_name" type="text"
                                                placeholder="{{ __('First Name') }}"
                                                value="{{ old('first_name', $user->first_name) }}" required="true"
                                                aria-required="true" />
                                            @if ($errors->has('first_name'))
                                            <span id="first_name-error" class="error text-danger"
                                                for="input-first_name">{{ $errors->first('first_name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{ __('Last Name') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
                                            <input
                                                class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                                name="last_name" id="input-last_name" type="text"
                                                placeholder="{{ __('Last Name') }}"
                                                value="{{ old('last_name', $user->last_name) }}" required="true"
                                                aria-required="true" />
                                            @if ($errors->has('last_name'))
                                            <span id="last_name-error" class="error text-danger"
                                                for="input-last_name">{{ $errors->first('last_name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{ __('Email') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                name="email" id="input-email" type="email"
                                                placeholder="{{ __('Email') }}" value="{{ old('email', $user->email) }}"
                                                required />
                                            @if ($errors->has('email'))
                                            <span id="email-error" class="error text-danger"
                                                for="input-email">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{ __('Account Created On') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('created_at') ? ' has-danger' : '' }}">
                                            <input
                                                class="form-control{{ $errors->has('created_at') ? ' is-invalid' : '' }}"
                                                name="date" id="input-created_at" 
                                                value="{{ old('created_at', $user->created_at->format('dS F Y H:i:A') ) }}"
                                                disabled />
                                                <input type="hidden" name="created_at" id="input-created_at" value="{{ old('created_at', $user->created_at ) }}" /> 
                                            @if ($errors->has('created_at'))
                                            <span id="created_at-error" class="error text-danger"
                                                for="input-created_at">{{ $errors->first('created_at')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{ __('User Type') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('getRoleNames') ? ' has-danger' : '' }}">
                                            @if(!empty($user->getRoleNames()))
                                            @foreach($user->getRoleNames() as $role)
                                            <label class="badge badge-success" style="color: white;">{{ $role }}</label>
                                            @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="col-md-6" style="float: right">
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{ __('Mobile No') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('mobile_no') ? ' has-danger' : '' }}">
                                            <input type="text"
                                                class="form-control{{ $errors->has('mobile_no') ? ' is-invalid' : '' }}"
                                                name="mobile_no" id="input-mobile_no" type="text"
                                                placeholder="{{ __('Mobile No') }}"
                                                value="{{ old('mobile_no', $user->mobile_no) }}" onKeyPress="return isNumberKey(event)" maxlength="10" required />
                                            @if ($errors->has('mobile_no'))
                                            <span id="mobile_no-error" class="error text-danger"
                                                for="input-mobile_no">{{ $errors->first('mobile_no') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{ __('Address') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('address_1') ? ' has-danger' : '' }}">
                                            <input
                                                class="form-control{{ $errors->has('address_1') ? ' is-invalid' : '' }}"
                                                name="address_1" id="input-address_1" type="text"
                                                placeholder="{{ __('Address') }}"
                                                value="{{ old('address_1', $user->address_1) }}" required />
                                            @if ($errors->has('address_1'))
                                            <span id="address_1-error" class="error text-danger"
                                                for="input-address_1">{{ $errors->first('address_1') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{ __('State') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('state') ? ' has-danger' : '' }}">
                                            <input
                                                class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}"
                                                name="state" id="input-state" type="text"
                                                placeholder="{{ __('state') }}"
                                                value="{{ old('state', $user->state) }}" required />
                                            @if ($errors->has('state'))
                                            <span id="state-error" class="error text-danger"
                                                for="input-state">{{ $errors->first('state') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @if($user->user_type <=4 )
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{ __('Credit Limit Given') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('credit_limit') ? ' has-danger' : '' }}">
                                            <input 
                                                class="form-control{{ $errors->has('credit_limit') ? ' is-invalid' : '' }}"
                                                name="credit_limit" id="input-credit_limit" type="text"
                                                placeholder="{{ __('Credit Limit Given') }}"
                                                value="{{ old('credit_limit', $user->credit_limit) }}" required />
                                            @if ($errors->has('credit_limit'))
                                            <span id="credit_limit-error" class="error text-danger"
                                                for="input-credit_limit">{{ $errors->first('credit_limit') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </section>
                        </div>
                        <div class="card-footer" align="center">
                            <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    @if($user->user_type == 5)
        
            <div class="row mb-4">
                <div class="col-md-12">
                    <form method="post" action="{{ route('user.update', $user) }}" autocomplete="off"
                        class="form-horizontal">
                        @csrf
                        @method('put')

                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Order  Statistics') }}</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body ">
                                <div class="row">

                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">
                                            <th>
                                                {{ __('Order  ID') }}
                                            </th>
                                            <th>
                                                {{ __('Purchase Date ') }}
                                            </th>
                                            <th>
                                                {{ __('Order Value') }}
                                            </th>
                                            <th>
                                                {{ __('Shipment Status ') }}
                                            </th>
                                        </thead>
                                        <tbody>

                                            <tr align="center" colspan="4">
                                                <td colspan="4"> No Record </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                    </form>
                </div>
            </div>
        
    @endif

    @if($user->user_type <= 4)
        
            <div class="row mb-4">
                <div class="col-md-12">
                    <form method="post" action="{{ route('user.update', $user) }}" autocomplete="off"
                        class="form-horizontal">
                        @csrf
                        @method('put')

                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Sales Statistics') }}</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body ">
                                <div class="row">

                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">
                                            <th>
                                                {{ __('Category') }}
                                            </th>
                                            <th>
                                                {{ __('Total Sales') }}
                                            </th>
                                            <th>
                                                {{ __('Total Sales Value') }}
                                            </th>
                                            <th>
                                                {{ __('Avg Sales Value ') }}
                                            </th>
                                        </thead>
                                        <tbody>

                                            <tr align="center" colspan="4">
                                                <td colspan="4"> No Record </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                    </form>
                </div>
            </div>
       
    @endif

@endsection