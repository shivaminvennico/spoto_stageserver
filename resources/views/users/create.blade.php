@extends('layouts.master')
@section('main-content')
	<div class="breadcrumb">
        <h1>User Management</h1>
        <ul>
            <li><a href=""></a></li>
            
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">

                <div class="col-md-8">
  
                    <div class="card mb-5">

                    	<div class="m-2">
                    		<h4 class="m-1">Add User</h4>
                    		<a class="btn btn-raised ripple btn-primary  float-right btn-sm" href="{{ route('user.index') }}">BACK TO LIST</a>
                    	</div>
                        <div class="card-body">

                            <form action="{{route('user.store')}}" method="post">
                            	@csrf
                            	@method('post')
                            	<div class="form-group row">
                                    <label for="first_name" class="col-sm-2 col-form-label">First Name</label>
                                    <div class="col-sm-10">
                                        
                                        <div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                            name="first_name" id="input-first_name" type="text" placeholder="{{ __('First Name') }}"
                                            value="{{ old('first_name') }}" required="true" aria-required="true" />
                                        @if ($errors->has('first_name'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-first_name">{{ $errors->first('first_name') }}</span>
                                        @endif
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="last_name" class="col-sm-2 col-form-label">Last Name</label>
                                    <div class="col-sm-10">
                                        <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                            name="last_name" id="input-last_name" type="text" placeholder="{{ __('Last Name') }}"
                                            value="{{ old('last_name') }}" required="true" aria-required="true" />
                                        @if ($errors->has('last_name'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-last_name">{{ $errors->first('last_name') }}</span>
                                        @endif
                                    	</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                            name="email" id="input-email" type="email" placeholder="{{ __('Email') }}"
                                            value="{{ old('email') }}" required />
                                        @if ($errors->has('email'))
                                        <span id="email-error" class="error text-danger"
                                            for="input-email">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="user_type" class="col-sm-2 col-form-label">User Type</label>
                                    <div class="col-sm-10">
                                    	 <div class="form-group{{ $errors->has('user_type') ? ' has-danger' : '' }}">
                                        <select style="-webkit-appearance: menulist;" name="user_type" id="user_type" class="form-control dropdown-toggle">
                                        	<option value="1">Manufacturer</option>
                                        	<option value="2">Importer</option>
                                        	<option value="3">Distributer</option> 
                                        	<option value="4">EndUser</option>
                                        </select>
                                        @if ($errors->has('user_type'))
                                        <div id="user_type" class="error text-danger pl-3" for="user_type"
                                            style="display: block;">
                                            <span id="name-error" class="error text-danger"
                                                for="input-name">{{ $errors->first('user_type') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                        
                                  
                                </div>
                                <div class="form-group row">
                                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                            <input
                                                class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                input type="password" name="password" id="input-password"
                                                placeholder="{{ __('Password') }}" value="" required />
                                            @if ($errors->has('password'))
                                            <span id="name-error" class="error text-danger"
                                                for="input-name">{{ $errors->first('password') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="confirm_password" class="col-sm-2 col-form-label">Confirm Password</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <input class="form-control" name="password_confirmation"
                                                id="input-password-confirmation" type="password"
                                                placeholder="{{ __('Confirm Password') }}" value="" required />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">ADD USER</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    

@endsection


