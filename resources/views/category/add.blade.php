@extends('layouts.master')
@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/toastr.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
@endsection
@section('main-content')
          <div class="breadcrumb">
                <h1>Add Category</h1>
          </div>

            <div class="separator-breadcrumb border-top"></div>

      
        @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="nav-icon i-Close-Window"></i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif

      <div class="row">
        <div class="col-md-4">
          <form method="post" action="{{ route('store_category') }}" autocomplete="off" class="form-horizontal">
            @csrf

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Add Category') }}</h4>
              </div>
              <div class="card-body ">
                <div class="row">
                  <label class="col-sm-3 col-form-label">{{ __('Name') }}</label>
                  <div class="col-sm-9">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Parent Category Name') }}" value="{{ old('name', '') }}" required="true" aria-required="true"/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer text-center">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <form method="post" action="{{ route('store_sub_category') }}" autocomplete="off" class="form-horizontal">
            @csrf

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Add Sub Category') }}</h4>
              </div>
              <div class="card-body ">
                <div class="row">
                  <label class="col-sm-3 col-form-label">{{ __('Parent') }}</label>
                  <div class="col-sm-9">
                    <div class="form-group{{ $errors->has('category_id') ? ' has-danger' : '' }}">
                    {{ Form::select('category_id', $ParentCategories, null, array('required','class'=>'form-control', 'placeholder'=>'Select Parent Category','style'=>'-webkit-appearance: menulist;')) }}
                      @if ($errors->has('category_id'))
                        <span id="category_id-error" class="error text-danger" for="input-category_id">{{ $errors->first('category_id') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <!-- <label class="col-sm-4 col-form-label">{{ __('Sub Category') }}</label> -->
                  <div class="col-sm-12">
                    <div class="form-group{{ $errors->has('sub_name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('sub_name') ? ' is-invalid' : '' }}" name="sub_name" type="text" placeholder="{{ __('Sub Category Name') }}" value="{{ old('sub_name', '') }}" required="true" aria-required="true"/>
                      @if ($errors->has('sub_name'))
                        <span id="sub_name-error" class="error text-danger" for="input-sub_name">{{ $errors->first('sub_name') }}</span>
                      @endif
                      @if(session()->has('sub_name'))
                        <span id="sub_name-error" class="error text-danger" for="input-sub_name">{{ session()->get('sub_name') }}</span> 
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer text-center">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <form method="post" action="{{ route('store_sub_sub_category') }}" autocomplete="off" class="form-horizontal">
            @csrf

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Add Sub Sub Category') }}</h4>
              </div>
              <div class="card-body ">
                <div class="row">
                  <label class="col-sm-5 col-form-label">{{ __('Sub Category') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('sub_category_id') ? ' has-danger' : '' }}">
                    {{ Form::select('sub_category_id', $SubCategories, null, array('required','class'=>'form-control', 'placeholder'=>'Select Sub Category','style'=>'-webkit-appearance: menulist;')) }}
                      @if ($errors->has('sub_category_id'))
                        <span id="sub_category_id-error" class="error text-danger" for="input-sub_category_id">{{ $errors->first('sub_category_id') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group{{ $errors->has('sub_sub_name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('sub_sub_name') ? ' is-invalid' : '' }}" name="sub_sub_name" type="text" placeholder="{{ __('Sub Sub Category Name') }}" value="{{ old('sub_sub_name', '') }}" required="true" aria-required="true"/>
                      @if ($errors->has('sub_sub_name'))
                        <span id="sub_sub_name-error" class="error text-danger" for="input-sub_sub_name">{{ $errors->first('sub_sub_name') }}</span>
                      @endif
                      @if(session()->has('sub_sub_name'))
                        <span id="sub_sub_name-error" class="error text-danger" for="input-sub_sub_name">{{ session()->get('sub_sub_name') }}</span> 
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer text-center">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>


     <div class="separator-breadcrumb "></div>


      <div class="row">
        <div class="col-md-4">
        <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Parent Categories') }}</h4>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-12">
                    <ul id="parent_categories">
                      @foreach($ViewCategories as $parent)
                        <li id="{{$parent->id}}" class="parent-category">{{$parent->name}}<i></i></li>
                        <div  class="input-category" id="div{{$parent->id}}"  style="display:none;">
                        <div class="row">
                          <input type='text' id="input{{$parent->id}}" value='{{$parent->name}}' style="width:120px;" class="form-control"/>
                          <button onclick="EditCategory('{{$parent->id}}');" class="btn btn-sm btn-primary ladda-button example-button m-1 btnSubmitCat" data-style="expand-left">
                            <span class="ladda-label">Submit</span>
                          </button>
                        </div>
                        </div>
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            </div>
        </div>

        <div class="col-md-4">
        <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Sub Categories') }}</h4>
              </div>
              <div class="card-body">
                <div id="loadSub" class="row">
                  
                </div>
              </div>
            </div>
        </div>

        <div class="col-md-4">
        <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Sub Sub Categories') }}</h4>
              </div>
              <div class="card-body">
                <div id="loadSubSub" class="row">
                  
                </div>
              </div>
            </div>
        </div>
      </div>
@endsection
@section('page-js')
<script src="{{asset('assets/js/vendor/toastr.min.js')}}"></script>
<script src="{{asset('assets/js/toastr.script.js')}}"></script>
<script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
<script src="{{asset('assets/js/ladda.script.js')}}"></script>
@endsection