<div class="col-sm-12">
<ul id="sub_categories">
@if(count($SubSubCategories) > 0)
  @foreach($SubSubCategories as $parent)
    <li id="sub-sub{{$parent->id}}" class="sub-sub-category">{{$parent->sub_sub_name}}<i></i></li>
    <div  class="input-sub-sub-category" id="div-sub-sub{{$parent->id}}"  style="display:none;">
    <div class="row">
      <input type='text' id="input-sub-sub{{$parent->id}}" value='{{$parent->sub_sub_name}}'  style="width:120px;" class="form-control"/>
      <button onclick="EditSubSubCategory('{{$parent->id}}');" class="btn btn-sm btn-primary ladda-button example-button m-1" data-style="expand-left">
        <span class="ladda-label">Submit</span>
      </button>
    </div>
</div>
  @endforeach
@else
    <li>No Sub Sub Categories Linked</li>
@endif
</ul>
</div>