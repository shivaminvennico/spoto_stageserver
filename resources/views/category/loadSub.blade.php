<div class="col-sm-12">
<ul id="sub_categories" >
@if(count($SubCategories) > 0)
  @foreach($SubCategories as $parent)
    <li id="sub{{$parent->id}}" data-id="{{$parent->id}}" class="sub-category">{{$parent->sub_name}}<i></i></li>
    <div  class="input-sub-category" id="div-sub{{$parent->id}}"  style="display:none;">
    <div class="row">
      <input type='text' id="input-sub{{$parent->id}}" value='{{$parent->sub_name}}'  style="width:120px;" class="form-control"/>
      <button onclick="EditSubCategory('{{$parent->id}}');" class="btn btn-sm btn-primary ladda-button example-button m-1" data-style="expand-left">
        <span class="ladda-label">Submit</span>
      </button>
    </div>
</div>
  @endforeach
@else
    <li>No Sub Categories Linked</li>
@endif
</ul>
</div>

