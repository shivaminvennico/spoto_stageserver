@extends('layouts.master')
@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/toastr.css')}}">
@endsection
@section('main-content')
<div class="breadcrumb">
                <h1>Create Connection</h1>
          </div>

            <div class="separator-breadcrumb border-top"></div>

@if(session('status'))

    <div class="row">
        <div class="col-sm-12">
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="nav-icon i-Close-Window"></i>
            </button>
            <span>{{ session('status') }}</span>
        </div>
        </div>
    </div>
@endif
    
    <div class="content">
        <div class="container-fluid">
        <div id="loadingScreen">

            <div class="">


            </div>
        </div>
        <div class="row">
        <div class="col-md-4">
          
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Requester') }}</h4>
              </div>
              <div class="card-body ">
                    <div class="row">
                        
                        <div class="col-sm-12">
                        <div class="form-group{{ $errors->has('requester_partner_type') ? ' has-danger' : '' }}">
                            {{ Form::select('requester_partner_type', $partner_type,null, array('required','class'=>'form-control', 'placeholder'=>'Select Partner Type','id' => 'requester_partner_type','style'=>'-webkit-appearance: menulist;')) }}
                            @if ($errors->has('requester_partner_type'))
                            <span id="requester_partner_type-error" class="error text-danger" for="input-requester_partner_type">{{ $errors->first('requester_partner_type') }}</span>
                            @endif
                        </div>
                        </div>
                        
                    </div>

                    <div class="row">
                        
                        <div id="loadRequestPartnerName" class="col-sm-12">
                        </div>
                        
                    </div>
               
              </div>
              

            </div>
        </div>
    
        <div class="col-md-4">
          
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Receiver') }}</h4>
              </div>
                <div class="card-body ">
                        <div class="row">
                            
                            <div class="col-sm-12">
                            <div class="form-group{{ $errors->has('receiver_partner_type') ? ' has-danger' : '' }}">
                                {{ Form::select('receiver_partner_type', $partner_type,null, array('required','class'=>'form-control', 'placeholder'=>'Select Partner Type','id' => 'receiver_partner_type','style'=>'-webkit-appearance: menulist;')) }}
                                @if ($errors->has('receiver_partner_type'))
                                <span id="receiver_partner_type-error" class="error text-danger" for="input-receiver_partner_type">{{ $errors->first('receiver_partner_type') }}</span>
                                @endif
                            </div>
                            </div>
                            
                        </div>

                        <div class="row">
                        
                            <div id="loadReceiverPartnerName" class="col-sm-12">
                            </div>
                        
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <button type="button" class="btn btn-primary" id="establishConnectionBtn">Make Connection</button>
                        </div>
                    
                </div>
              
          
                </div>
             </div>
        </div>
        </div>
    </div>

    
    <div class="separator-breadcrumb"></div>
        
                <div class="col-md-16">
                    <div class="card text-left">

                        <div class="card-body">
                            <h4 class="card-title mb-3">Connections</h4>
                            <p>Here you view affiliate connections</p>
                            <div class="table-responsive">
                            	
                            	
                                <table class="table table-hover h5">
                                    <thead>
                                        <tr>
                                            
                                            <th scope="col">{{ __('No') }}</th>
                                            <th scope="col">Requester</th>
                                            <th scope="col">Receiver</th>
                                            <th scope="col">Status</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $counter=1; ?>
                                        @foreach($affiliateConnection as $connection)
                                            <tr>
                                                <th scope="row">{{$counter++}}</th>
                                                <td>
                                                    {{ $connection->req_name }}
                                                    
                                                </td>
                                                <td>
                                                    {{ $connection->rec_name }}
                                                    
                                                </td>
                                                <td>
                                                    @if($connection->status == 0 )
                                                    <label class="badge badge-info">Connection Established</label>
                                                    @elseif($connection->status == 1)
                                                    <label class="badge badge-success">Accepted</label>
                                                    @elseif($connection->status == 4)
                                                    <label class="badge badge-info">Access Given</label>
                                                    
                                                    @else
                                                    <label class="badge badge-danger">Rejected</label>
                                                    @endif                                                  
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{$affiliateConnection->links()}}

                            </div>


                        </div>

                        
                    </div>
                </div>

   

@endsection
@section('page-js')

<script src="{{asset('assets/js/vendor/toastr.min.js')}}"></script>
<script src="{{asset('assets/js/toastr.script.js')}}"></script>

@endsection