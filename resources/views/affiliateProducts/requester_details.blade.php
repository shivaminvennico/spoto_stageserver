@extends('layouts.master')

@section('main-content')
<div class="col-md-12 mb-4">
    <div class="card text-left">

        <div class="card-body">
            <h4 class="card-title mb-3">Requester Details</h4>
            <div class="col-md-12 text-right">
                <a href="{{route('affiliate.request')}}"
                    class="btn btn-sm btn-primary">{{ __('BACK TO LIST') }}</a>
            </div>
            
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-basic-tab" data-toggle="tab" href="#homeBasic" role="tab" aria-controls="homeBasic" aria-selected="true">Home</a>
                </li>
                @if(count($products)>0)
                <li class="nav-item">
                    <a class="nav-link" id="product-basic-tab" data-toggle="tab" href="#productBasic" role="tab" aria-controls="productBasic" aria-selected="false">Products</a>
                </li>
                @endif
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="homeBasic" role="tabpanel" aria-labelledby="home-basic-tab">
                    <div class="row">
                        <label class="col-sm-3 col-form-label">{{ __('First Name') }}</label>
                        <div class="col-sm-7">
                            <div class="form-group">
                            <label class="col-form-label">{{$user->first_name}}</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3 col-form-label">{{ __('Last Name') }}</label>
                        <div class="col-sm-7">
                            <div class="form-group">
                            <label class="col-form-label">{{$user->last_name}}</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3 col-form-label">{{ __('User Type') }}</label>
                        <div class="col-sm-7">
                            <div class="form-group">
                            @foreach($user->getRoleNames() as $role)
                                <label class="col-form-label badge badge-success" style="color:white;">{{ $role }}</label>
                            @endforeach
                            </div>
                        </div>
                    </div>
                    @isset($user->mobile_no)
                    <div class="row">
                        <label class="col-sm-3 col-form-label">{{ __('Mobile No') }}</label>
                        <div class="col-sm-7">
                            <div class="form-group">
                            <label class="col-form-label">{{$user->mobile_no}}</label>
                            </div>
                        </div>
                    </div>
                    @endisset
                    <div class="row">
                        <label class="col-sm-3 col-form-label">{{ __('Email') }}</label>
                        <div class="col-sm-7">
                            <div class="form-group">
                            <label class="col-form-label">{{$user->email}}</label>
                            </div>
                        </div>
                    </div>
                    @isset($user->country)
                    <div class="row">
                        <label class="col-sm-3 col-form-label">{{ __('Country') }}</label>
                        <div class="col-sm-7">
                            <div class="form-group">
                            <label class="col-form-label">{{$user->country}}</label>
                            </div>
                        </div>
                    </div>
                    @endisset
                    @isset($user->state)
                    <div class="row">
                        <label class="col-sm-3 col-form-label">{{ __('State') }}</label>
                        <div class="col-sm-7">
                            <div class="form-group">
                            <label class="col-form-label">{{$user->state}}</label>
                            </div>
                        </div>
                    </div>
                    @endisset
                    @isset($user->address_1)
                    <div class="row">
                        <label class="col-sm-3 col-form-label">{{ __('Address') }}</label>
                        <div class="col-sm-7">
                            <div class="form-group">
                            <label class="col-form-label">{{$user->address_1}}</label>
                            </div>
                        </div>
                    </div>
                    @endisset
                    
                </div>
                <div class="tab-pane fade" id="productBasic" role="tabpanel" aria-labelledby="product-basic-tab">
                    @if(count($products)>0)
                        <div class="row  text-center h5">
                            <label class="col-sm-2 col-form-label">{{ __('Product Name') }}</label>
                            <label class="col-sm-2 col-form-label">{{ __('Brand') }}</label>
                            <label class="col-sm-2 col-form-label">{{ __('Unit') }}</label>
                            <label class="col-sm-2 col-form-label">{{ __('Sku Number') }}</label>
                            <label class="col-sm-2 col-form-label">{{ __('Unit Price') }}</label>         
                        </div>
                        @foreach($products as $value)
                            
                            <div class="row text-center">
                                
                                <div class="col-sm-2">
                                    <div class="form-group">
                                    <label class="col-form-label">{{$value->product_name}}</label>
                                    </div>
                                </div>
                                
                                <div class="col-sm-2">
                                    <div class="form-group">
                                    <label class="col-form-label">{{$value->brand_name}}</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                    <label class="col-form-label">{{$value->unit}}</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                    <label class="col-form-label">{{$value->sku_number}}</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                    <label class="col-form-label">{{$value->unit_price}}</label>
                                    </div>
                                </div>
                            </div>
                            
                        @endforeach
                    @endif
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection