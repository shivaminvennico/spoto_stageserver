@if(count($partner_name) > 0)
<div class="form-group{{ $errors->has('partner_name') ? ' has-danger' : '' }}">
  {{ Form::select('partner_name', $partner_name, null, array('required','class'=>'form-control', 'placeholder'=>'Select Partner Name','id' => 'partner_name'. $dType,'style'=>'-webkit-appearance: menulist;')) }}
  @if ($errors->has('partner_name'))
    <span id="partner_name-error" class="error text-danger" for="input-partner_name">{{ $errors->first('partner_name') }}</span>
  @endif
</div>
@else
    <li>No records found</li>
@endif