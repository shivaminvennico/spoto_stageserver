@if(count($parent_category_affiliate) > 0)
<div class="form-group{{ $errors->has('parent_category_affiliate') ? ' has-danger' : '' }}">
  {{ Form::select('parent_category_affiliate', $parent_category_affiliate, null, array('required','class'=>'form-control', 'placeholder'=>'Select Parent Category','id' => 'parent_category_affiliate','style'=>'-webkit-appearance: menulist;')) }}
  @if ($errors->has('parent_category_affiliate'))
    <span id="parent_category_affiliate-error" class="error text-danger" for="input-parent_category_affiliate">{{ $errors->first('parent_category_affiliate') }}</span>
  @endif
</div>
@else
    <li>No records found</li>
@endif