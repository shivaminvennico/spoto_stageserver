@extends('layouts.master')
@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection
@section('main-content')
@if(session('status'))
    <div class="row">
        <div class="col-sm-12">
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="nav-icon i-Close-Window"></i>
            </button>
            <span>{{ session('status') }}</span>
        </div>
        </div>
    </div>
@endif
<div id="connectionAffiliateRequest">
    <div class="separator-breadcrumb"></div>
        
        <div class="col-md-16">
            <div class="card text-left">

                <div class="card-body">
                    <h4 class="card-title mb-3">Connections</h4>
                    <p>Here you may view affiliate connections</p>
                    <div class="table-responsive">
                        
                    <a class="btn btn-raised ripple btn-primary m-1 float-right" href="{{ route('affiliate.your.request') }}">YOUR REQUESTS</a>
                        <table class="table table-hover h5">
                            <thead>
                                <tr>
                                    
                                    <th scope="col">{{ __('No') }}</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Mobile</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">User Type</th>
                                    <th scope="col">State</th>
                                    <th scope="col">Action</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            <?php $counter=1; ?>
                                @foreach($affiliateConnection as $connection)
                                    <tr class="affiliateProductsRow" data-id="{{ $connection->requester_id }}" style="cursor:pointer;">
                                        <th scope="row">{{$counter++}}</th>
                                        <td>
                                            {{ $connection->req_name }}
                                            
                                        </td>
                                        <td>
                                            {{ $connection->mobile_no }}
                                            
                                        </td>
                                        <td>
                                            {{ $connection->email }}
                                            
                                        </td>
                                        <td>
                                        <label class="badge badge-success">{{ $connection->user_type }}</label>
                                            
                                        </td>
                                        <td>
                                            {{ $connection->state }}
                                            
                                        </td>
                                        <td>
                                            @if($connection->status == 0 )
                                            <button type="button" onclick="acceptConnectionRequest(event,'{{$connection->id}}');" class="btn btn-sm btn-success ladda-button example-button" data-style="expand-left">
                                            <span class="ladda-label">Accept</span></button>
                                            <button type="button" onclick="rejectConnectionRequest(event,'{{$connection->id}}');" class="btn btn-sm btn-danger ladda-button example-button ml-1 " data-style="expand-left">
                                            <span class="ladda-label">Reject</span></button>
                                            @elseif($connection->status == 1)
                                            <button type="button" onclick="giveAccessProducts(event,'{{$connection->id}}');" class="btn btn-success btn-sm ladda-button example-button ml-1 " data-style="expand-left">
                                            <span class="ladda-label">Give Access</span></button>
                                            @elseif($connection->status == 4)
                                            <label class="badge badge-info">Access Given</label>
                                            @else
                                            <label class="badge badge-danger">Rejected</label>
                                            @endif                                                  
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{$affiliateConnection->links()}}

                    </div>


                </div>

                
            </div>
        </div>
</div>
<div id="loadAffiliateProducts">
   
</div>
        <!-- Modal -->
<div class="modal fade" id="selectTierPrice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Set default price</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <form action="">
                        <div class="modal-body">
                        
                        <select id="tier_price_receiver" class="form-control" style="-webkit-appearance: menulist;">
                            <option value="">Select Price</option> 
                        </select>
                                   

                                        
                        </div>
                        <div class="modal-footer">
                            
                            <button type="button" class="btn btn-primary" id="setPriceBtn">Set Tier Price</button>
                        </div>
                        </form>
                    </div>
                </div>
</div>

@endsection
@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
 <script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
<script src="{{asset('assets/js/ladda.script.js')}}"></script>

@endsection