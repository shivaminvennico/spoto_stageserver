@extends('layouts.master')
@section('main-content')
@if(session('status'))
    <div class="row">
        <div class="col-sm-12">
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="nav-icon i-Close-Window"></i>
            </button>
            <span>{{ session('status') }}</span>
        </div>
        </div>
    </div>
@endif
<div class="separator-breadcrumb"></div>
        
        <div class="col-md-16">
            <div class="card text-left">

                <div class="card-body">
                    <h4 class="card-title mb-3">Your Request</h4>
                    <p>Here you may view your affiliate request</p>
                    <div class="table-responsive">
                        
                    <a class="btn btn-raised ripple btn-primary m-1 float-right" href="{{ route('affiliate.request') }}">BACK TO CONNECTION</a>
                        <table class="table table-hover h5">
                            <thead>
                                <tr>
                                    
                                    <th scope="col">{{ __('No') }}</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Mobile</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">User Type</th>
                                    <th scope="col">State</th>
                                    <th scope="col">Action</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            <?php $counter=1; ?>
                                @foreach($affiliateConnection as $connection)
                                    <tr>
                                        <th scope="row">{{$counter++}}</th>
                                        <td>
                                            {{ $connection->req_name }}
                                            
                                        </td>
                                        <td>
                                            {{ $connection->mobile_no }}
                                            
                                        </td>
                                        <td>
                                            {{ $connection->email }}
                                            
                                        </td>
                                        <td>
                                        <label class="badge badge-success">{{ $connection->user_type }}</label>
                                            
                                        </td>
                                        <td>
                                            {{ $connection->state }}
                                            
                                        </td>
                                        <td>
                                            @if($connection->status == 0 )
                                            <label class="badge badge-info">Pending</label>
                                            @elseif($connection->status == 1)
                                            <label class="badge badge-success">Accepted</label>
                                            @elseif($connection->status == 4)
                                            <label class="badge badge-info">Access Given</label>
                                            @else
                                            <label class="badge badge-danger">Rejected</label>
                                            @endif                                                  
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    {{$affiliateConnection->links()}}
                    </div>


                </div>

                
            </div>
        </div>


        <!-- Modal -->
<div class="modal fade" id="selectTierPrice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Set default price</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <form action="">
                        <div class="modal-body">
                        
                        <select id="tier_price_receiver" class="form-control" style="-webkit-appearance: menulist;">
                            <option value="">Select Price</option> 
                        </select>
                                   

                                        
                        </div>
                        <div class="modal-footer">
                            
                            <button type="button" class="btn btn-primary" id="setPriceBtn">Set Tier Price</button>
                        </div>
                        </form>
                    </div>
                </div>
</div>

@endsection