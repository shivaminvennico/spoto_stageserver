<div class="separator-breadcrumb"></div>
        
        <div class="col-md-16">
            <div class="card text-left">

                <div class="card-body">
                    <h4 class="card-title mb-3">Products</h4>
                    <p>Here you can manage products</p>
                    <div class="table-responsive">
                        <table id="zero_configuration_table" class="table table-hover">
                        <thead>
                                        <tr>
                                            <th><label class="checkbox checkbox-primary"><input type="checkbox" class="select-all-affiliate" /><span class="checkmark"></span></label></th>
                                            <th scope="col">{{ __('No') }}</th>
                                            <th scope="col">Product Name</th>
                                            <th scope="col">Category</th>
                                            <th scope="col">Sub Category</th>
                                            @isset($product->sub_sub_name)
                                            <th scope="col">Sub Sub Category</th>
                                            @endisset
                                            <th scope="col">Brand</th>
                                            <th scope="col">SKU Number</th>
                                            <th scope="col">Current Quantity</th>
                                            <th scope="col">Unit Price</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php $counter=1; ?>
                                      @foreach($Products as $product)
                                        <tr class="h5">
                                            <td><label class="checkbox checkbox-primary"><input type="checkbox" name="affiliate_products[]" value="{{$product->id}}"/><span class="checkmark"></span></label></td>
                                            <th scope="row">{{$counter++}}</th>
                                            <td>
                                            	{{ $product->product_name }}
                            					
                                            </td>
                                            <td>{{ $product->name }}</td>
                                            <td>{{ $product->sub_name }}</td>
                                            @isset($product->sub_sub_name)
                                            <td>{{ $product->sub_sub_name }}</td>
                                            @endisset
                                            <td>{{ $product->brand_name }}</td>
                                            <td>{{ $product->sku_number }}</td>
                                            <td>
                                            	{{ $product->unit }}
                                            </td>

                                            <td>{{ $product->unit_price }}</td>
                                            
                                          
                                        </tr>
                                      @endforeach
                                       
                                    </tbody>
                        </table>
                    </div>


                </div>

                <div class="card-footer text-center">
                <button type="button" class="btn btn-primary" id="giveAccessAffiliateProducts" data-id="{{$id}}">{{ __('Give Access') }}</button>
            </div>
            </div>
        </div>               
                               