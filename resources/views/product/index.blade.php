@extends('layouts.master')
@section('main-content')
@if(session('status'))
    <div class="row">
        <div class="col-sm-12">
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="nav-icon i-Close-Window"></i>
            </button>
            <span>{{ session('status') }}</span>
        </div>
        </div>
    </div>
@endif
@if(session('error'))
    <div class="row">
        <div class="col-sm-12">
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="nav-icon i-Close-Window"></i>
            </button>
            <span>{{ session()->get('error') }}</span>
        </div>
        </div>
    </div>
    @endif

            
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="product-basic-tab" data-toggle="tab" href="#productBasic" role="tab" aria-controls="productBasic" aria-selected="true">Products</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="affiliate-products-basic-tab" data-toggle="tab" href="#affiliateProductBasic" role="tab" aria-controls="affiliateProductBasic" aria-selected="false">Affiliate Products</a>
    </li>
    
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="productBasic" role="tabpanel" aria-labelledby="product-basic-tab">
        @can('product-create')
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-6">
                <div class="card mb-6">
                    <div class="card-header">
                        Upload Excel
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Excel</h5>
                        <p class="card-text">Description of Excel module shown here.
                        </p>
                        <button type="button" class="btn btn-primary btn-rounded" data-toggle="modal" data-target="#uploadFile">
                                    Upload File
                                </button>
                        <a href="{{route('products.downloadExcel')}}"  class="btn btn-primary btn-rounded">Download Format</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-6">
                <div class="card mb-6">
                    <div class="card-header">
                        Add Products
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Manually</h5>
                        <p class="card-text">Description of Manually module shown here.
                        </p>
                        <a href="{{route('product.add')}}" class="btn btn-primary btn-rounded">Add Single Product</a>
                        <a href="{{route('bundle.add')}}" class="btn btn-primary btn-rounded">Add Bundle Products</a>
                    </div>
                </div>
            </div>
        </div>
        @endcan
        <div class="separator-breadcrumb"></div>
        
        <div class="col-md-16">
            <div class="card text-left">

                <div class="card-body">
                    <h4 class="card-title mb-3">Products</h4>
                    <p>Here you can manage products</p>
                    <div class="table-responsive">
                        <table class="table table-hover">
                        <thead>
                            <tr>
                            <th scope="col">{{ __('No') }}</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Brand</th>
                            <th scope="col">SKU Number</th>
                            <th scope="col">Current Quantity</th>
                            <th scope="col">Unit Price</th>
                            @can('product-edit')
                            <th scope="col">Action</th>
                            @endcan
                            <!-- <th scope="col">Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                        
                            <?php $counter=1;?>
                            @foreach($Products as $product)
                           
                            <tr class="h5 productRows" data-id="{{$product->id}}" style="cursor:pointer;">

                            <th scope="row">{{$counter++}}</th>
                            <td>
                            {{ $product->product_name }} 
                            <?php
                            
                            if($product->bundled_product != 0){
                                ?>
                                <label class="badge badge-info">B</label>
                            <?php
                            }                          
                            ?> 

                            </td>
                            <td>{{ $product->brand_name }}</td>
                            <td>{{ $product->sku_number }}</td>
                            <td>
                            {{ $product->unit }}
                            </td>

                            <td>{{ $product->unit_price }}</td>


                            @can('product-edit')
                            <td>

                            <a rel="tooltip" class="btn btn-success" href="{{route('products.edit',$product)}}" data-original-title="" title="">
                            <i class="nav-icon i-Pen-2"></i>
                            <div class="ripple-container"></div>
                            </a>

                            </td>
                            @endcan
                            </tr>
                            @endforeach
                           
                        </tbody>
                        
                        </table>
                        {{$Products->links()}}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="uploadFile" tabindex="-1" role="dialog" aria-labelledby="uploadFileTitle-2" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="uploadFileTitle-2">Upload File</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="form" method="POST" enctype="multipart/form-data" action="{{route('upload.products.excel')}}">
                        <div class="modal-body">
                        @csrf   
                        <input type="file" id="product_file" name="product_file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">

                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="affiliateProductBasic" role="tabpanel" aria-labelledby="affiliate-products-basic-tab">
        <div class="col-md-16">
            <div class="card text-left">

                <div class="card-body">
                    <h4 class="card-title mb-3">Affiliate Products</h4>
                    <p>Here you can manage affiliate products</p>
                    <div class="table-responsive">
                        <table class="table table-hover">
                        <thead>
                            <tr>
                            <th scope="col">{{ __('No') }}</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Brand</th>
                            <th scope="col">SKU Number</th>
                            <th scope="col">Current Quantity</th>
                            <th scope="col">Unit Price</th>
                            <th scope="col">User Type</th>
                            <th scope="col">Receiver Name</th>
                            @role('Admin')
                            <th scope="col">Requester Name</th>
                            @endrole
                            @unlessrole('Admin')
                            <th scope="col">Action</th>
                            @endunlessrole
                            </tr>
                        </thead>
                        <tbody>
                            <?php $counter=1; ?>
                            @foreach($AffiliateProducts as $product)
                            <tr class="h5 productRows" data-id="{{$product->id}}" style="cursor:pointer;">

                            <th scope="row">{{$counter++}}</th>
                            <td>
                            {{ $product->product_name }}

                            </td>
                            <td>{{ $product->brand_name }}</td>
                            <td>{{ $product->sku_number }}</td>
                            <td>
                            {{ $product->unit }}
                            </td>

                            <td>{{ $product->unit_price }}</td>
                            <td>{{ $product->user_type }}</td>
                            <td>{{ $product->rec_name }}</td>
                            @role('Admin')
                            <td>{{ $product->req_name }}</td>
                            @endrole
                            @unlessrole('Admin')
                            <td>

                            <a rel="tooltip" class="btn btn-success" href="{{route('affiliate.products.edit',$product)}}" data-original-title="" title="">
                            <i class="nav-icon i-Pen-2"></i>
                            <div class="ripple-container"></div>
                            </a>

                            </td>
                            @endunlessrole
                            </tr>
                            @endforeach

                        </tbody>
                        </table>
                        {{$AffiliateProducts->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection