@extends('layouts.master')
@section('before-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/smart.wizard/smart_wizard_theme_arrows.min.css')}}">
  <!-- MultiSelect CSS & JS library -->
  <link href="{{asset('assets/styles/css/jquery.multiselect.css')}}" rel="stylesheet" />
@endsection
@section('page-css')
  <link rel="stylesheet" href="{{asset('assets/styles/vendor/dropzone.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/toastr.css')}}">

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/quill.snow.css')}}">
@endsection
@section('main-content')
  <div class="breadcrumb">
              <h1>Add Products</h1>
                  <ul>
                      <li><a href="">Manually</a></li>
                      <li>Add Products</li>
                  </ul>
  </div>

  <div class="separator-breadcrumb border-top"></div>
<form method="post" action="{{route('product.store')}}" id="product_form" autocomplete="off" enctype="multipart/form-data">
  @csrf   
  <div class="row">
                <div class="col-md-12">
                    <!-- SmartWizard html -->
                    <div id="smartwizard" class="sw-main sw-theme-arrows">
                        <ul>
                            <li><a href="#step-1">General<br /><small>The basic details.</small></a></li>
                            <li><a href="#step-2">Images<br /><small>Upload image for products.</small></a></li>
                            <li><a href="#step-3">Price<br /><small>Detail about product price.</small></a></li>
                            <li><a href="#step-4">Description<br /><small>This product description.</small></a></li>
                            <li><a href="#step-5">Shipping Info<br /><small>This payment details.</small></a></li>
                            <li><a href="#step-6">Inventory<br /><small>This Stock details.</small></a></li>
                            <li><a href="#step-7">Vehicle Mapping<br /><small>This vehicle details.</small></a></li>
                        </ul>
                        
                          <div style="height:361px;overflow:auto;">
                              <div id="step-1" class="">
                              <div class="form-group">
                                <label for="product_name" class="col-form-label">Product Name</label>
                                  <div class="col-sm-8">
                                      <div class="form-group{{ $errors->has('product_name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('product_name') ? ' is-invalid' : '' }}"
                                            name="product_name" id="input-product_name" type="text" placeholder="{{ __('Product Name') }}"
                                            value="{{ old('product_name') }}" required aria-required="true" />
                                          @if ($errors->has('product_name'))
                                          <span id="name-error" class="error text-danger"
                                              for="input-product_name">{{ $errors->first('product_name') }}</span>
                                          @endif
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group">
                              <!-- <div class="row"> -->
                                <div class="col-sm-4">
                                <label for="product_name" class="col-form-label">Category</label>
                                    <div class="form-group{{ $errors->has('category_id') ? ' has-danger' : '' }}">
                                        {{ Form::select('category_id', $ParentCategories, null, array('required','class'=>'form-control', 'placeholder'=>'Select Parent Category','id' => 'categoryId','style'=>'-webkit-appearance: menulist;')) }}
                                          @if ($errors->has('category_id'))
                                            <span id="category_id-error" class="error text-danger" for="input-category_id">{{ $errors->first('category_id') }}</span>
                                          @endif
                                    </div>
                                  </div>
                                  <div id="loadSubCategoryForProduct" class="col-sm-4">
                                  
                                      
                                  </div>
                                  <div id="loadSubSubCategoryForProduct" class="col-sm-4">
                                  
                                    
                                  </div>
                                <!-- </div> -->
                              </div>
                              <div class="form-group">
                                <label for="brand_name" class="col-form-label">Brand Name</label>
                                  <div class="col-sm-8">
                                      <div class="form-group{{ $errors->has('brand_name') ? ' has-danger' : '' }}">
                                      {{ Form::select('brand_name', $Brands, null, array('required','class'=>'form-control', 'placeholder'=>'Select Brand','id' => 'input-brand_name','style'=>'-webkit-appearance: menulist;')) }}
                                      
                                          @if ($errors->has('brand_name'))
                                          <span id="name-error" class="error text-danger"
                                              for="input-brand_name">{{ $errors->first('brand_name') }}</span>
                                          @endif
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                <label for="sku_number" class="col-form-label">SKU Number</label>
                                  <div class="col-sm-8">
                                      <div class="form-group{{ $errors->has('sku_number') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('sku_number') ? ' is-invalid' : '' }}"
                                        name="sku_number" id="input-sku_number" type="text" placeholder="{{ __('SKU number') }}"
                                        value="{{ old('sku_number') }}" required="true" aria-required="true" />
                                        @if ($errors->has('sku_number'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-sku_number">{{ $errors->first('sku_number') }}</span>
                                        @endif
                                      </div>
                                  </div>
                              </div>             
                              </div>
                              <div id="step-2" class="">
                              
                              <div id="myId" class="dropzone dropzone-area">
                                <div class="fallback ">
                                  <input type="file" name="file[]" id="myFile" multiple/>
                                </div>
                              </div>
                              
                              </div>
                              <div id="step-3" class="">
                              <div class="form-group">
                                <label for="product_unit_price" class="col-form-label">Unit Price</label>
                                <div class="row">
                               
                                  <div class="col-sm-8">
                                      <div class="form-group{{ $errors->has('product_unit_price') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('product_unit_price') ? ' is-invalid' : '' }}"
                                            name="product_unit_price" id="input-product_unit_price" type="text" placeholder="{{ __('Unit Price') }}"
                                            value="{{ old('product_unit_price') }}" required aria-required="true" />
                                          @if ($errors->has('product_unit_price'))
                                          <span id="name-error" class="error text-danger"
                                              for="input-product_unit_price">{{ $errors->first('product_unit_price') }}</span>
                                          @endif
                                        
                                      </div>
                                      
                                  </div>
                                  <div class="col-sm-2"></div>
                                  <div>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#multiplePrice">
                                            Multiple Price
                                        </button>
                                  </div>
                                  </div>
                              </div>

                              <div class="form-group">
                                <label for="product_purchase_price" class="col-form-label">Purchase Price</label>
                                  <div class="col-sm-8">
                                      <div class="form-group{{ $errors->has('product_purchase_price') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('product_purchase_price') ? ' is-invalid' : '' }}"
                                            name="product_purchase_price" id="input-product_purchase_price" type="text" placeholder="{{ __('Purchase Price') }}"
                                            value="{{ old('product_purchase_price') }}" required aria-required="true" />
                                          @if ($errors->has('product_purchase_price'))
                                          <span id="name-error" class="error text-danger"
                                              for="input-productpurchaset_price">{{ $errors->first('product_purchase_price') }}</span>
                                          @endif
                                      </div>
                                  </div>
                              </div>

                              <div class="form-group">
                                <label for="product_tax" class="col-form-label">Tax</label>
                                  <div class="col-sm-8">
                                      <div class="form-group{{ $errors->has('product_tax') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('product_tax') ? ' is-invalid' : '' }}"
                                            name="product_tax" id="input-product_tax" type="text" placeholder="{{ __('Tax') }}"
                                            value="{{ old('product_tax') }}" required aria-required="true" />
                                          @if ($errors->has('product_tax'))
                                          <span id="name-error" class="error text-danger"
                                              for="input-product_tax">{{ $errors->first('product_tax') }}</span>
                                          @endif
                                      </div>
                                  </div>
                              </div>

                              <div class="form-group">
                                <label for="product_discount" class="col-form-label">Discount</label>
                                  <div class="col-sm-8">
                                      <div class="form-group{{ $errors->has('product_discount') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('product_discount') ? ' is-invalid' : '' }}"
                                            name="product_discount" id="input-product_discount" type="text" placeholder="{{ __('Discount') }}"
                                            value="{{ old('product_discount') }}" required aria-required="true" />
                                          @if ($errors->has('product_discount'))
                                          <span id="name-error" class="error text-danger"
                                              for="input-product_discount">{{ $errors->first('product_discount') }}</span>
                                          @endif
                                      </div>
                                  </div>
                              </div>

                              </div>
                              <div id="step-4" class="">
                                <div class="mx-auto col-md-8">
                                  <div id="snow-editor">
                                      
                                  </div>
                                </div>
                                <div class="form-group">
                                <label for="product_description" class="col-form-label">Product Description</label>
                                  <div class="col-sm-8">
                                      <div class="form-group{{ $errors->has('product_description') ? ' has-danger' : '' }}">
                                     
                                      <div id="full-editor">
                                        
                                       

           
                                      </div>
                                      <input type="hidden" name="product_description" id="product_description"/>
                                      
                                        <!-- <textarea class="form-control{{ $errors->has('product_description') ? ' is-invalid' : '' }}"
                                            name="product_description" id="input-product_description" type="text" placeholder="{{ __('Product Description') }}"
                                            value="{{ old('product_description') }}" required aria-required="true" ></textarea>
                                          @if ($errors->has('product_description'))
                                          <span id="name-error" class="error text-danger"
                                              for="input-product_description">{{ $errors->first('product_description') }}</span>
                                          @endif -->
                                      </div>
                                  </div>
                              </div>
                            </div>
                            <div id="step-5" class="">
                                
                                <div class="form-group">
                                <label for="free_shipping" class="col-form-label">Free Shipping</label>
                                  <div class="col-sm-8">
                                      <div class="form-group{{ $errors->has('free_shipping') ? ' has-danger' : '' }}">
                                        
                                        <div class="form-group row">
                                    <div class="col-sm-2">Status</div>
                                    <div class="col-sm-10">
                                        <label class="switch switch-primary mr-3">
                                        <span></span>
                                            <input type="checkbox" name="free_shipping" id="input-free_shipping">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                    </div>
                                         
                                      </div>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                <label for="local_pickup" class="col-form-label">Local Pickup</label>
                                  <div class="col-sm-8">
                                      <div class="form-group{{ $errors->has('local_pickup') ? ' has-danger' : '' }}">
                                      <div class="form-group row">
                                        <div class="col-sm-2">Status</div>
                                        <div class="col-sm-10">
                                            <label class="switch switch-primary mr-3">
                                            <span></span>
                                                <input type="checkbox" name="local_pickup" id="input-local_pickup">
                                                <span class="slider"></span>
                                            </label>
                                        </div>
                                      </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="local_pickup_shipping_cost" class="col-sm-2 col-form-label">Shipping Cost</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                            name="local_pickup_shipping_cost" id="local_pickup_shipping_cost" placeholder="Shipping Cost" value="{{ old('local_pickup_shipping_cost') }}" required aria-required="true" >
                                            @if ($errors->has('local_pickup_shipping_cost'))
                                              <span id="name-error" class="error text-danger"
                                                  for="input-local_pickup_shipping_cost">{{ $errors->first('local_pickup_shipping_cost') }}</span>
                                              @endif
                                        </div>
                                     
                                    </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                <label for="flat_rate" class="col-form-label">Flat Rate</label>
                                  <div class="col-sm-8">
                                      <div class="form-group{{ $errors->has('flat_rate') ? ' has-danger' : '' }}">
                                      <div class="form-group row">
                                        <div class="col-sm-2">Status</div>
                                        <div class="col-sm-10">
                                            <label class="switch switch-primary mr-3">
                                            <span></span>
                                                <input type="checkbox" name="flat_rate" id="input-flat_rate">
                                                <span class="slider"></span>
                                            </label>
                                        </div>
                                      </div>
                                      </div>
                                      <div class="form-group row">
                                    <label for="flat_rate_shipping_cost" class="col-sm-2 col-form-label">Shipping Cost</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control"
                                        name="flat_rate_shipping_cost"  id="flat_rate_shipping_cost" placeholder="Shipping Cost" value="{{ old('flat_rate_shipping_cost') }}" required aria-required="true" >
                                        @if ($errors->has('flat_rate_shipping_cost'))
                                          <span id="name-error" class="error text-danger"
                                              for="input-flat_rate_shipping_cost">{{ $errors->first('flat_rate_shipping_cost') }}</span>
                                          @endif
                                    
                                    </div>
                                    </div>
                                  </div>
                              </div>
                              
                              
                              </div>

                              <div id="step-6" class="">
                                <div class="addMultipleWarehouse">
                                  <div class="form-group row" id="1">
                                    <label for="qty" class="col-form-label">Quantity</label>
                                    <div class="col-sm-1">
                                          <input type="text" class="form-control" name="qty[]"  placeholder="Qty" value="{{ old('qty.0') }}" required aria-required="true" >
                                          @if ($errors->has('qty.0'))
                                            <span id="name-error" class="error text-danger"
                                                for="input-qty">{{ $errors->first('qty.0') }}</span>
                                            @endif
                                      </div>
                                      <label for="warehouse" class="col-form-label">Warehouse</label>
                                      <div class="col-sm-4">
                                      {{ Form::select('warehouse[]', $Warehouse, null, array('class'=>'form-control warehouse', 'placeholder'=>'Select Warehouse','style'=>'-webkit-appearance: menulist;','required')) }}
                                          
                                          @if ($errors->has('warehouse.0'))
                                            <span id="name-error" class="error text-danger"
                                                for="input-warehouse">{{ $errors->first('warehouse.0') }}</span>
                                            @endif
                                      </div>
                                      <label for="qty_InHand" class="col-form-label">Quantity In Hand</label>
                                      <div class="col-sm-2">
                                          <input type="text" class="form-control"
                                          name="qty_InHand[]" placeholder="Qty In Hand" value="{{ old('qty_InHand.0') }}" required aria-required="true" >
                                          @if ($errors->has('qty_InHand.0'))
                                            <span id="name-error" class="error text-danger"
                                                for="input-qty_InHand">{{ $errors->first('qty_InHand.0') }}</span>
                                            @endif
                                      </div>

                                  </div>
                                </div>
                                <div id="additionalWarehouse"></div>
                                @if(count($Warehouse) > 1)
                                <div class="form-group row">
                                  <div class="col-sm-4">
                                  </div>
                                  <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary" id="addWarehouseBtn">Add More</button>
                                  </div>
                                  <div class="col-sm-4">
                                  </div>
                                </div>
                              @endif
                              </div>


                              <div id="step-7" class="">
                              <div class="row">
                  
                  <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('make_id') ? ' has-danger' : '' }}">
                    {{ Form::select('make_id', $Make, null, array('class'=>'form-control', 'placeholder'=>'Select Make','id' => 'makeId','style'=>'-webkit-appearance: menulist;')) }}
                      @if ($errors->has('make_id'))
                        <span id="make_id-error" class="error text-danger" for="input-make_id">{{ $errors->first('make_id') }}</span>
                      @endif
                    </div>
                  </div>

                  <!-- SearchBar -->
                  <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('searchBar') ? ' has-danger' : '' }}">
                   
                    <select id="searchBar" multiple="multiple" name="searchBar[]">
                    </select>
                    
                    </div>
                  </div>
                  
                </div>
                <div class="row">
                  
                  <div id="loadVehiclModel" class="col-sm-6">
                    
                  </div>
                  
                </div>
                <div class="row">
                  
                  <div id="loadVehiclYear" class="col-sm-6">
                    
                  </div>
                  
                </div>
                <div class="row">
                  
                  <div id="loadVehiclVariant" class="col-sm-6">
                    
                  </div>
                  
                </div>
                <div class="row m-1">
                  <button type="button" class="btn btn-primary btn-sm" id="mapMoreVehicles">{{ __('ADD MORE') }}</button>
                </div>  
                <div class="row">
                <div class="col-md-6 mb-6">
                    <div class="card text-left">

                        <div class="card-body"  id="mappedVehicleList" style="display:none;height:200px;overflow:auto">
                            <h4 class="card-title mb-2">Mapped Vehicles</h4>
                            
                            <ul class="list-group">
                                
                            </ul>
                        </div>
                    </div> 
                  </div>       
              </div>
                 
            </div>
  </div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="multiplePrice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Multiple Price</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <form action="">
                        <div class="modal-body">
                        <div class="form-group row">
                                            <label for="tier_one_price" class="ul-form__label ul-form--margin col-lg-2   col-form-label ">{{$tierNames->tier_1}}</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="tier_one_price" name="tier_one_price" placeholder="00">
                                                
                                            </div>

                                            <label for="tier_two_price" class="ul-form__label ul-form--margin col-lg-2 col-form-label ">{{$tierNames->tier_2}}</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="tier_two_price" name="tier_two_price" placeholder="00">
                                                
                                            </div>

                                          
                                           
                          </div>

                          <div class="form-group row">
                                            <label for="tier_three_price" class="ul-form__label ul-form--margin col-lg-2   col-form-label ">{{$tierNames->tier_3}}</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="tier_three_price" name="tier_three_price" placeholder="00">
                                                
                                            </div>

                                            <label for="tier_four_price" class="ul-form__label ul-form--margin col-lg-2 col-form-label ">{{$tierNames->tier_4}}</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="tier_four_price" name="tier_four_price" placeholder="00">
                                                
                                            </div>

                                          
                                           
                                        </div>


                                        <div class="form-group row">
                                            <label for="tier_five_price" class="ul-form__label ul-form--margin col-lg-2   col-form-label ">{{$tierNames->tier_5}}</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="tier_five_price" name="tier_five_price" placeholder="00">
                                                
                                            </div>

                                            <label for="tier_six_price" class="ul-form__label ul-form--margin col-lg-2 col-form-label ">{{$tierNames->tier_6}}</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="tier_six_price" name="tier_six_price" placeholder="00">
                                                
                                            </div>

                                          
                                        </div>

                                            <div class="form-group row">
                                            <label for="tier_seven_price" class="ul-form__label ul-form--margin col-lg-2   col-form-label ">{{$tierNames->tier_7}}</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="tier_seven_price" name="tier_seven_price" placeholder="00">
                                                
                                            </div>

                                            <label for="tier_eight_price" class="ul-form__label ul-form--margin col-lg-2 col-form-label ">{{$tierNames->tier_8}}</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="tier_eight_price" name="tier_eight_price" placeholder="00">
                                                
                                            </div>

                                           
                                        </div>

                                        
                                        <div class="form-group row">
                                            <label for="tier_nine_price" class="ul-form__label ul-form--margin col-lg-2   col-form-label ">{{$tierNames->tier_9}}</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="tier_nine_price" name="tier_nine_price" placeholder="00">
                                                
                                            </div>

                                            <label for="tier_ten_price" class="ul-form__label ul-form--margin col-lg-2 col-form-label ">{{$tierNames->tier_10}}</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="tier_ten_price" name="tier_ten_price" placeholder="00">
                                                
                                            </div>

                                           
                                        </div>

                                        

                                        
                        </div>
                        <div class="modal-footer">
                            <input type="reset" class="btn btn-secondary"  value="Reset"/>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Add multiple price</button>
                        </div>
                        </form>
                    </div>
                </div>
</div>
</form>
@endsection

@section('page-js')
  <script src="{{asset('assets/js/vendor/jquery.smartWizard.min.js')}}"></script>
  
  <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
  <script src="{{asset('assets/js/vendor/quill.min.js')}}"></script>
  
  <script src="{{asset('assets/js/vendor/dropzone.min.js')}}"></script>
  <script>
     var uploadedDocumentMap = {};
    Dropzone.autoDiscover = false;
   $("div#myId").dropzone({ 
        url: "{{route('product.image')}}" ,
        method:'post',
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 1, // MB
        maxFiles: 5,
        maxThumbnailFilesize: 1, // MB
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        headers: {
        'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        success: function (file, response) {
         
      $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
      uploadedDocumentMap[file.name] = response.name
    },
    init: function () {
      this.on("error", function(file) {
                this.removeFile(file);
              
            });
            
          
    },
    removedfile: function (file) {
      if (file.status == "success") {
        $.ajax({
        url: "{{route('product.image.remove')}}",
        data : {fileName :uploadedDocumentMap[file.name]},
        headers: {
        'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        method: 'post'
      });
    }
          file.previewElement.remove()
          var name = ''
          if (typeof file.file_name !== 'undefined') {
            name = file.file_name
          } else {
            name = uploadedDocumentMap[file.name]
          }
          $('form').find('input[name="document[]"][value="' + name + '"]').remove()
      
     
      
    }
    
        
  });

  $(document).ready(function(){
    $("#product_form").validate({
       errorClass: 'text-danger'



   });
  $('#searchBar').multiselect({
                  columns: 1,
                  placeholder: "Search Vehicle",
                  search:true,
                  selectAll:true
              });
  
    
    var quill = new Quill('#full-editor', {
        modules: {
            syntax: !0,
            toolbar: [
                [{
                    list: "ordered"
                }, {
                    list: "bullet"
                }],
                
                
            ]
        },
        theme: 'snow'
    });

  });
   </script>

@endsection
@section('bottom-js')
  <script src="{{asset('assets/js/smart.wizard.script.js')}}"></script>
<script src="{{asset('assets/js/jquery.multiselect.js')}}"></script>
<script src="{{asset('assets/js/vendor/toastr.min.js')}}"></script>
<script src="{{asset('assets/js/toastr.script.js')}}"></script>
  
@endsection