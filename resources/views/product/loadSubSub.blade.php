@if(count($SubSubCategories) > 0)
<label for="sub_sub_category_id" class="col-form-label">Sub Sub Category</label>
<div class="form-group{{ $errors->has('sub_sub_category_id') ? ' has-danger' : '' }}">
  {{ Form::select('sub_sub_category_id', $SubSubCategories, null, array('required','class'=>'form-control', 'placeholder'=>'Select Sub Category','style'=>'-webkit-appearance: menulist;')) }}
  @if ($errors->has('sub_sub_category_id'))
    <span id="sub_sub_category_id-error" class="error text-danger" for="input-sub_sub_category_id">{{ $errors->first('sub_sub_category_id') }}</span>
  @endif
</div>
@else
    <li>No Sub Sub Categories Linked</li>
@endif
