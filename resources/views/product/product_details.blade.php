@extends('layouts.master')
@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/quill.snow.css')}}">
@endsection
@section('main-content')
          <div class="breadcrumb">
                <h1>Products</h1>
                <ul>
                <li><a href="">Details</a></li>
                </ul>
                <div class="col-md-12 text-right">
                                    <a href="{{route('product')}}"
                                        class="btn btn-sm btn-primary">{{ __('BACK TO LIST') }}</a>
                                </div>
            </div>

            <div class="separator-breadcrumb border-top"></div>
            

            <div class="row mb-4">


                <div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
                                </li>
                                @if(count($bundle) > 0)
                                <li class="nav-item">
                                    <a class="nav-link" id="bundle-tab" data-toggle="tab" href="#bundle" role="tab" aria-controls="bundle" aria-selected="true">Bundled Products</a>
                                </li>
                                @endif
                                <li class="nav-item">
                                    <a class="nav-link" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="false">Description</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="images-tab" data-toggle="tab" href="#Images" role="tab" aria-controls="Images" aria-selected="false">Images</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="price-tab" data-toggle="tab" href="#price" role="tab" aria-controls="price" aria-selected="false">Price</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="shipping-tab" data-toggle="tab" href="#shipping" role="tab" aria-controls="shipping" aria-selected="false">Shipping</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="inventory-tab" data-toggle="tab" href="#inventory" role="tab" aria-controls="inventory" aria-selected="false">Inventory</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="mapped-vehicle-tab" data-toggle="tab" href="#mappedVehicles" role="tab" aria-controls="mappedVehicles" aria-selected="false">Mapped Vehicles</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general-tab">
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label">{{ __('Product Name') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                            <label class="col-form-label">{{$products[0]->product_name}}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label">{{ __('Brand') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                            <label class="col-form-label">{{$products[0]->brand_name}}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label">{{ __('SKU Number') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                            <label class="col-form-label">{{$products[0]->sku_number}}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label">{{ __('Category') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                            <label class="col-form-label">{{$products[0]->name}}</label>
                                            </div>
                                        </div>
                                    </div>
                                    @isset($products[0]->sub_name)
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label">{{ __('Sub Category') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                            <label class="col-form-label">{{$products[0]->sub_name}}</label>
                                            </div>
                                        </div>
                                    </div>
                                    @endisset
                                    @isset($products[0]->sub_sub_name)
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label">{{ __('Sub Sub Category') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                            <label class="col-form-label">{{$products[0]->sub_sub_name}}</label>
                                            </div>
                                        </div>
                                    </div>
                                    @endisset
                                </div>
                                <div class="tab-pane fade" id="bundle" role="tabpanel" aria-labelledby="bundle-tab">
                                    @if(count($bundle)>0)
                                        <div class="row  text-center h5">
                                            <label class="col-sm-2 col-form-label">{{ __('Product Name') }}</label>
                                            <label class="col-sm-2 col-form-label">{{ __('Brand') }}</label>
                                            <label class="col-sm-2 col-form-label">{{ __('Unit') }}</label>
                                            <label class="col-sm-2 col-form-label">{{ __('Sku Number') }}</label>
                                            <label class="col-sm-2 col-form-label">{{ __('Unit Price') }}</label>         
                                        </div>
                                        @foreach($bundle as $value)
                                            
                                            <div class="row text-center">
                                                
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                    <label class="col-form-label">{{$value->product_name}}</label>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                    <label class="col-form-label">{{$value->brand_name}}</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                    <label class="col-form-label">{{$value->unit}}</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                    <label class="col-form-label">{{$value->sku_number}}</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                    <label class="col-form-label">{{$value->unit_price}}</label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        @endforeach
                                    @endif
                                </div>
                                <div class="tab-pane fade" id="description" role="tabpanel" aria-labelledby="description-tab">
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label">{{ __('Description') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                            <label class="col-form-label ql-editor">{!! $products[0]->description !!}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="Images" role="tabpanel" aria-labelledby="images-tab">
                                
                                @if($products[0]->photos != 0)
                                    <?php $photo=[];
                                    array_push($photo,explode(',',$products[0]->photos));
                                   
                                    ?>
                                    <div class="row list-grid">
                                    @foreach($photo[0] as $img)
                                    @if(Storage::disk('s3')->exists('Products/'.$img))
                                    <div class="list-item col-md-3">
                                        <div class="card o-hidden mb-4 d-flex flex-column">
                                            <div class="list-thumb d-flex">
                                            <img src="{{Storage::disk('s3')->url('Products/'.$img)}}">
                                            </div>
                                            
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                    </div>
                                @endif
                                </div>
                                <div class="tab-pane fade" id="price" role="tabpanel" aria-labelledby="price-tab">
                                    <section class="col-md-6 float-left">
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{ __('Unit Price') }}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->unit_price}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{ __('Purchase Price') }}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->purchase_price}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{ __('Tax') }}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->tax}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{ __('Discount') }}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->discount}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="col-md-6 float-right">
                                        @isset($products[0]->tier_1)
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{$tier_labels->tier_1}}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->tier_1}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endisset
                                    
                                        @isset($products[0]->tier_2)
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{$tier_labels->tier_2}}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->tier_2}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endisset

                                        @isset($products[0]->tier_3)
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{$tier_labels->tier_3}}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->tier_3}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endisset

                                        @isset($products[0]->tier_4)
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{$tier_labels->tier_4}}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->tier_4}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endisset

                                        @isset($products[0]->tier_5)
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{$tier_labels->tier_5}}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->tier_5}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endisset

                                        @isset($products[0]->tier_6)
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{$tier_labels->tier_6}}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->tier_6}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endisset

                                        @isset($products[0]->tier_7)
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{$tier_labels->tier_7}}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->tier_7}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endisset

                                        @isset($products[0]->tier_8)
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{$tier_labels->tier_8}}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->tier_8}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endisset

                                        @isset($products[0]->tier_9)
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{$tier_labels->tier_9}}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->tier_9}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endisset

                                        @isset($products[0]->tier_10)
                                        <div class="row">
                                            <label class="col-sm-3 col-form-label">{{$tier_labels->tier_10}}</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                <label class="col-form-label">{{$products[0]->tier_10}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endisset
                                    </section>
                                </div>
                                
                                <div class="tab-pane fade" id="shipping" role="tabpanel" aria-labelledby="shipping-tab">
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label">{{ __('Free Shipping') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                            <label class="col-form-label">
                                            @if($products[0]->free_shipping == 1)
                                        ON
                                            @else
                                            OFF
                                            @endif</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label">{{ __('Local Pickup') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                            <label class="col-form-label"> @if($products[0]->local_pickup == 1)
                                        ON
                                            @else
                                            OFF
                                            @endif</label>
                                            </div>
                                        </div>
                                    </div>
                                    @if($products[0]->local_pickup == 1)
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label">{{ __('Shipping Cost') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                            <label class="col-form-label">
                                                {{$products[0]->local_pickup_shipping_cost}}</label>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label">{{ __('Flat Rate') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                            <label class="col-form-label"> @if($products[0]->flat_rate == 1)
                                            ON
                                            @else
                                            OFF
                                            @endif</label>
                                            </div>
                                        </div>
                                    </div>
                                    @if($products[0]->flat_rate == 1)
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label">{{ __('Shipping Cost') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                            <label class="col-form-label">{{$products[0]->flat_rate_shipping_cost}}</label>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="tab-pane fade" id="inventory" role="tabpanel" aria-labelledby="inventory-tab">
                                    @isset($inventory)
                                        <div class="row  text-center h5">
                                            <label class="col-sm-3 col-form-label">{{ __('Quantity') }}</label>
                                            <label class="col-sm-3 col-form-label">{{ __('Warehouse') }}</label>
                                            <label class="col-sm-3 col-form-label">{{ __('Quantity In Hand') }}</label>         
                                        </div>
                                        @foreach($inventory as $value)
                                            
                                            <div class="row text-center">
                                                
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                    <label class="col-form-label">{{$value->qty}}</label>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                    <label class="col-form-label">{{$value->name}}</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                    <label class="col-form-label">{{$value->qty_in_hand}}</label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        @endforeach
                                    @endisset
                                </div>
                                <div class="tab-pane fade" id="mappedVehicles" role="tabpanel" aria-labelledby="mapped-vehicle-tab">
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label">Mapped Vehicle</label>
                                        <div class="col-sm-7 ">
                                            <div class="form-group">
                                        @foreach($products as $product)
                                        
                                            <label class="col-form-label ">{{$product->make}} {{$product->model}} {{$product->variant_name}}</label> <br/>
                                            
                                        @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>

@endsection