
    <div class="container-fluid">
    @if (session('status'))
          <div class="row">
            <div class="col-sm-12">
              <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <i class="material-icons">close</i>
                </button>
                <span>{{ session('status') }}</span>
              </div>
            </div>
          </div>
        @endif
      <div class="row">
      <div class="col-md-2"></div>
        <div class="col-md-8">
          

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Add a Product') }}</h4>
              </div>
              <div class="card-body ">
                
                <div class="row">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('product_name') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('product_name') ? ' is-invalid' : '' }}"
                            name="product_name" id="input-product_name" type="text" placeholder="{{ __('Product Name') }}"
                            value="{{ old('product_name') }}" required="true" aria-required="true" />
                        @if ($errors->has('product_name'))
                        <span id="name-error" class="error text-danger"
                            for="input-product_name">{{ $errors->first('product_name') }}</span>
                        @endif
                    </div>
                  </div>
                  <div class="col-sm-3"></div>
                </div>
                <div class="row">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('brand_name') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('brand_name') ? ' is-invalid' : '' }}"
                            name="brand_name" id="input-brand_name" type="text" placeholder="{{ __('Brand Name') }}"
                            value="{{ old('brand_name') }}" required="true" aria-required="true" />
                        @if ($errors->has('brand_name'))
                        <span id="name-error" class="error text-danger"
                            for="input-brand_name">{{ $errors->first('brand_name') }}</span>
                        @endif
                    </div>
                  </div>
                  <div class="col-sm-3"></div>
                </div>
                <div class="row">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('unit') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('unit') ? ' is-invalid' : '' }}"
                            name="unit" id="input-unit" type="text" placeholder="{{ __('In Hand Quantity') }}"
                            value="{{ old('unit') }}" required="true" aria-required="true" />
                        @if ($errors->has('unit'))
                        <span id="name-error" class="error text-danger"
                            for="input-unit">{{ $errors->first('unit') }}</span>
                        @endif
                    </div>
                  </div>
                  <div class="col-sm-3"></div>
                </div>
                <div class="row">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('sku_number') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('sku_number') ? ' is-invalid' : '' }}"
                            name="sku_number" id="input-sku_number" type="text" placeholder="{{ __('SKU Number') }}"
                            value="{{ old('sku_number') }}" required="true" aria-required="true" />
                        @if ($errors->has('sku_number'))
                        <span id="name-error" class="error text-danger"
                            for="input-sku_number">{{ $errors->first('sku_number') }}</span>
                        @endif
                    </div>
                  </div>
                  <div class="col-sm-3"></div>
                </div>
                <div class="row">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('unit_price') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('unit_price') ? ' is-invalid' : '' }}"
                            name="unit_price" id="input-unit_price" type="text" placeholder="{{ __('Unit Price') }}"
                            value="{{ old('unit_price') }}" required="true" aria-required="true" />
                        @if ($errors->has('unit_price'))
                        <span id="name-error" class="error text-danger"
                            for="input-unit_price">{{ $errors->first('unit_price') }}</span>
                        @endif
                    </div>
                  </div>
                  <div class="col-sm-3"></div>
                </div>
                <div class="row">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('purchase_price') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('purchase_price') ? ' is-invalid' : '' }}"
                            name="purchase_price" id="input-purchase_price" type="text" placeholder="{{ __('Purchase Price') }}"
                            value="{{ old('purchase_price') }}" required="true" aria-required="true" />
                        @if ($errors->has('purchase_price'))
                        <span id="name-error" class="error text-danger"
                            for="input-purchase_price">{{ $errors->first('purchase_price') }}</span>
                        @endif
                    </div>
                  </div>
                  <div class="col-sm-3"></div>
                </div>
                <div class="row">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                       <textarea class="form-control" rows="4" cols="50" placeholder="Product Description" name="description"></textarea>
                        @if ($errors->has('description'))
                        <span id="name-error" class="error text-danger"
                            for="input-description">{{ $errors->first('description') }}</span>
                        @endif
                    </div>
                  </div>
                  <div class="col-sm-3"></div>
                </div>
                <div class="">
                            <button type="submit" class="btn btn-primary">{{ __('Add Product') }}</button>
                            </div>
                
              </div>
              
            </div>
          
        </div>
      <div class="col-md-2"></div>
      </div>
    </div>
  