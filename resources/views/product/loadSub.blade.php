@if(count($SubCategories) > 0)
<label for="sub_category_id" class="col-form-label">Sub Category</label>
<div class="form-group{{ $errors->has('sub_category_id') ? ' has-danger' : '' }}">
  {{ Form::select('sub_category_id', $SubCategories, null, array('required','class'=>'form-control', 'placeholder'=>'Select Sub Category','id' => 'subCategoryId','style'=>'-webkit-appearance: menulist;')) }}
  @if ($errors->has('sub_category_id'))
    <span id="sub_category_id-error" class="error text-danger" for="input-sub_category_id">{{ $errors->first('sub_category_id') }}</span>
  @endif
</div>
@else
    <li>No Sub Categories Linked</li>
@endif
