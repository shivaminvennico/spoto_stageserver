@if(count($Year) > 0)
<div class="form-group{{ $errors->has('vehicle_year') ? ' has-danger' : '' }}">
  {{ Form::select('vehicle_year', $Year, null, array('required','class'=>'form-control', 'placeholder'=>'Select Year','id' => 'VehicleYear','style'=>'-webkit-appearance: menulist;')) }}
  @if ($errors->has('vehicle_year'))
    <span id="vehicle_year-error" class="error text-danger" for="input-vehicle_year">{{ $errors->first('vehicle_year') }}</span>
  @endif
</div>
@else
    <div>No Year is added</li>
@endif
