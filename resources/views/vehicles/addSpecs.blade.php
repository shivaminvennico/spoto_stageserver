@extends('layouts.master')
@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/toastr.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/ladda-themeless.min.css')}}">
@endsection
@section('main-content')
          <div class="breadcrumb">
                <h1>Add Specification</h1>
          </div>

            <div class="separator-breadcrumb border-top"></div>

      
        @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="nav-icon i-Close-Window"></i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif

      <div class="row">
        <div class="col-md-4">
          <form method="post" action="{{ route('store_specification') }}" autocomplete="off" class="form-horizontal">
            @csrf

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Add Parent Specification') }}</h4>
              </div>
              <div class="card-body ">
                <div class="row">
                  <label class="col-sm-4 col-form-label">{{ __('Name') }}</label>
                  <div class="col-sm-8">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"  type="text" placeholder="{{ __('Parent Specification') }}" value="{{ old('name', '') }}" required="true" aria-required="true"/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-4 col-form-label">{{ __('Mandatory') }}</label>
                  <div class="col-sm-8">
                  <label class="checkbox checkbox-primary">
                                <input type="checkbox" name="is_mandatory">
                                
                                <span class="checkmark"></span>
                            </label>
                  </div>
                </div>
              </div>
              <div class="card-footer text-center">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
  
        <div class="col-md-4">
          <form method="post" action="{{ route('store_sub_specification') }}" autocomplete="off" class="form-horizontal">
            @csrf

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Add Sub Specification') }}</h4>
              </div>
              <div class="card-body ">
                <div class="row">
                  <label class="col-sm-5 col-form-label">{{ __('Sub Specification') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('specification_id') ? ' has-danger' : '' }}">
                    {{ Form::select('specification_id', $ParentSpecification, null, array('required','class'=>'form-control', 'placeholder'=>'Select Sub Specification','style'=>'-webkit-appearance: menulist;')) }}
                      @if ($errors->has('specification_id'))
                        <span id="specification_id-error" class="error text-danger" for="input-specification_id">{{ $errors->first('specification_id') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group{{ $errors->has('sub_name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('sub_name') ? ' is-invalid' : '' }}" name="sub_name"  type="text" placeholder="{{ __('Sub Specification') }}" value="{{ old('sub_name', '') }}" required="true" aria-required="true"/>
                      @if ($errors->has('sub_name'))
                        <span id="sub_name-error" class="error text-danger" for="input-sub_name">{{ $errors->first('sub_name') }}</span>
                      @endif
                      @if(session()->has('sub_name'))
                        <span id="sub_name-error" class="error text-danger" for="input-sub_name">{{ session()->get('sub_name') }}</span> 
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer text-center">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>


     <div class="separator-breadcrumb "></div>


      <div class="row">
        <div class="col-md-4">
        <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Parent Specification') }}</h4>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-12">
                    <ul id="parent_specifications">
                    @foreach($ViewSpecification as $parent)
                        <li id="specs{{$parent->id}}" data-id="{{$parent->id}}" class="parent-specification">{{$parent->name}}<i></i></li>
                        <div  class="input-specification" id="div-specs{{$parent->id}}"  style="display:none;">
                        <div class="row">
                        <input type='text' id="input-specs{{$parent->id}}" value='{{$parent->name}}' style="width:120px;" class="form-control"/>
                        <?php $checked = "";
                        if ($parent->is_mandatory == 1) {
                          $checked = "checked";
                        }?>
                        <label class="checkbox checkbox-primary ml-2">
                                <input type="checkbox" id="edit_is_mandatory{{$parent->id}}" {{$checked}}>
                                
                                <span class="checkmark" style="margin-top: 6px;"></span>
                            </label>
                             
                        <button onclick="EditSpecification('{{$parent->id}}');" class="btn btn-sm btn-primary ladda-button example-button ml-1 mt-1 mb-1" data-style="expand-left">
                          <span class="ladda-label">Submit</span>
                        </button>
                        </div>
                        </div>
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            </div>
        </div>

        <div class="col-md-4">
        <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Sub Specification') }}</h4>
              </div>
              <div class="card-body">
                <div id="loadSubSpecs" class="row">
                  
                </div>
              </div>
            </div>
        </div>

      



@endsection
@section('page-js')
<script src="{{asset('assets/js/vendor/toastr.min.js')}}"></script>
<script src="{{asset('assets/js/toastr.script.js')}}"></script>
<script src="{{asset('assets/js/vendor/spin.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/ladda.js')}}"></script>
<script src="{{asset('assets/js/ladda.script.js')}}"></script>
@endsection