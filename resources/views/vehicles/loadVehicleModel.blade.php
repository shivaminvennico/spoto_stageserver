@if(count($Model) > 0)
<div class="form-group{{ $errors->has('make_model') ? ' has-danger' : '' }}">
  {{ Form::select('make_model', $Model, null, array('required','class'=>'form-control', 'placeholder'=>'Select Model','id' => 'makeModel','style'=>'-webkit-appearance: menulist;')) }}
  @if ($errors->has('make_model'))
    <span id="make_model-error" class="error text-danger" for="input-make_model">{{ $errors->first('make_model') }}</span>
  @endif
</div>
@else
    <div>No Model is added</li>
@endif
