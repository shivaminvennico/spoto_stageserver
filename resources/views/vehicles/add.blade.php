@extends('layouts.master')
@section('main-content')
    @if(session('status'))
    <div class="row">
        <div class="col-sm-12">
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="nav-icon i-Close-Window"></i>
            </button>
            <span>{{ session('status') }}</span>
        </div>
        </div>
    </div>
    @endif
    @if(session('duplicate_error'))
    <div class="row">
        <div class="col-sm-12">
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="nav-icon i-Close-Window"></i>
            </button>
            <span>{{ session()->get('duplicate_error') }}</span>
        </div>
        </div>
    </div>
    @endif
<div class="row">
        <div class="col-lg-6 col-md-12 col-sm-6">
            <div class="card mb-6">
                <div class="card-header">
                    Vehicle Corner
                </div>
                <div class="card-body">
                    <h5 class="card-title">Excel</h5>
                    <p class="card-text">To import vehicles data using excel file please download the format.
                    </p>
                    <button type="button" class="btn btn-primary btn-rounded" data-toggle="modal" data-target="#uploadFile">
                        Upload File
                    </button>
                    
                    <a href="{{route('downloadExcel')}}"  class="btn btn-primary btn-rounded">Download Format</a>
                </div>
            </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-6">
            <div class="card mb-6">
                <div class="card-header">
                    Vehicle Corner
                </div>
                <div class="card-body">
                    <h5 class="card-title">Manually</h5>
                    <p class="card-text">Description of Manually module shown here.
                    </p>
                    <a href="{{route('vehicle.add')}}" class="btn btn-primary btn-rounded">Go Manually</a>
                </div>
            </div>
            </div>
      
</div>
<div class="separator-breadcrumb"></div>
        
                <div class="col-md-16">
                    <div class="card text-left">

                        <div class="card-body">
                            <h4 class="card-title mb-3">Vehicle Corner</h4>
                            <p>Here you can manage vehicles</p>
                            <div class="table-responsive">
                            	
                            	<!-- <a class="btn btn-raised ripple btn-primary m-1 float-right" href="{{ route('user.create') }}">ADD VEHICLE</a> -->
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">{{ __('No') }}</th>
                                            <th scope="col">Variant Name</th>
                                            <th scope="col">Make</th>
                                            <th scope="col">Model</th>
                                            <th scope="col">Year</th>
                                            
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php $counter=1; ?>
                                      @foreach($Vehicles as $vehicle)
                                        <tr class="h5 vehicleRows" data-id="{{$vehicle->id}}" style="cursor:pointer;">
                                         
                                            <th scope="row">{{$counter++}}</th>
                                            <td>
                                            	{{ $vehicle->variant_name }}
                            					
                                            </td>
                                            <td>{{ $vehicle->make }}</td>
                                            <td>
                                            	{{ $vehicle->model }}
                                            </td>

                                            <td>{{ $vehicle->year }}</td>
                                            
                            				
                                            <td>
                                 
                            <a rel="tooltip" class="btn btn-success" href="{{route('vehicle.edit',$vehicle)}}" data-original-title="" title="">
                                    <i class="nav-icon i-Pen-2"></i>
                                    <div class="ripple-container"></div>
                            </a>
                            
                                            </td>
                                        </tr>
                                      @endforeach
                                       
                                    </tbody>
                                </table>
                                {{$Vehicles->links()}}
                            </div>


                        </div>
                    </div>
                </div>
                
                <div class="modal fade" id="uploadFile" tabindex="-1" role="dialog" aria-labelledby="uploadFileTitle-2" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="uploadFileTitle-2">Upload File</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form class="form" method="POST" enctype="multipart/form-data" action="{{route('upload.excel')}}">
                        <div class="modal-body">
                            @csrf   
                            <input type="file" id="vehicle_file" name="vehicle_file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>             
@endsection
@section('page-js')
<script src="{{asset('assets/js/modal.script.js')}}"></script>
@endsection