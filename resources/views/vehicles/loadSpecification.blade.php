<div class="col-sm-12">
<ul id="sub_specifications" >
@if(count($SubSpecifications) > 0)
  @foreach($SubSpecifications as $parent)
    <li id="sub-specs{{$parent->id}}"  class="sub-specification">{{$parent->sub_name}}<i></i></li>
      <div  class="input-sub-specification" id="div-sub-specs{{$parent->id}}"  style="display:none;">
        <div class="row">
          <input type='text' id="input-sub-specs{{$parent->id}}" value='{{$parent->sub_name}}'  style="width:120px;" class="form-control"/>
          <button onclick="EditSubSpecification('{{$parent->id}}');" class="btn btn-sm btn-primary ladda-button example-button m-1" data-style="expand-left">
            <span class="ladda-label">Submit</span>
          </button>
        </div>
      </div>
  @endforeach
@else
    <li>No Sub Specifications Linked</li>
@endif
</ul>
</div>