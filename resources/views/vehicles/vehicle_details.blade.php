@extends('layouts.master')
@section('main-content')
          <div class="breadcrumb">
                <h1>Vehicle Corner</h1>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
            <div class="col-md-12">
                  <div class="card ">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ __('Vehicle Details') }}</h4>
                            <p class="card-category"></p>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="{{route('vehicle.index')}}"
                                        class="btn btn-sm btn-primary">{{ __('BACK TO LIST') }}</a>
                                </div>
                            </div>
                            <section class="col-md-6" style="float: left">
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{ __('Variant Name') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                        <label class="col-form-label">{{$vehicleDetails[0]->variant_name}}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{ __('Make') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                        <label class="col-form-label">{{$vehicleDetails[0]->make}}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{ __('Model') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                        <label class="col-form-label">{{$vehicleDetails[0]->model}}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{ __('Years') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                        <label class="col-form-label">{{$vehicleDetails[0]->year}}</label>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="col-md-6" style="float: right">
                            @foreach($vehicleDetails as $vehicle)
                                <div class="row">
                                    <label class="col-sm-3 col-form-label">{{$vehicle->name}}</label>
                                    <?php $subString=[];
                                        foreach($subSpecs as $sub)
                                        {
                                            if($vehicle->specs_id == $sub->specification_id)
                                            {
                                                $subString [] = $sub->sub_name;
                                            }
                                        }  
                                        $sub =   implode(', ',$subString) 
                                    ?>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                        <label class="col-form-label">{{$sub}}</label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </section>

                    </div>
            </div>
        </div>

@endsection