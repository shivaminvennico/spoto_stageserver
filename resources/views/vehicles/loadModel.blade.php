@if(isset($Model['new']))
    @if(count($Model) > 1)
    <div class="form-group{{ $errors->has('mmy_id') ? ' has-danger' : '' }}">
      {{ Form::select('mmy_id', $Model, null, array('required','class'=>'form-control','id'=>'mmy_id', 'placeholder'=>'Select Model','style'=>'-webkit-appearance: menulist;')) }}
      @if ($errors->has('mmy_id'))
        <span id="mmy_id-error" class="error text-danger" for="input-mmy_id">{{ $errors->first('mmy_id') }}</span>
      @endif
    </div>
    @else
    <div class="form-group{{ $errors->has('mmy_id') ? ' has-danger' : '' }}">
      <input type="text" name="mmy_id" id="mmy_id"  class="form-control" placeholder="Model" required/>
      @if ($errors->has('mmy_id'))
        <span id="mmy_id-error" class="error text-danger" for="input-mmy_id">{{ $errors->first('mmy_id') }}</span>
      @endif
    </div>
    @endif
@else
    @if(count($Model) > 0)
    <div class="form-group{{ $errors->has('mmy_id') ? ' has-danger' : '' }}">
      {{ Form::select('mmy_id', $Model, null, array('required','class'=>'form-control','id'=>'mmy_id', 'placeholder'=>'Select Model','style'=>'-webkit-appearance: menulist;','data-id'=>'1')) }}
      @if ($errors->has('mmy_id'))
        <span id="mmy_id-error" class="error text-danger" for="input-mmy_id">{{ $errors->first('mmy_id') }}</span>
      @endif
    </div>
    @else
    <li>No records found</li>
    @endif
@endif