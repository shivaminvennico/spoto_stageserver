@if(count($Variant) > 0)
<div class="form-group{{ $errors->has('vehicle_id') ? ' has-danger' : '' }}">
  {{ Form::select('vehicle_id[]', $Variant, null, array('required','class'=>'form-control','style'=>'-webkit-appearance: menulist;','multiple','id'=>"vehicleId")) }}
  @if ($errors->has('vehicle_id'))
    <span id="vehicle_id-error" class="error text-danger" for="input-vehicle_id">{{ $errors->first('vehicle_id') }}</span>
  @endif
</div>
@else
    <div>No Variant is added</li>
@endif
