@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/smart.wizard/smart_wizard_theme_arrows.min.css')}}">
@endsection
@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/toastr.css')}}">
@endsection
@section('main-content')
<div class="breadcrumb">
   <h1>Vehicles Corner</h1>
</div>
<div class="separator-breadcrumb border-top"></div>
<!-- Add Specs -->
<form id="formVehicle" method="post" action="{{route('vehicle.update',$vehicle)}}" autocomplete="off" class="form-horizontal">
   @csrf
   <div class="content" id="addVehicleForm">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
               <div class="card ">
                  <div class="card-header card-header-primary">
                     <h4 class="card-title">{{ __('Edit Vehicle') }}</h4>
                  </div>
                  <div class="card-body ">
                     <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                           <div class="form-group{{ $errors->has('make') ? ' has-danger' : '' }}">
                              {{ Form::select('make', $Make,$vehicle->make, array('required','class'=>'form-control', 'placeholder'=>'Select Make','id' => 'make','style'=>'-webkit-appearance: menulist;','data-id'=>'1')) }}
                              @if ($errors->has('make'))
                              <span id="make-error" class="error text-danger" for="input-make">{{ $errors->first('make') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="col-sm-3"></div>
                     </div>
                     <div class="row">
                        <div class="col-sm-3"></div>
                        <div id="loadYear" class="col-sm-6">
                        @if(count($Year) > 0)
                            <div class="form-group{{ $errors->has('year') ? ' has-danger' : '' }}">
                            {{ Form::select('year', $Year, $vehicle->year, array('required','class'=>'form-control', 'placeholder'=>'Year','id' => 'makeYear','style'=>'-webkit-appearance: menulist;','data-id'=>'1')) }}
                            @if ($errors->has('year'))
                                <span id="year-error" class="error text-danger" for="input-year">{{ $errors->first('year') }}</span>
                            @endif
                            </div>
                        @else
                            <li>No records found</li>
                        @endif
                        </div>
                        <div class="col-sm-3"></div>
                     </div>
                    
                     <div class="row">
                        <div class="col-sm-3"></div>
                        <div id="loadModel" class="col-sm-6">
                        @if(count($Model) > 0)
                            <div class="form-group{{ $errors->has('mmy_id') ? ' has-danger' : '' }}">
                            {{ Form::select('mmy_id', $Model, $vehicle->mmy_id, array('required','class'=>'form-control','id'=>'mmy_id', 'placeholder'=>'Select Model','style'=>'-webkit-appearance: menulist;','data-id'=>'1')) }}
                            @if ($errors->has('mmy_id'))
                                <span id="mmy_id-error" class="error text-danger" for="input-mmy_id">{{ $errors->first('mmy_id') }}</span>
                            @endif
                            </div>
                        @else
                            <li>No records found</li>
                        @endif

                        </div>
                        <div class="col-sm-3"></div>
                     </div>
                     <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                           <div class="form-group{{ $errors->has('variant_name') ? ' has-danger' : '' }}">
                              <input class="form-control{{ $errors->has('variant_name') ? ' is-invalid' : '' }}"
                                 name="variant_name" id="input-variant_name" type="text" placeholder="{{ __('Variant Name') }}"
                                 value="{{ $vehicle->variant_name }}" required="true" aria-required="true" />
                              @if ($errors->has('variant_name'))
                              <span id="name-error" class="error text-danger"
                                 for="input-variant_name">{{ $errors->first('variant_name') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="col-sm-3"></div>
                     </div>
                  </div>
                  <div class="card-footer text-center">
                     <button type="button" class="btn btn-primary" id="showWizardSpecs">{{ __('Save') }}</button>
                  </div>
               </div>
            </div>
            <div class="col-md-2"></div>
         </div>
      </div>
   </div>
   <div class="row"  id="wizardSpecsForm" style="display:none">
      <div class="float-right m-2">
         <button type="button" class="btn btn-primary" id="backbtnOnWizard">{{ __('Back') }}</button>
      </div>
      <div class="col-md-12">
      
         <!-- SmartWizard html -->
         <div id="smartwizard" class="sw-main sw-theme-arrows">
            <ul  class='stepsProducts'>   
            <li><a href="#step-1">Step 1<br /><small>Mandatory Specification</small></a></li>      
            </ul>
            <div class="divStepsSpecification">
            </div>
         
         </div>
      </div>
   </div>
   </div>
   </div>

</form>
@endsection
@section('page-js')
<script>
 var counter = 0;
   $.ajax({
       url:"/loadSpecification",
       method:'get',
       success: function(data) {
         
          

           var len = 0;
           var decimal = 1;
           
           $('.divStepsSpecification').find('.row').html('');
           $('.divStepsSpecification').append('<div id="step-1" class=""><div class="row accordion" id="accordionRightIcon'+decimal+'"></div></div>');
           data.main_specification.forEach(val => {
              
               len++;
               if(len>4)
               {
                  
                   len=1;
                   decimal++;
                   
                   
                   $('#step-1').append('<div class="row mt-3 accordion" id="accordionRightIcon'+(decimal)+'"></div>');
                  
               }
               
               $('#accordionRightIcon'+decimal).append(
                   '<div class="col-md-3"><div class="card"><div class="card-header header-elements-inline"><h6 class="card-title ul-collapse__icon--size ul-collapse__right-icon mb-0"><a data-toggle="collapse" class="text-default collapsed" href="#accordion-item-icon-right-'+val.id+'" aria-expanded="false">'+val.name+'</a></h6></div><div id="accordion-item-icon-right-'+val.id+'" class="collapse " data-parent="#accordionRightIcon'+decimal+'"><div class="card-body sub_specs_body'+val.id+' mainSpecs'+val.id+' required" ></div></div></div></div>'

                );
               
               
           });

           len=0;
           decimal+=1;
           
           data.basic_specification.forEach(val => {
               if(len==0)
               {
                  $('.stepsProducts').append('<li><a href="#step-2">Step 2<br /><small>Optional Specification</small></a></li>');
                  $('.divStepsSpecification').append('<div id="step-2" class=""><div class="row accordion" id="accordionRightIcon'+decimal+'"></div></div>');
               }
               len++;
               if(len>4)
               {
                  
                   len=1;
                   decimal++;
                   
                   
                   $('#step-2').append('<div class="row mt-3 accordion" id="accordionRightIcon'+(decimal)+'"></div>');
                   
               }
               
               $('#accordionRightIcon'+decimal).append(
                   '<div class="col-md-3"><div class="card"><div class="card-header header-elements-inline"><h6 class="card-title ul-collapse__icon--size ul-collapse__right-icon mb-0"><a data-toggle="collapse" class="text-default collapsed" href="#accordion-item-icon-right-'+val.id+'" aria-expanded="false">'+val.name+'</a></h6></div><div id="accordion-item-icon-right-'+val.id+'" class="collapse " data-parent="#accordionRightIcon'+decimal+'"><div class="card-body sub_specs_body'+val.id+'"></div></div></div></div>'

                );
               
               
           });
       
           $.each(data.sub_specification,function(i,val){
            var checked = "";
            if ($.inArray(val.id,<?php echo $subSpecs; ?>) !=  '-1') {
                checked = "checked";
            }
             $('.sub_specs_body'+val.specification_id).append('<label class="checkbox checkbox-primary"><input type="checkbox" data-id="'+val.specification_id+'" name="sub_specification[]" value="'+val.id+'" '+checked+'><span>'+val.sub_name+'</span><span class="checkmark"></span></label>');
               
           });
           
           
       },
       complete: function () {
           if($('#smartwizard ul li').length > 0){
               var smartWiz1 = document.createElement('script');
               smartWiz1.setAttribute('src','/assets/js/vendor/jquery.smartWizard.min.js');
               document.head.appendChild(smartWiz1)
   
               var smartWiz2 = document.createElement('script');
               smartWiz2.setAttribute('src','/assets/js/smart.wizard.script.js');
               document.head.appendChild(smartWiz2)
               
                counter = $('div.required').length;
                
           }
           
       }
   });
   
   $(document).ready(function(){
    
   $("#formVehicle").validate({
       errorClass: 'text-danger'
   });
 var loadSubmitButton = 0;
   $('#formVehicle').on('click',function(){
       
    if (loadSubmitButton==0) {
        $('#submitFinish').css('display','');
    }
    loadSubmitButton+=1;
   });
    
  
    $(document).on('click','input[type="checkbox"]',function() {
       var cls = $(this).parent('label').parent('div').attr('class');

       if (~cls.indexOf("required")) {
               var length =  $('div.required').length;
               var total = $('div.mainSpecs'+$(this).data('id')+' :checkbox:checked').length;
            
               if ($(this).prop("checked")==true && total==1) {
                  
                  counter++;
                  
                  
               }
               else if($(this).prop("checked")==false && total==0)
               {
                  counter--;          
                  
               }
      
         
            if($(this).prop("checked")==true && counter==length)
            {
               $('#submitFinish').css('display','');
            }
            else if(counter<length){
               $('#submitFinish').css('display','none');
            }
          
       }
       
   });
   
   });
</script>
<script src="{{asset('assets/js/vendor/toastr.min.js')}}"></script>
<script src="{{asset('assets/js/toastr.script.js')}}"></script>
@endsection
@section('bottom-js')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
@endsection