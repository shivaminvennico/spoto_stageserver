@if(isset($Year[1]))
    @if(count($Year) > 1 )
    <div class="form-group{{ $errors->has('year') ? ' has-danger' : '' }}">
      {{ Form::select('year', $Year, null, array('required','class'=>'form-control', 'placeholder'=>'Year','id' => 'makeYear','style'=>'-webkit-appearance: menulist;')) }}
      @if ($errors->has('year'))
        <span id="year-error" class="error text-danger" for="input-year">{{ $errors->first('year') }}</span>
      @endif
    </div>
    @else
    <div class="form-group{{ $errors->has('year') ? ' has-danger' : '' }}">
      <input type="text" name="year" id="makeYear"  class="form-control" placeholder="Year" onkeyup='digits();' required/>
      @if ($errors->has('year'))
        <span id="year-error" class="error text-danger" for="input-year">{{ $errors->first('year') }}</span>
      @endif
    </div>
    @endif
@else
    @if(count($Year) > 0 )
    <div class="form-group{{ $errors->has('year') ? ' has-danger' : '' }}">
      {{ Form::select('year', $Year, null, array('required','class'=>'form-control', 'placeholder'=>'Year','id' => 'makeYear','style'=>'-webkit-appearance: menulist;','data-id'=>'1')) }}
      @if ($errors->has('year'))
        <span id="year-error" class="error text-danger" for="input-year">{{ $errors->first('year') }}</span>
      @endif
    </div>
    @else
    <li>No records found</li>
    @endif
@endif
