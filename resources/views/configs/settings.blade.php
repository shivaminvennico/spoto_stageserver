@extends('layouts.master')
@section('main-content')
<div class="breadcrumb">
   <h1>Configurations</h1>
</div>
<div class="separator-breadcrumb border-top"></div>
@if(session('status'))
<div class="row">
   <div class="col-sm-12">
      <div class="alert alert-success">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
         <i class="nav-icon i-Close-Window"></i>
         </button>
         <span>{{ session('status') }}</span>
      </div>
   </div>
</div>
@endif
@if(session('error'))
<div class="row">
   <div class="col-sm-12">
      <div class="alert alert-danger">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
         <i class="nav-icon i-Close-Window"></i>
         </button>
         <span>{{ session('error') }}</span>
      </div>
   </div>
</div>
@endif

   <!-- Tier's Layout started-->
   <div class="card ul-card__margin-25">
    <div class="card-body">
    <!-- Tab Structure Start -->
    <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="tier-labels" data-toggle="tab" href="#tier" role="tab" aria-controls="tier" aria-selected="true">Tier Labels</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="warehouse-tab" data-toggle="tab" href="#warehouse-settings" role="tab" aria-controls="warehouse-settings" aria-selected="false">Warehouse</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="brands-tab" data-toggle="tab" href="#brands-settings" role="tab" aria-controls="brands-settings" aria-selected="false">Brands</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="tier" role="tabpanel" aria-labelledby="tier-labels">
                <form action="{{route('settings.update')}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <label for="tier_one_name" class="ul-form__label ul-form--margin col-lg-1   col-form-label ">Tier 1
                        Name:</label>
                        <div class="col-lg-3">
                        <input type="text" class="form-control" id="tier_one_name" name="tier_one_name" placeholder="Enter Tier 1 Name" value="{{$labels->tier_1}}">
                        </div>
                        <label for="tier_two_name" class="ul-form__label ul-form--margin col-lg-1 col-form-label ">Tier 2 Name:</label>
                        <div class="col-lg-3">
                        <input type="text" class="form-control" id="tier_two_name" name="tier_two_name" placeholder="Enter Tier 2 Name" value="{{$labels->tier_2}}">
                        </div>
                    </div>
                    <div class="custom-separator"></div>
                    <div class="form-group row">
                        <label for="tier_three_name" class="ul-form__label ul-form--margin col-lg-1 col-form-label ">Tier 3 Name:</label>
                        <div class="col-lg-3">
                        <input type="text" class="form-control" id="tier_three_name" name="tier_three_name" placeholder="Enter Tier 3 Name" value="{{$labels->tier_3}}">
                        </div>
                        <label for="tier_four_name" class="ul-form__label ul-form--margin col-lg-1   col-form-label ">Tier 4
                        Name:</label>
                        <div class="col-lg-3">
                        <input type="text" class="form-control" id="tier_four_name" name="tier_four_name" placeholder="Enter Tier 4 Name" value="{{$labels->tier_4}}">
                        </div>
                    </div>
                    <div class="custom-separator"></div>
                    <div class="form-group row">
                        <label for="tier_five_name" class="ul-form__label ul-form--margin col-lg-1 col-form-label ">Tier 5 Name:</label>
                        <div class="col-lg-3">
                        <input type="text" class="form-control" id="tier_five_name" name="tier_five_name" placeholder="Enter Tier 5 Name" value="{{$labels->tier_5}}">
                        </div>
                        <label for="tier_six_name" class="ul-form__label ul-form--margin col-lg-1 col-form-label ">Tier 6 Name:</label>
                        <div class="col-lg-3">
                        <input type="text" class="form-control" id="tier_six_name" name="tier_six_name" placeholder="Enter Tier 6 Name" value="{{$labels->tier_6}}">
                        </div>
                    </div>
                    <div class="custom-separator"></div>
                    <div class="form-group row">
                        <label for="tier_seven_name" class="ul-form__label ul-form--margin col-lg-1   col-form-label ">Tier 7
                        Name:</label>
                        <div class="col-lg-3">
                        <input type="text" class="form-control" id="tier_seven_name" name="tier_seven_name" placeholder="Enter Tier 7 Name" value="{{$labels->tier_7}}">
                        </div>
                        <label for="tier_eight_name" class="ul-form__label ul-form--margin col-lg-1 col-form-label ">Tier 8 Name:</label>
                        <div class="col-lg-3">
                        <input type="text" class="form-control" id="tier_eight_name" name="tier_eight_name" placeholder="Enter Tier 8 Name" value="{{$labels->tier_8}}">
                        </div>
                    </div>
                    <div class="custom-separator"></div>
                    <div class="form-group row">
                        <label for="tier_nine_name" class="ul-form__label ul-form--margin col-lg-1 col-form-label ">Tier 9 Name:</label>
                        <div class="col-lg-3">
                        <input type="text" class="form-control" id="tier_nine_name" name="tier_nine_name" placeholder="Enter Tier 9 Name" value="{{$labels->tier_9}}">
                        </div>
                        <label for="tier_ten_name" class="ul-form__label ul-form--margin col-lg-1   col-form-label ">Tier 10
                        Name:</label>
                        <div class="col-lg-3">
                        <input type="text" class="form-control" id="tier_ten_name" name="tier_ten_name" placeholder="Enter Tier 10 Name" value="{{$labels->tier_10}}">
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="mc-footer">
                            <div class="row text-center">
                            <div class="col-lg-12 ">
                                <button type="submit" class="btn btn-primary m-1">Save</button>
                            </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="warehouse-settings" role="tabpanel" aria-labelledby="warehouse-tab">
                <div class="table-responsive">
                            	
                            	<a class="btn btn-raised ripple btn-primary m-1 float-right" data-toggle="modal" data-target="#addWarehouseModal" style="color:white;">ADD WAREHOUSE</a>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">{{ __('No') }}</th>
                                            <th scope="col">Warehouse Name</th>
                                            <th scope="col">Location</th>
                                            <th scope="col">City</th>
                                            <th scope="col">Contact No</th>
                                            <th scope="col">Contact Person</th>
                                            <th scope="col">Operating Hrs</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $counter=1; ?>
                                      @foreach($warehouses as $warehouse)
                                        <tr class="h5 warehouse">
                                         
                                            <th scope="row">{{$counter++}}</th>
                                            <td class="name">
                                            	{{ $warehouse->name }}
                            					
                            				</td>
                                            <td class="location">
                                            	{{ $warehouse->location }}
                                            </td>

                                            <td class="city">{{ $warehouse->city }}</td>
                                           
                                            <td class="contact_no">{{ $warehouse->contact_no }}</td>
                                    
                                            <td class="contact_person">{{ $warehouse->contact_person }}</td>

                                            <td class="operating_time">{{ $warehouse->open_time }} - {{ $warehouse->close_time }}</td>
                                            <td>
                                                
                                                <a rel="tooltip" class="btn btn-success" style="color:white;" id="editWarehouseItem" data-item-id="{{$warehouse->id}}">
                                                        <i class="nav-icon i-Pen-2"></i>
                                                        <div class="ripple-container"></div>
                                                </a>
                                                
                                            </td>
                                   
                                        </tr>
                                      @endforeach

                                       
                                    </tbody>
                                </table>
                </div>
            </div>
            <div class="tab-pane fade" id="brands-settings" role="tabpanel" aria-labelledby="brands-tab">
            <form action="{{route('brands.store')}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <label for="brand_name_settings" class="ul-form__label ul-form--margin col-lg-1   col-form-label ">Brand
                        Name</label>
                        <div class="col-lg-3">
                        <input type="text" class="form-control" id="brand_name_settings" name="brand_name_settings" placeholder="Enter Brand Name">
                        </div>
                        <button class="btn btn-primary">Save</button>
                    </div>
            </form>

            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">{{ __('No') }}</th>
                                            <th scope="col">Brand Name</th>
                                            
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $counter=1; ?>
                                      @foreach($brands as $brand)
                                        <tr class="h5 brand">
                                         
                                            <th scope="row">{{$counter++}}</th>
                                            <td class="brand_name">
                                            	{{ $brand->name }}
                            					
                            				</td>
                                            <td>
                                                
                                                <a rel="tooltip" class="btn btn-success" style="color:white;" id="editBrandsItem" data-item-id="{{$brand->id}}">
                                                        <i class="nav-icon i-Pen-2"></i>
                                                        <div class="ripple-container"></div>
                                                </a>
                                                
                                            </td>
                                   
                                        </tr>
                                      @endforeach

                                       
                                    </tbody>
                                </table>
                </div>
            </div>

            <!-- Add Warehouse Modal -->
            <div class="modal fade" id="addWarehouseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Warehouse</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <form action="{{route('warehouse.store')}}" method="post">
                        @csrf
                            <div class="modal-body">
                                <div class="form-group row">
                                    <label for="warehouse_name" class="ul-form__label ul-form--margin col-lg-5   col-form-label ">Warehouse Name</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="warehouse_name" name="warehouse_name" required placeholder="Warehouse Name">
                                        
                                    </div>                                          
                                </div>

                                <div class="form-group row">
                                    <label for="warehouse_location" class="ul-form__label ul-form--margin col-lg-5   col-form-label ">Warehouse Location</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="warehouse_location" name="warehouse_location" required placeholder="Warehouse Location">
                                        
                                    </div>                                          
                                </div>

                                <div class="form-group row">
                                    <label for="warehouse_city" class="ul-form__label ul-form--margin col-lg-5   col-form-label ">Warehouse City</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="warehouse_city" name="warehouse_city" required placeholder="Warehouse City">
                                        
                                    </div>                                          
                                </div>

                                <div class="form-group row">
                                    <label for="warehouse_contact_no" class="ul-form__label ul-form--margin col-lg-5   col-form-label ">Warehouse Contact No.</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="warehouse_contact_no" name="warehouse_contact_no" required placeholder="Warehouse Contact No.">
                                        
                                    </div>                                          
                                </div>

                                <div class="form-group row">
                                    <label for="warehouse_contact_person" class="ul-form__label ul-form--margin col-lg-5   col-form-label ">Warehouse Contact Person</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="warehouse_contact_person" name="warehouse_contact_person" required placeholder="Warehouse Contact Person">
                                        
                                    </div>                                          
                                </div>

                                <div class="form-group row">
                                    <label for="warehouse_operating_hrs_start" class="ul-form__label ul-form--margin col-lg-5   col-form-label ">Warehouse Operating Hrs</label>
                                    <div class="col-lg-2 mt-2">
                                    From
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="time" class="form-control" id="warehouse_operating_hrs_start" name="warehouse_operating_hrs_start" required>
                                        
                                    </div>
                                    
                                                                          
                                </div>

                                <div class="form-group row">
                                    <label for="warehouse_operating_hrs_end" class="ul-form__label ul-form--margin col-lg-5   col-form-label "></label>
                                    <div class="col-lg-2 mt-2">
                                    To
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="time" class="form-control" id="warehouse_operating_hrs_end" name="warehouse_operating_hrs_end" required>
                                        
                                    </div>
                                    
                                                                          
                                </div>

                            </div>
                            <div class="modal-footer">
                               
                                <button class="btn btn-primary">Add Warehouse</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- End Add Warehouse Modal -->

             <!-- Edit Warehouse Modal -->
             <div class="modal fade" id="editWarehouseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit Warehouse</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <form action="{{route('warehouse.update')}}" method="post">
                        @csrf
                            <div class="modal-body">
                                <input type="hidden" id="update_id" name="id" />
                                <div class="form-group row">
                                    <label for="edit_warehouse_name" class="ul-form__label ul-form--margin col-lg-5   col-form-label ">Warehouse Name</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="edit_warehouse_name" name="edit_warehouse_name" required placeholder="Warehouse Name">
                                        
                                    </div>                                          
                                </div>

                                <div class="form-group row">
                                    <label for="edit_warehouse_location" class="ul-form__label ul-form--margin col-lg-5   col-form-label ">Warehouse Location</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="edit_warehouse_location" name="edit_warehouse_location" required placeholder="Warehouse Location">
                                        
                                    </div>                                          
                                </div>

                                <div class="form-group row">
                                    <label for="edit_warehouse_city" class="ul-form__label ul-form--margin col-lg-5   col-form-label ">Warehouse City</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="edit_warehouse_city" name="edit_warehouse_city" required placeholder="Warehouse City">
                                        
                                    </div>                                          
                                </div>

                                <div class="form-group row">
                                    <label for="edit_warehouse_contact_no" class="ul-form__label ul-form--margin col-lg-5   col-form-label ">Warehouse Contact No.</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="edit_warehouse_contact_no" name="edit_warehouse_contact_no" required placeholder="Warehouse Contact No.">
                                        
                                    </div>                                          
                                </div>

                                <div class="form-group row">
                                    <label for="edit_warehouse_contact_person" class="ul-form__label ul-form--margin col-lg-5   col-form-label ">Warehouse Contact Person</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="edit_warehouse_contact_person" name="edit_warehouse_contact_person" required placeholder="Warehouse Contact Person">
                                        
                                    </div>                                          
                                </div>

                                <div class="form-group row">
                                    <label for="edit_warehouse_operating_hrs_start" class="ul-form__label ul-form--margin col-lg-5   col-form-label ">Warehouse Operating Hrs</label>
                                    <div class="col-lg-2 mt-2">
                                    From
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="time" class="form-control" id="edit_warehouse_operating_hrs_start" name="edit_warehouse_operating_hrs_start" required>
                                        
                                    </div>
                                    
                                                                          
                                </div>

                                <div class="form-group row">
                                    <label for="edit_warehouse_operating_hrs_end" class="ul-form__label ul-form--margin col-lg-5   col-form-label "></label>
                                    <div class="col-lg-2 mt-2">
                                    To
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="time" class="form-control" id="edit_warehouse_operating_hrs_end" name="edit_warehouse_operating_hrs_end" required>
                                        
                                    </div>
                                    
                                                                          
                                </div>

                            </div>
                            <div class="modal-footer">
                               
                                <button class="btn btn-primary">Edit Warehouse</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- End Edit Warehouse Modal -->

            <!-- Edit Brands Modal -->
            <div class="modal fade" id="editBrandsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit Brand</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <form action="{{route('brands.update')}}" method="post">
                        @csrf
                            <div class="modal-body">
                                <input type="hidden" id="brand_update_id" name="id" />
                                <div class="form-group row">
                                    <label for="edit_brands_name" class="ul-form__label ul-form--margin col-lg-3   col-form-label ">Brand Name</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="edit_brands_name" name="edit_brands_name" required placeholder="Brand Name">
                                        
                                    </div>                                          
                                </div>


                            </div>
                            <div class="modal-footer">
                               
                                <button class="btn btn-primary">Edit Brand</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<!-- Tab Structure Ends -->
      </div>
      
   </div>
   <!-- Tier's Layout Finished-->
@endsection
@section('page-js')
<script>
var id = window.location.hash.substr(1);
        $('#'+id).click();
            $(window).on('hashchange', function(e){
                var new_id = window.location.hash.substr(1);
                $('#'+new_id).click();
            });
</script>
@endsection
