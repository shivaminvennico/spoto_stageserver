@extends('layouts.master')
@section('main-content')
          <div class="breadcrumb">
                <h1>User Management</h1>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
            	<div class="col-md-12">
            		<form method="post" action="{{ route('manager.update', $user) }}" autocomplete="off"
                    class="form-horizontal">
                    @csrf
                    @method('put')

                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ __('Edit Manager') }}</h4>
                            <p class="card-category"></p>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="{{ route('manager.index') }}"
                                        class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label">{{ __('First Name') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                            name="first_name" id="input-first_name" type="text"
                                            placeholder="{{ __('First Name') }}"
                                            value="{{ old('first_name', $user->first_name) }}" required="true"
                                            aria-required="true" />
                                        @if ($errors->has('first_name'))
                                        <span id="first_name-error" class="error text-danger"
                                            for="input-first_name">{{ $errors->first('first_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label">{{ __('Last Name') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                            name="last_name" id="input-last_name" type="text"
                                            placeholder="{{ __('Last Name') }}"
                                            value="{{ old('last_name', $user->last_name) }}" required="true"
                                            aria-required="true" />
                                        @if ($errors->has('last_name'))
                                        <span id="last_name-error" class="error text-danger"
                                            for="input-last_name">{{ $errors->first('last_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label">{{ __('Mobile No') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('mobile_no') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('mobile_no') ? ' is-invalid' : '' }}"
                                            name="mobile_no" id="input-mobile_no" type="text"
                                            placeholder="{{ __('Mobile No') }}"
                                            value="{{ old('mobile_no', $user->mobile_no) }}"
                                            onKeyPress="return isNumberKey(event)" maxlength="10" required />
                                        @if ($errors->has('mobile_no'))
                                        <span id="mobile_no-error" class="error text-danger"
                                            for="input-mobile_no">{{ $errors->first('mobile_no') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label">{{ __('Manager Roles') }}</label>
                                <ul style=" columns: 3 12em; ">
                                    @if(!empty($loggedInUser->getAllPermissions()))
                                    @foreach($loggedInUser->getAllPermissions() as $role)
                                    <li>
                                        <label>
                                            <input class="name" type="checkbox" name="Permissions[]" value="{{$role->name}}" 
                                            	@if(in_array($role->id, $rolePermissions)) checked @endif
											> {{$role->name}}
                                        </label>
                                    </li>
                                    @endforeach
                                    @endif
                                    @if ($errors->has('Permissions'))
                                </ul>
                                <span id="Permissions-error" class="error text-danger"
                                    for="input-Permissions">{{ $errors->first('Permissions') }}</span>
                                    @endif
                                </div>
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            	</div>
        	</div>
@endsection