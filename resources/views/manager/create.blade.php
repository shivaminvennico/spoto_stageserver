@extends('layouts.master')
@section('main-content')
    <div class="breadcrumb">
        <h1>User Management</h1>
        <ul>
            <li><a href=""></a></li>
            
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">

                

                <form method="post" action="{{ route('manager.store') }}" autocomplete="off" class="form-horizontal">
                    @csrf
                    @method('post')

                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ __('Add Manager') }}</h4>
                            <p class="card-category"></p>
                        </div>

                        <div class="card-body ">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="{{ route('manager.index') }}"
                                        class="btn btn-sm btn-primary">{{ __('BACK TO LIST') }}</a>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('First Name') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                            name="first_name" id="input-first_name" type="text"
                                            placeholder="{{ __('First Name') }}" value="{{ old('first_name') }}"
                                            required="true" aria-required="true" />
                                        @if ($errors->has('first_name'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-first_name">{{ $errors->first('first_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Last Lame') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                            name="last_name" id="input-last_name" type="text"
                                            placeholder="{{ __('Last Name') }}" value="{{ old('last_name') }}"
                                            required="true" aria-required="true" />
                                        @if ($errors->has('last_name'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-last_name">{{ $errors->first('last_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Email') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                            name="email" id="input-email" type="email" placeholder="{{ __('Email') }}"
                                            value="{{ old('email') }}" required />
                                        @if ($errors->has('email'))
                                        <span id="email-error" class="error text-danger"
                                            for="input-email">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label">{{ __('Manager Roles') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('getRoleNames') ? ' has-danger' : '' }}">
                                    <ul style=" columns: 3 12em; ">
                                        @if(!empty($user->getAllPermissions()))
                                        @foreach($user->getAllPermissions() as $role)
                                        <li><label><form><input type="checkbox" name="getAllPermissions[]" value="{{$role->name}}" > {{$role->name}}</form></li></label>
                                        @endforeach
                                        @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label"
                                    for="input-password">{{ __(' Password') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                            input type="password" name="password" id="input-password"
                                            placeholder="{{ __('Password') }}" value="" required />
                                        @if ($errors->has('password'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-name">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label"
                                    for="input-password-confirmation">{{ __('Confirm Password') }}</label>
                                <div class="col-sm-7">
                                     <div class="form-group">
                                        <input class="form-control" name="confirm-password"
                                            id="input-password-confirmation" type="password"
                                            placeholder="{{ __('Confirm Password') }}" value="" required />
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">{{ __('Add Manager') }}</button>
                            </div>
                        </div>
                </form>
                        </div>
                    </div>
                </div>
            </div>
    

@endsection


