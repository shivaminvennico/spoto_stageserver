<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::view('seller', 'auth.seller_register');
Route::post('seller','Auth\SellerRegisterController@create')->name('seller_register');

//Verification Routes..
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

//Reset Password
Route::get('password/reset','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::group(['middleware' => ['auth','verified']], function () {
Route::get('/', function () {
    return view('dashboard.dashboardv1');
});
    Route::resource('user', 'UserController', ['except' => ['show']]);
    Route::resource('manager', 'ManagerController', ['except' => ['show']]);

    //Configurations
    Route::get('settings','ConfigurationController@settings')->name('settings');
    Route::post('settings/update','ConfigurationController@settingsUpdate')->name('settings.update');
    Route::post('warehouse/store','ConfigurationController@warehouseStore')->name('warehouse.store');
    Route::post('warehouse/update','ConfigurationController@warehouseUpdate')->name('warehouse.update');
    Route::post('brands/store','ConfigurationController@brandStore')->name('brands.store');
    Route::post('brands/update','ConfigurationController@brandUpdate')->name('brands.update');

    // Bundle Products
    Route::get('/bundle/product/add','BundleProductController@add')->name('bundle.add');
    Route::post('/bundle/product/add/loadProducts','BundleProductController@loadProducts')->name('bundle.add.load');
    Route::post('/bundle/product/store','BundleProductController@store')->name('bundle.store');
    Route::post('/bundle/vehicle/mapped/request','BundleProductController@getMappedVehicles')->name('bundle.mapped.vehicles');
    
    // Affiliate
    Route::get('/affiliate/connection','AffiliateProductController@connection')->name('admin.affiliate.connection');
    Route::post('/affiliate/get/partner/name','AffiliateProductController@getPartnerName')->name('admin.affiliate.connection.partner.name');
    Route::post('/affiliate/establish/connection','AffiliateProductController@establishedConnection')->name('admin.affiliate.connection.establish');
    Route::get('/affiliate/products/request','AffiliateProductController@index')->name('affiliate.request');
    Route::get('/affiliate/reject/connection/{id}','AffiliateProductController@rejectConnection')->name('affiliate.reject.connection');
    Route::get('/affiliate/accept/connection/{id}','AffiliateProductController@acceptConnection')->name('affiliate.accept.connection');
    Route::post('/affiliate/set/connection/default/price','AffiliateProductController@defaultPrice')->name('affiliate.set.connection.default.price');
    Route::get('/affiliate/products/your/request','AffiliateProductController@yourRequest')->name('affiliate.your.request');
    Route::get('/affiliate/give/access/products/{id}','AffiliateProductController@giveAccess')->name('affiliate.give.access');
    Route::post('/affiliate/products/give/access/requester','AffiliateProductController@affiliateStore')->name('affiliate.product.give.access.store');
    Route::get('/affiliate/get/requester_details/{id}','AffiliateProductController@requesterDetails')->name('affiliate.get.requester.details');
    Route::get('affiliate/product/edit/{id}','ProductController@affiliateEdit')->name('affiliate.products.edit');
    Route::post('affiliate/product/update/{id}','ProductController@affiliateUpdate')->name('affiliate.products.update');

    //Product
    Route::get('/product','ProductController@loadProducts')->name('product');
    Route::get('/product_desc','ProductController@loadProductDesc')->name('productdesc');
    Route::post('/product/store','ProductController@store')->name('product.store');
    Route::get('/product/add','ProductController@add')->name('product.add');
    Route::get('/get/product_details/{id}','ProductController@get_product_details')->name('product.get_product_details');
    Route::post('/product/image','ProductController@image')->name("product.image");
    Route::post('/product/image/remove','ProductController@imageRemove')->name("product.image.remove");
    Route::get('/product/add/searchBar','ProductController@searchBar')->name("product.add.searchBar");
    Route::get('/product/add/searchBar/fetchVehicleData','ProductController@fetchVehicle')->name("product.add.searchBar.fetchVehicleData");
    Route::get('/product/edit/{id}','ProductController@edit')->name('products.edit');
    Route::post('/product/update/{id}','ProductController@update')->name('products.update');

    //Category in Product
    Route::post('/get/sub_category','CategoryController@getSub')->name('get_sub');
    Route::post('/get/sub_sub_category','CategoryController@getSubSub')->name('get_sub_sub');

    //Vehicle in Product
    Route::post('/get/vehicle_model','VehicleController@getVehicleModel')->name('get_vehicle_model');
    Route::post('/get/vehicle_year','VehicleController@getVehicleYear')->name('get_vehicle_year');
    Route::post('/get/vehicle_variant','VehicleController@getVehicleVariant')->name('get_vehicle_variant');

    //Import Vehicle
    Route::get('/download','CreateExcelController@downloadExcel')->name('downloadExcel');
    Route::post('/upload/excel','ImportVehicleController@import')->name('upload.excel');

    //Products
    Route::get('product/download','CreateExcelController@downloadProductExcel')->name('products.downloadExcel');
    Route::post('/upload/product/excel','ImportProductController@import')->name('upload.products.excel');
});


// Route::view('/', 'starter')->name('starter');
Route::get('large-compact-sidebar/dashboard/dashboard1', function () {
    // set layout sesion(key)
    session(['layout' => 'compact']);
    return view('dashboard.dashboardv1');
})->name('compact');

Route::get('large-sidebar/dashboard/dashboard1', function () {
    // set layout sesion(key)
    session(['layout' => 'normal']);
    return view('dashboard.dashboardv1');
})->name('normal');

Route::get('horizontal-bar/dashboard/dashboard1', function () {
    // set layout sesion(key)
    session(['layout' => 'horizontal']);
    return view('dashboard.dashboardv1');
})->name('horizontal');

//sideBar in dashboard
//Users
// Route::get('user','UserController@index')->name('user.index');
// Route::get('user/create','UserController@create')->name('user.create');
// Route::post('user/store','UserController@store')->name('user.store');
// Route::any('user/{user}/edit','UserController@edit')->name('user.edit');
// Route::put('user/{user}/update','UserController@update')->name('user.update');
// Route::delete('user/{user}/delete','UserController@delete')->name('user.delete');

// //Manager
// Route::get('manager','ManagerController@index')->name('manager.index');
// //Route::get('manager/create','ManagerController@create')->name('manager.create');
// // Route::post('manager/store','UserController@store')->name('manager.store');
// Route::any('manager/{user}/edit','ManagerController@edit')->name('manager.edit');
// Route::put('manager/{user}/update','ManagerController@update')->name('manager.update');
// //Route::delete('manager/{user}/delete','ManagerController@delete')->name('manager.delete');





//Roles
// Route::get('role','RoleController@index')->name('role.index');
// Route::get('role/create','RoleController@create')->name('role.create');
// Route::post('role/store','RoleController@store')->name('role.store');
// Route::any('role/{user}/edit','RoleController@edit')->name('role.edit');
// Route::patch('role/{user}/update','RoleController@update')->name('role.update');
// Route::delete('role/{user}/delete','RoleController@delete')->name('role.delete');



//admin routes
Route::group(['middleware' => ['role:Admin']], function () {
    Route::resource('role','RoleController');
    
    Route::post('/get/make_year','VehicleController@getMake')->name('get_make');
    // Route::post('/get/make_to_year','VehicleController@getToMake')->name('get_to_make');
    Route::post('/get/year_model','VehicleController@getModel')->name('get_year_make');
    Route::post('/store/mmy','VehicleController@storeMmy');
    
    //Specification
    Route::get('add/specification','SpecificationController@create')->name('add_specification');
    Route::post('add/specification','SpecificationController@store')->name('store_specification');
    Route::post('add/sub_specification','SpecificationController@storeSub')->name('store_sub_specification');
    Route::post('/get/sub_specification','SpecificationController@getSub')->name('get_sub_specs');
    Route::post('/get/edit_specs_input','SpecificationController@edit_specs_input')->name('edit_specs_input');
    Route::post('/get/edit_sub_specs_input','SpecificationController@edit_sub_specs_input')->name('edit_sub_specs_input');

    //Category
    Route::get('add/category','CategoryController@create')->name('add_category');
    Route::post('add/category','CategoryController@store')->name('store_category');
    Route::post('add/sub_category','CategoryController@storeSub')->name('store_sub_category');
    Route::post('add/sub_sub_category','CategoryController@storeSubSub')->name('store_sub_sub_category');
    Route::post('/get/edit_cat_input','CategoryController@edit_cat_input')->name('edit_cat_input');
    Route::post('/get/edit_sub_cat_input','CategoryController@edit_sub_cat_input')->name('edit_sub_cat_input');
    Route::post('/get/edit_sub_sub_cat_input','CategoryController@edit_sub_sub_cat_input')->name('edit_sub_sub_cat_input');

    //Vehicle
    Route::get('vehicle','VehicleController@index')->name('vehicle.index');
    Route::get('/vehicle/add','VehicleController@add')->name('vehicle.add');
    Route::post('/vehicle/add','VehicleController@store')->name('vehicle.store');
    Route::get('/get/vehicle_details/{id}','VehicleController@get_vehicle_details')->name('vehicle.get_vehicle_details');
    Route::get('vehicle/{id}/edit','VehicleController@edit')->name('vehicle.edit');
    Route::post('vehicle/{id}/update','VehicleController@update')->name('vehicle.update');
    Route::get('/loadSpecification','SpecificationController@loadSpecification')->name('vehicle.loadSpecification');    
});

Route::view('dashboard/dashboard1', 'dashboard.dashboardv1')->name('dashboard_version_1');
Route::view('dashboard/dashboard2', 'dashboard.dashboardv2')->name('dashboard_version_2');
Route::view('dashboard/dashboard3', 'dashboard.dashboardv3')->name('dashboard_version_3');
Route::view('dashboard/dashboard4', 'dashboard.dashboardv4')->name('dashboard_version_4');

// uiKits
Route::view('uikits/alerts', 'uiKits.alerts')->name('alerts');
Route::view('uikits/accordion', 'uiKits.accordion')->name('accordion');
Route::view('uikits/buttons', 'uiKits.buttons')->name('buttons');
Route::view('uikits/badges', 'uiKits.badges')->name('badges');
Route::view('uikits/bootstrap-tab', 'uiKits.bootstrap-tab')->name('bootstrap-tab');
Route::view('uikits/carousel', 'uiKits.carousel')->name('carousel');
Route::view('uikits/collapsible', 'uiKits.collapsible')->name('collapsible');
Route::view('uikits/lists', 'uiKits.lists')->name('lists');
Route::view('uikits/pagination', 'uiKits.pagination')->name('pagination');
Route::view('uikits/popover', 'uiKits.popover')->name('popover');
Route::view('uikits/progressbar', 'uiKits.progressbar')->name('progressbar');
Route::view('uikits/tables', 'uiKits.tables')->name('tables');
Route::view('uikits/tabs', 'uiKits.tabs')->name('tabs');
Route::view('uikits/tooltip', 'uiKits.tooltip')->name('tooltip');
Route::view('uikits/modals', 'uiKits.modals')->name('modals');
Route::view('uikits/NoUislider', 'uiKits.NoUislider')->name('NoUislider');
Route::view('uikits/cards', 'uiKits.cards')->name('cards');
Route::view('uikits/cards-metrics', 'uiKits.cards-metrics')->name('cards-metrics');
Route::view('uikits/typography', 'uiKits.typography')->name('typography');

// extra kits
Route::view('extrakits/dropDown', 'extraKits.dropDown')->name('dropDown');
Route::view('extrakits/imageCroper', 'extraKits.imageCroper')->name('imageCroper');
Route::view('extrakits/loader', 'extraKits.loader')->name('loader');
Route::view('extrakits/laddaButton', 'extraKits.laddaButton')->name('laddaButton');
Route::view('extrakits/toastr', 'extraKits.toastr')->name('toastr');
Route::view('extrakits/sweetAlert', 'extraKits.sweetAlert')->name('sweetAlert');
Route::view('extrakits/tour', 'extraKits.tour')->name('tour');
Route::view('extrakits/upload', 'extraKits.upload')->name('upload');


// Apps
Route::view('apps/invoice', 'apps.invoice')->name('invoice');
Route::view('apps/inbox', 'apps.inbox')->name('inbox');
Route::view('apps/chat', 'apps.chat')->name('chat');
Route::view('apps/calendar', 'apps.calendar')->name('calendar');
Route::view('apps/task-manager-list', 'apps.task-manager-list')->name('task-manager-list');
Route::view('apps/task-manager', 'apps.task-manager')->name('task-manager');
Route::view('apps/toDo', 'apps.toDo')->name('toDo');
Route::view('apps/ecommerce/products', 'apps.ecommerce.products')->name('ecommerce-products');
Route::view('apps/ecommerce/product-details', 'apps.ecommerce.product-details')->name('ecommerce-product-details');
Route::view('apps/ecommerce/cart', 'apps.ecommerce.cart')->name('ecommerce-cart');
Route::view('apps/ecommerce/checkout', 'apps.ecommerce.checkout')->name('ecommerce-checkout');


Route::view('apps/contacts/lists', 'apps.contacts.lists')->name('contacts-lists');
Route::view('apps/contacts/contact-details', 'apps.contacts.contact-details')->name('contact-details');
Route::view('apps/contacts/grid', 'apps.contacts.grid')->name('contacts-grid');
Route::view('apps/contacts/contact-list-table', 'apps.contacts.contact-list-table')->name('contact-list-table');

// forms
Route::view('forms/basic-action-bar', 'forms.basic-action-bar')->name('basic-action-bar');
Route::view('forms/multi-column-forms', 'forms.multi-column-forms')->name('multi-column-forms');
Route::view('forms/smartWizard', 'forms.smartWizard')->name('smartWizard');
Route::view('forms/tagInput', 'forms.tagInput')->name('tagInput');
Route::view('forms/forms-basic', 'forms.forms-basic')->name('forms-basic');
Route::view('forms/form-layouts', 'forms.form-layouts')->name('form-layouts');
Route::view('forms/form-input-group', 'forms.form-input-group')->name('form-input-group');
Route::view('forms/form-validation', 'forms.form-validation')->name('form-validation');
Route::view('forms/form-editor', 'forms.form-editor')->name('form-editor');

// Charts
Route::view('charts/echarts', 'charts.echarts')->name('echarts');
Route::view('charts/chartjs', 'charts.chartjs')->name('chartjs');
Route::view('charts/apexLineCharts', 'charts.apexLineCharts')->name('apexLineCharts');
Route::view('charts/apexAreaCharts', 'charts.apexAreaCharts')->name('apexAreaCharts');
Route::view('charts/apexBarCharts', 'charts.apexBarCharts')->name('apexBarCharts');
Route::view('charts/apexColumnCharts', 'charts.apexColumnCharts')->name('apexColumnCharts');
Route::view('charts/apexRadialBarCharts', 'charts.apexRadialBarCharts')->name('apexRadialBarCharts');
Route::view('charts/apexRadarCharts', 'charts.apexRadarCharts')->name('apexRadarCharts');
Route::view('charts/apexPieDonutCharts', 'charts.apexPieDonutCharts')->name('apexPieDonutCharts');
Route::view('charts/apexSparklineCharts', 'charts.apexSparklineCharts')->name('apexSparklineCharts');
Route::view('charts/apexScatterCharts', 'charts.apexScatterCharts')->name('apexScatterCharts');
Route::view('charts/apexBubbleCharts', 'charts.apexBubbleCharts')->name('apexBubbleCharts');
Route::view('charts/apexCandleStickCharts', 'charts.apexCandleStickCharts')->name('apexCandleStickCharts');
Route::view('charts/apexMixCharts', 'charts.apexMixCharts')->name('apexMixCharts');

// datatables
Route::view('datatables/basic-tables', 'datatables.basic-tables')->name('basic-tables');

// sessions
Route::view('sessions/signIn', 'sessions.signIn')->name('signIn');
Route::view('sessions/signUp', 'sessions.signUp')->name('signUp');
Route::view('sessions/forgot', 'sessions.forgot')->name('forgot');

// widgets
Route::view('widgets/card', 'widgets.card')->name('widget-card');
Route::view('widgets/statistics', 'widgets.statistics')->name('widget-statistics');
Route::view('widgets/list', 'widgets.list')->name('widget-list');
Route::view('widgets/app', 'widgets.app')->name('widget-app');
Route::view('widgets/weather-app', 'widgets.weather-app')->name('widget-weather-app');

// others
Route::view('others/notFound', 'others.notFound')->name('notFound');
Route::view('others/user-profile', 'others.user-profile')->name('user-profile');
Route::view('others/starter', 'starter')->name('starter');
Route::view('others/faq', 'others.faq')->name('faq');
Route::view('others/pricing-table', 'others.pricing-table')->name('pricing-table');
Route::view('others/search-result', 'others.search-result')->name('search-result');
