<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductWarehouse extends Model
{
    protected $fillable = ['product_id','qty','warehouse','qty_in_hand'];
}
