<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubSpecification extends Model
{
    protected $fillable = ['specification_id','sub_name'];
}
