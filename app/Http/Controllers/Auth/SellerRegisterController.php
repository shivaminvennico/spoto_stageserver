<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Storage;
use Auth;

class SellerRegisterController extends Controller
{
    use RegistersUsers;


     /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $ip_address =  \Request::getClientIp();
        $request->validate([
            'first_name' => 'required|alpha|max:255',
            'last_name' => 'required|alpha|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|string|min:8|confirmed',
            'seller_type' => 'required|in:1,2,3',
            'seller_documents.*' => 'required|mimes:doc,docx,pdf,jpg,jpeg,png|max:2000',
        ]);
        $files = $request->file('seller_documents');
        $data = $request->all();    
        $seller  = $data['seller_type'];
        $role_type = '';
        $user_type = 0;
        $fileNames = array();
        $BackEndUser = 0;
        $input_files = '';

        if($seller == 1){
            $role_type = 'Manufacturer';
            $user_type = 2;
            
        }elseif($seller == 2){
            $role_type = 'Importer';
            $user_type = 3;

        }elseif($seller == 3) {
            $role_type = 'Distributor';
            $user_type = 4;
        }
 
        if($request->hasFile('seller_documents'))
        {
            foreach ($files as $file) {
                $name = time().'_'. $file->getClientOriginalName();
                $filePath = $role_type.'/' . $name;
                Storage::disk('s3')->put($filePath, file_get_contents($file));
                $fileNames[] = $name;
            }
            $input_files = implode(',',$fileNames);
        }
       
        $CheckUser = User::select('id','first_name')->where('email',$data['email'])->where('user_type',5)->first();
        if(isset($CheckUser->id)){
            $CheckUser->user_type = $user_type;
            $CheckUser->documents = $input_files;
            $CheckUser->ip_address = $ip_address;
            $CheckUser->assignRole($role_type);
            $BackEndUser = $CheckUser->save();
            return redirect()->route('login')->withStatus(__('Account created please login.'));
        }else{
            $BackEndUser = User::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'user_type'=>$user_type,
                'documents' => $input_files,
                'ip_address'=>$ip_address,
            ]);
            $BackEndUser->assignRole($role_type);
            $BackEndUser->sendEmailVerificationNotification();
            if (Auth::attempt($request->only('email', 'password'))){
                return redirect()->route('/');
            }else{
                return redirect()->route('login')->withStatus(__('Account created successfully please check your email.'));
            }
        }
        
        
    }
}
