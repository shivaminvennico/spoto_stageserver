<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use Illuminate\Support\Facades\Validator;
use Response;
use App\Product;

class CategoryController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:category-management', ['only' => ['create','store','edit_cat_input','edit_sub_cat_input','edit_sub_sub_cat_input','storeSub','storeSubSub']]);
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ParentCategories = Category::orderBy('name')->pluck('name', 'id');
        $SubCategories = SubCategory::orderBy('sub_name')->pluck('sub_name', 'id');
        $ViewCategories = Category::all();
        
        return view('category.add',compact('ParentCategories','SubCategories','ViewCategories'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:categories|regex:/^[\pL\s\-]+$/u|max:80'
        ]);
        $Insert = Category::create($request->all());
        return back()->withStatus(__('Parent category added successfully.'));
    }

    public function edit_cat_input(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:categories|regex:/^[\pL\s\-]+$/u|max:80'
        ]);
        $includeInProduct = Product::where('category_id',$request->parent)->count();
        if($includeInProduct>0)
        {
            return 'dublicate';
        }
        else
        {
            Category::where('id',$request->parent)->update(['name'=>$request->name]);
            return $request->all();
        }
    }

    public function edit_sub_cat_input(Request $request)
    {
        $request->validate([
            'parentValue' => 'required|regex:/^[\pL\s\-]+$/u|max:80'
        ]);
        $includeInProduct = Product::where('sub_category_id',$request->parent)->count();
        if($includeInProduct>0)
        {
            return 'dublicate';
        }
        else
        {
            SubCategory::where('id',$request->parent)->update(['sub_name'=>$request->parentValue]);
            return $request->all();
        }
        
    }

    public function edit_sub_sub_cat_input(Request $request)
    {
        $request->validate([
            'subParentValue' => 'required|regex:/^[\pL\s\-]+$/u|max:80'
        ]);

        $includeInProduct = Product::where('sub_sub_category_id',$request->parent)->count();
        if($includeInProduct>0)
        {
            return 'dublicate';
        }
        else
        {
            SubSubCategory::where('id',$request->parent)->update(['sub_sub_name'=>$request->subParentValue]);
            return $request->all();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeSub(Request $request)
    {
        $ParentIds = implode(',',Category::pluck('id')->toArray());
        $request->validate([
            'sub_name' => 'required|regex:/^[\pL\s\-]+$/u|max:80',
            'category_id' => 'required|in:'.$ParentIds
        ]);
        
        $checkDuplicate = SubCategory::where('category_id',$request->category_id)
        ->where('sub_name',$request->sub_name)->count();    

            if($checkDuplicate > 0){
                return redirect()->back()->with('sub_name', 'A Sub Category with same combination exists.');
            }else{
                $Insert = SubCategory::create($request->only(['category_id', 'sub_name']));
                return back()->withStatus(__('Sub category added successfully.'));
            }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeSubSub(Request $request)
    {
        // echo '<pre>';print_r($request->all());die;

        $SubCategoryIds = implode(',',SubCategory::pluck('id')->toArray());
        $request->validate([
            'sub_sub_name' => 'required|regex:/^[\pL\s\-]+$/u|max:80',
            'sub_category_id' => 'required|in:'.$SubCategoryIds
        ]);
        
        $checkDuplicate = SubSubCategory::where('sub_category_id',$request->sub_category_id)
        ->where('sub_sub_name',$request->sub_sub_name)->count();    

            if($checkDuplicate > 0){
                return redirect()->back()->with('sub_sub_name', 'A Sub Sub Category with same combination exists.');
            }else{
                $Insert = SubSubCategory::create($request->only(['sub_category_id', 'sub_sub_name']));
                return back()->withStatus(__('Sub Sub category added successfully.'));
            }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getSub(Request $request)
    {
        $SubCategories = SubCategory::select('sub_name','id')->where('category_id',$request->parent)->get();
        if(isset($request->product) && $request->product==true){
            $SubCategories = $SubCategories->pluck('sub_name', 'id');
            return view('product.loadSub',compact('SubCategories'));
        }else{
            return view('category.loadSub',compact('SubCategories'));
        }

    }
    public function getSubSub(Request $request)
    {
        $SubSubCategories = SubSubCategory::select('sub_sub_name','id')->where('sub_category_id',$request->sub_parent)->get();

        if(isset($request->product) && $request->product==true){
            $SubSubCategories = $SubSubCategories->pluck('sub_sub_name', 'id');
            return view('product.loadSubSub',compact('SubSubCategories'));
        }else{
            return view('category.loadSubSub',compact('SubSubCategories'));
        }

    }
}
