<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Specification;
use App\SubSpecification;
use Illuminate\Support\Facades\Storage;


class CreateExcelController extends Controller
{
    public function downloadExcel()
    {
        if($url = Storage::disk('s3')->url('Excel_Template/Vehicle_Import.xlsx'))
        {
            $input = file_get_contents($url);
            file_put_contents('Vehicle_Import.xlsx',$input);
            $file =  'Vehicle_Import.xlsx';
            
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
            $sheet = $spreadsheet->getActiveSheet();
            $lastLoadedColumn = $sheet->getHighestColumn();
            $lastCellValue = $sheet->getCell($lastLoadedColumn.'1')->getValue();
            $specsId = Specification::latest()->first();
            $astrick = substr($lastCellValue,0,1);
            if ($astrick == '*') {
                $lastCellValue = substr($lastCellValue,1);
            }
            if ($lastCellValue == $specsId->name) {
               
            }
            else {
                $locationFile = 'template/Vehicle_Import_Format.xlsx';
                $existingSpreadsheet = IOFactory::load("template/Vehicle_Import_Format.xlsx");
                
                $sheet = $existingSpreadsheet->getActiveSheet();
                $lastLoadedColumn = $sheet->getHighestColumn();
                $storedLastColumn = $lastLoadedColumn;
                
                


                $existingSpecification = Specification::select('id','name','is_mandatory')->get();
                $subSpecArr=[];
                foreach($existingSpecification as $key => $val){
                    
                    $getSubSpecifications = SubSpecification::where('specification_id',$val->id)->get();
                    foreach($getSubSpecifications as $key => $sub)
                    {
                        
                        if($key>0)
                        {
                            $subSpecArr[$val->name] .= ', '. $sub->sub_name;
                        }
                        else{
                            $subSpecArr[$val->name] = $sub->sub_name;
                        }
                    
                    
                        
                    }
                    $lastLoadedColumn++;
                    if($val->is_mandatory == 1)
                    {
                        $sheet->setCellValue($lastLoadedColumn.'1', '*'.$val->name);
                    }
                    else{
                        $sheet->setCellValue($lastLoadedColumn.'1', $val->name);
                    }
                    
                    $validation = $sheet->getCell($lastLoadedColumn.'2')->getDataValidation();
                    $validation->setType( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST );
                    
                    $validation->setAllowBlank(false);
                    $validation->setShowInputMessage(true);
                    $validation->setShowErrorMessage(false);
                    $validation->setShowDropDown(true);
                    $validation->setErrorTitle('Input error');
                    $validation->setError('Value is not in list.');
                    $validation->setPromptTitle('Pick from list');
                    $validation->setPrompt('Please pick a value from the drop-down list.');
                    $validation->setFormula1('"'.$subSpecArr[$val->name].'"');
                    $sheet->setDataValidation($lastLoadedColumn."2:".$lastLoadedColumn."65536", $validation);
                }
                
                $validation = $sheet->getCell('C2')->getDataValidation();
                $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_WHOLE);
                $validation->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP );
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setErrorTitle('Input error');
                $validation->setError(' Enter Only Numbers !');
                $validation->setPromptTitle('Allowed input');
                $validation->setPrompt('Only numbers are allowed.');
            
                $sheet->setDataValidation("C2:C65536", $validation);
            
            
                $writer = new Xlsx($existingSpreadsheet);
                $path = storage_path('template/');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                $writer->save($path.'Vehicle_Import.xlsx');
                $filePath = 'Excel_Template/Vehicle_Import.xlsx';
                Storage::disk('s3')->put($filePath, file_get_contents($path.'Vehicle_Import.xlsx'));
                unlink($path.'Vehicle_Import.xlsx');
            }

            $filename = 'Vehicle_Import.xlsx';
            $tempImage = tempnam(sys_get_temp_dir(), $filename);

            copy('https://spoto-files.s3.us-east-2.amazonaws.com/Excel_Template/Vehicle_Import.xlsx', $tempImage);

            return response()->download($tempImage, $filename);
        }
        
    }

    public function downloadProductExcel()
    {
        $url = Storage::disk('s3')->url('Excel_Template/Product_Import.xlsx');
        if(empty($url))
        {
            $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('A1','Product Name');
            $sheet->setCellValue('B1','Brand Name');
            $sheet->setCellValue('C1','Category Name');
            $sheet->setCellValue('D1','Sub Category Name');
            $sheet->setCellValue('E1','Sub Sub Category Name');
            $sheet->setCellValue('F1','SKU Number');
            $sheet->setCellValue('G1','Unit Price');
            $sheet->setCellValue('H1','Purchase Price');
            $sheet->setCellValue('I1','Tax');
            $sheet->setCellValue('J1','Discount');
            $sheet->setCellValue('K1','Description');
            $sheet->setCellValue('L1','Free Shipping');
            $sheet->setCellValue('M1','Local Pickup Shipping Cost');
            $sheet->setCellValue('N1','Flat Rate Shipping Cost');
            $sheet->setCellValue('O1','Unit');
            $sheet->setCellValue('P1','Warehouse');
            $sheet->setCellValue('Q1','Qty in hand');
            
    
            $validation = $sheet->getCell('L2')->getDataValidation();
            $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_WHOLE);
            $validation->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP );
            $validation->setAllowBlank(false);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setErrorTitle('Input error');
            $validation->setError(' Enter Only 0 or 1!');
            $validation->setPromptTitle('Allowed input');
            $validation->setPrompt('0 for off Or 1 for on.');
            $validation->setFormula1('0');
            $validation->setFormula2('1');
            $sheet->setDataValidation("L2:L65536", $validation);
    
            $writer = new Xlsx($spreadsheet);
            $path = storage_path('template/');
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
    
            $writer->save($path.'Product_Import.xlsx');
            $filePath = 'Excel_Template/Product_Import.xlsx';
            Storage::disk('s3')->put($filePath, file_get_contents($path.'Product_Import.xlsx'));
            unlink($path.'Product_Import.xlsx');
            
        }

        $filename = 'Product_Import.xlsx';
        $tempImage = tempnam(sys_get_temp_dir(), $filename);

        copy('https://spoto-files.s3.us-east-2.amazonaws.com/Excel_Template/Product_Import.xlsx', $tempImage);

        return response()->download($tempImage, $filename);

    }
}
