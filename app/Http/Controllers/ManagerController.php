<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use App\Role;
use DB;

class ManagerController extends Controller
{
    public function index(User $model)
    {
        $currentuser = Auth::user()->getRoleNames();

        if($currentuser[0] == 'Admin'){
            $user_id = null;
        }else{
            $user_id = auth()->id();
        }
        return view('manager.index', ['users' => $model->where('id', '!=', auth()->id())
                ->where('user_type', '!=', 1)
                ->where('created_by','=',$user_id)
                ->paginate(10)]);
    }
    
    public function create()
    {
        $user = Auth::user();
        return view('manager.create',compact('user'));
    }

    /**
     * Store a newly created user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\RedirectResponse
     */

    public function store(Request $request)
    {
        
        $currentuser = Auth::user()->id;
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'getAllPermissions' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            
            ]);
            $data = $request->all();
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'created_by' =>$currentuser,
            'user_type' =>6
            ]);
            $user->sendEmailVerificationNotification();
            $user->assignRole('Manager');
            $getAllPermissions = $request->getAllPermissions;
        
          
            foreach ($getAllPermissions as $Permissions) {
                $user->givePermissionTo($Permissions);
            }
        
        return redirect()->route('manager.index')->withStatus(__('Manager successfully created.'));
    }


    public function edit($id)
    {    
        $user = User::find($id);
        $loggedInUser = Auth::user();
        $rolePermissions = DB::table("model_has_permissions")->where("model_has_permissions.model_id",$id)
            ->pluck('model_has_permissions.permission_id')
            ->all();
        return view('manager.edit', compact('loggedInUser','user','rolePermissions'));
    }


    public function update(Request $request,$id)
    {
            $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'mobile_no' => 'required|numeric'
            ]);
            
            $user = User::find($id);
            $user->first_name = $request->first_name;    
            $user->last_name = $request->last_name;    
            $user->mobile_no = $request->mobile_no;
            
            $user->save();    
            
           $user->syncPermissions($request->input('Permissions'));
        
        return redirect()->route('manager.index')->withStatus(__('User successfully updated.'));
    }
}
