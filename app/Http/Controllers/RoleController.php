<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use Spatie\Permission\Models\Permission;
use DB;

class RoleController extends Controller
{
    //
    	function __construct()
        {
             $this->middleware('permission:role-management');
        }

        public function index(Request $request)
        {
            $roles = Role::where('id','!=','1')->pluck('name','id');
            return view('roles.index',compact('roles'));
                
        }

        // public function create()
        // {
        //     $permission = Permission::get();
            
        //     return view('roles.create',compact('permission'));
        // }
    
    
        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        // public function store(Request $request)
        // {
        //     $this->validate($request, [
        //         'name' => 'required|unique:roles,name',
        //          'permission' => 'required',
        //     ]);
    
    
        //     $role = Role::create(['name' => $request->input('name')]);
        //     $role->syncPermissions($request->input('permission'));
    
    
        //     return redirect()->route('role.index')
        //                     ->with('status','Role created successfully');
        // }
    
        
        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $role = Role::find($id);
            $permission = Permission::get();
            $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id')
            ->all();
            return view('roles.edit',compact('role','permission','rolePermissions'));
        }
        
        
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $this->validate($request, [
                'permission' => 'required',
            ]);
            $role = Role::find($id);
            $role->syncPermissions($request->input('permission'));
            return redirect()->route('role.index')
                            ->withStatus(__('Permissions updated successfully'));
        }
        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function delete($id)
        {
            DB::table("roles")->where('id',$id)->delete();
            return redirect()->route('role.index')
                            ->with('status','Role deleted successfully');
        }
}
