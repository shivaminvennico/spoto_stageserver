<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\VehicleImport;
use App\Exports\VehicleExport;
use Maatwebsite\Excel\Facades\Excel;

class ImportVehicleController extends Controller
{
    /**
    * @return \Illuminate\Support\Collection
    */
  

    public function import(Request $request) 
    {
        if ($request->hasFile('vehicle_file')) {
            $usersImport = new VehicleImport();
            $usersImport->import($request->file('vehicle_file'));
        // I made function getErrors on UsersImport class:
           
        }
       
       $errorData = ($usersImport->getErrorsData());
       $error = 0;
       foreach ($errorData as $key => $value) {
         if (count($errorData[0]) < count($errorData[$key])) {
             $error = 1;
           }
       }
          
       if ($error == 0){
            return redirect()->route('vehicle.index')->with('status','Vehicles added Successfully');
        }
        
       return  Excel::download(new VehicleExport($errorData),'error_Export.xlsx');
    }
}
