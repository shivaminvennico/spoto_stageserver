<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\ProductImport;
use App\Exports\VehicleExport;
use Maatwebsite\Excel\Facades\Excel;

class ImportProductController extends Controller
{
    public function import(Request $request) 
    {
        if ($request->hasFile('product_file')) {
            $usersImport = new ProductImport();
            $usersImport->import($request->file('product_file'));
        
           
        }
       
        $errorData = ($usersImport->getErrorsData());
        $error = 0;
      foreach ($errorData as $key => $value) {
        if (count($errorData[0]) < count($errorData[$key])) {
            $error = 1;
          }
      }
         
   
    if ($error == 0){
        return redirect()->route('product')->withStatus(__('Product successfully added.'));
    }
    
       return  Excel::download(new VehicleExport($errorData),'error_Export.xlsx');
    }
}
