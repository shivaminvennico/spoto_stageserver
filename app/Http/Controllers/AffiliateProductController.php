<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;
use App\AffiliateConnection;
use App\AffiliateProduct;
use DB;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\TierLabel;
use App\Helpers\Email_sender;

class AffiliateProductController extends Controller
{
    public function connection()
    {
        $partner_type = ['2' => 'Manufacturer','3' => 'Importer', '4' => 'Distributer'];
        $affiliateConnection = AffiliateConnection::select(DB::raw("CONCAT(u.first_name,' ',u.last_name) as req_name"),DB::raw("CONCAT(urec.first_name,' ',urec.last_name) as rec_name"),'affiliate_connections.status')->leftjoin('users as u','u.id','affiliate_connections.requester_id')->leftjoin('users as urec','urec.id','affiliate_connections.receiver_id')->paginate(10);
        return view('affiliateProducts.connection',compact('partner_type','affiliateConnection'));
    }
    public function index()
    {
        $affiliateConnection = AffiliateConnection::select('affiliate_connections.id',DB::raw("CONCAT(u.first_name,' ',u.last_name) as req_name"),'u.mobile_no','u.email','r.name as user_type','u.state','affiliate_connections.status','affiliate_connections.requester_id')->leftjoin('users as u','u.id','affiliate_connections.requester_id')->leftjoin('roles as r','r.id','u.user_type')->where('affiliate_connections.receiver_id',Auth::id())->paginate(10);
        return view('affiliateProducts.index',compact('affiliateConnection'));
    }
    public function rejectConnection($id)
    {
        AffiliateConnection::where('id',$id)->update(['status'=>2]);
        $emails = User::select('users.email')->leftjoin('affiliate_connections as ac','ac.requester_id','users.id')->where('ac.id',$id)->first();
        $connectionEmail = ['message' => 'Your connection is rejected by receiver',
        'email' => $emails->email];

        Email_sender::sendConnectionEstablishedMail($connectionEmail);
    }

    public function acceptConnection($id)
    {
        AffiliateConnection::where('id',$id)->update(['status'=>1]);
        $emails = User::select('users.email')->leftjoin('affiliate_connections as ac','ac.requester_id','users.id')->where('ac.id',$id)->first();
        $connectionEmail = ['message' => 'Your connection is accepted by receiver',
        'email' => $emails->email];

        Email_sender::sendConnectionEstablishedMail($connectionEmail);
        $tier = TierLabel::where('user_id',Auth::id())->count();
        if ($tier == 0 ) {
           TierLabel::create(['user_id'=>Auth::id()]);
        }
        $price_label = TierLabel::select('tier_1','tier_2','tier_3','tier_4','tier_5','tier_6','tier_7','tier_8','tier_9','tier_10')->where('user_id',Auth::id())->get();
        $tier = [];
        foreach ($price_label as $key => $value) {

            $tier[$key] = $value;
        }
        return $tier;
    }

    public function defaultPrice(Request $request)
    {
        AffiliateConnection::where('receiver_id',Auth::id())->update(['tier_price'=>$request->id]);
    }

    public function yourRequest()
    {
        $affiliateConnection = AffiliateConnection::select('affiliate_connections.id',DB::raw("CONCAT(u.first_name,' ',u.last_name) as req_name"),'u.mobile_no','u.email','r.name as user_type','u.state','affiliate_connections.status')->leftjoin('users as u','u.id','affiliate_connections.receiver_id')->leftjoin('roles as r','r.id','u.user_type')->where('affiliate_connections.requester_id',Auth::id())->paginate(10);
        return view('affiliateProducts.yourRequest',compact('affiliateConnection'));
    }
    public function getPartnerName(Request $request)
    {
        $partner_name =  User::select("id", DB::raw("CONCAT(users.first_name,' ',users.last_name) as full_name"))->where('user_type',$request->parent)
        ->pluck('full_name', 'id');
        $dType = $request->dType;
        return view('affiliateProducts.loadPartnerName',compact('partner_name','dType'));
    }

    public function establishedConnection(Request $request)
    {
        $ac = AffiliateConnection::create(['requester_id'=>$request->req,'receiver_id'=>$request->rec,'status'=>0]);
        $emails = User::select('users.email')->orWhere('id',$request->req)->orWhere('id',$request->rec)->get();
        foreach ($emails as $key => $email) {
            $connectionEmail = array('message' => "New connection established by admin, please login and check your panel",
            'email' => $email->email);

            Email_sender::sendConnectionEstablishedMail($connectionEmail);
        }
        
   
    }

    public function giveAccess($id)
    {
        $Products = Product::select('products.id','products.product_name','b.name as brand_name','products.sku_number','products.unit','products.unit_price','c.name','sc.sub_name','ssc.sub_sub_name')->leftjoin('categories as c','c.id','products.category_id')->leftjoin('sub_categories as sc','sc.id','products.sub_category_id')->leftjoin('sub_sub_categories as ssc','ssc.id','products.sub_sub_category_id')->leftjoin('brands as b','b.id','products.brand_name')->where('created_by',Auth::id())->get();
        return view('affiliateProducts.loadAffiliateProducts',compact('Products','id'));
    }
    public function affiliateStore(Request $request)
    {

      $aff =  AffiliateConnection::select('tier_price')->where('id',$request->id)->first();
      $unit_price=0;
       
        foreach ($request->product_arr as $key => $value) {
            
           
                $prd = Product::select($aff->tier_price. ' as tier_price','unit_price','description','sku_number','free_shipping','local_pickup','local_pickup_shipping_cost','flat_rate','flat_rate_shipping_cost','brand_name')->where('id',$value)->first();

                if (empty($prd->tier_price)) {
                    $unit_price = $prd->unit_price;
                }
                else {
                    $unit_price = $prd->tier_price;
                }
            

            
            AffiliateProduct::create(['affiliate_connection_id'=>$request->id,'product_id'=>$value,'unit_price'=>$unit_price,'description'=>$prd->description,'sku_number'=>$prd->sku_number,'free_shipping'=>$prd->free_shipping,'local_pickup'=>$prd->local_pickup,'local_pickup_shipping_cost'=>$prd->local_pickup_shipping_cost,'flat_rate'=>$prd->flat_rate,'flat_rate_shipping_cost'=>$prd->flat_rate_shipping_cost,'brand_name'=>$prd->brand_name]);
            
        }
        AffiliateConnection::where('id',$request->id)->update(['status'=>4]);

        $emails = User::select('users.email')->leftjoin('affiliate_connections as ac','ac.requester_id','users.id')->where('ac.id',$request->id)->first();
        $connectionEmail = ['message' => 'The receiver gave access to their products please check in your panel.',
        'email' => $emails->email];

        Email_sender::sendConnectionEstablishedMail($connectionEmail);
    }

    public function requesterDetails($id)
    {
        $user = User::where('id',$id)->first();
        
        $products = AffiliateProduct::select('p.product_name','b.name as brand_name','p.sku_number','p.unit','p.unit_price')->leftjoin('affiliate_connections as ac','ac.id','affiliate_products.affiliate_connection_id')->leftjoin('products as p','p.id','affiliate_products.product_id')->leftjoin('brands as b','b.id','p.brand_name')->where('ac.requester_id',$id)->where('ac.receiver_id',Auth::id())->get();
        
        return view('affiliateProducts.requester_details',compact('user','products'));
    }

   
}
