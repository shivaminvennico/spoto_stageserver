<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Specification;
use App\SubSpecification;
use App\VehicleSpecification;
use Illuminate\Support\Facades\Validator;


class SpecificationController extends Controller
{
    
    public function create()
    {
        $ParentSpecification = Specification::orderBy('name')->pluck('name', 'id');
        
        $ViewSpecification = Specification::all();
            
        return view('vehicles.addSpecs',compact('ParentSpecification', 'ViewSpecification'));
    }

    public function store(Request $request)
    {
    
        $request->validate([
            'name' => 'required|unique:specifications|max:80'
        ]);
        $specArr = [];
        $specArr['name'] = $request->name;
        $specArr['is_mandatory'] = isset($request->is_mandatory) ? 1 : 0;
        $Insert = Specification::create($specArr);
        return back()->withStatus(__('Parent specification added successfully.'));
    }

    public function storeSub(Request $request)
    {
        
        $ParentIds = implode(',',Specification::pluck('id')->toArray());
        $request->validate([
            'sub_name' => 'required|max:80',
            'specification_id' => 'required|in:'.$ParentIds
        ]);
        
        $checkDuplicate = SubSpecification::where('specification_id',$request->specification_id)
        ->where('sub_name',$request->sub_name)->count();    

            if($checkDuplicate > 0){
                return redirect()->back()->with('sub_name', 'A Sub Specification with same combination exists.');
            }else{
                $Insert = SubSpecification::create($request->only(['specification_id', 'sub_name']));
                return back()->withStatus(__('Sub Specification added successfully.'));
            }
        
    }

    public function getSub(Request $request)
    {
        $SubSpecifications = SubSpecification::select('sub_name','id')->where('specification_id',$request->parent)->get();
        
            return view('vehicles.loadSpecification',compact('SubSpecifications'));
        

    }

    //Load Specification inside wizard
    public function loadSpecification()
    {
        $Specification['main_specification'] = SubSpecification::select('specifications.id','specifications.name','specifications.is_mandatory')->leftjoin('specifications','sub_specifications.specification_id','specifications.id')->distinct('sub_specifications.id')->where('specifications.is_mandatory','1')->get();

        $Specification['basic_specification'] = SubSpecification::select('specifications.id','specifications.name','specifications.is_mandatory')->leftjoin('specifications','sub_specifications.specification_id','specifications.id')->distinct('sub_specifications.id')->where('specifications.is_mandatory','0')->get();

        $Specification['sub_specification'] = SubSpecification::select('sub_name','id','specification_id')->get();
        
     //    return view('vehicles.loadSpecificationWizard',compact('Specification'));
     return $Specification;
    }

    public function edit_specs_input(Request $request)
    {
        $request->validate([
            'name' => 'required|max:80'
        ]);
        $includeInVehicleSpecs = VehicleSpecification::leftjoin('sub_specifications as ss','ss.id','vehicle_specifications.sub_specification_id')
        ->leftjoin('specifications as s','s.id','ss.specification_id')
        ->where('s.id',$request->parent)->count();
        if($includeInVehicleSpecs>0)
        {
            return 'dublicate';
        }
        else
        {
            Specification::where('id',$request->parent)->update(['name'=>$request->name, 'is_mandatory'=>($request->mandatory == "true") ? 1 : 0]);
            return $request->all();
        }
    }

    public function edit_sub_specs_input(Request $request)
    {
        $request->validate([
            'name' => 'required|max:80'
        ]);
        $includeInVehicleSpecs = VehicleSpecification::where('sub_specification_id',$request->parent)->count();
        if($includeInVehicleSpecs>0)
        {
            return 'dublicate';
        }
        else
        {
            SubSpecification::where('id',$request->parent)->update(['sub_name'=>$request->name]);
            return $request->all();
        }
    }

}
