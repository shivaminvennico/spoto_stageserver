<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MakeModelYear;
use App\Vehicle;
use App\VehicleSpecification;
use DB;

class VehicleController extends Controller
{
        public function index()
        {
            $Vehicles = Vehicle::select('vehicles.id','variant_name','photo','make','model','year','to_year')
            ->join('make_model_years','make_model_years.id','=','vehicles.mmy_id')->paginate(10);
            return view('vehicles.add',compact('Vehicles'));
        }
        public function add()
        {
            $Make = MakeModelYear::groupBy('make')->pluck('make','make');
            $Make['1'] = "Add Another Make";
            return view('vehicles.loadSpecs',compact('Make'));
        }
        public function getMake(Request $request)
        {
            $Year = MakeModelYear::where('make',$request->make)->groupBy('year')->pluck('year','year');
            if ($request->edit ==0) {
                $Year['1'] = "Add Another Year";
            }
            
            return view('vehicles.loadYear',compact('Year'));
        }
        /**
         * For sending the list of 
         * model for a particular make and year
         * 
         * @return list of the models from request of make and year
         */
        public function getModel(Request $request)
        {
            $Model = MakeModelYear::where('year',$request->year)
            ->where('make',$request->make)->pluck('model','id');
            if ($request->edit == 0) {
                $Model['new'] = "Add Another Model";
            }
                   
            return view('vehicles.loadModel',compact('Model'));
        }
        /**
         * For sending the list of 
         * models for a particular make from vehicles added 
         * 
         * @return list of the year from request of make and year
         */
        public function getVehicleModel(Request $request)
        {
            $Model = MakeModelYear::
            join('vehicles','vehicles.mmy_id','=','make_model_years.id')
            ->where('make',$request->makeName)
            ->groupBy('make_model_years.model')
            ->pluck('model','model');
            
            return view('vehicles.loadVehicleModel',compact('Model'));
        }

        /**
         * For sending the list of 
         * years for a particular make from vehicles added 
         * 
         * @return list of the year from request of make and year
         */
        public function getVehicleYear(Request $request)
        {
            $Year = MakeModelYear::
            join('vehicles','vehicles.mmy_id','=','make_model_years.id')
            ->where('model',$request->ModelName)
            ->pluck('year','year');
            
            return view('vehicles.loadVehicleYear',compact('Year'));
        }

        /**
         * For sending the list of 
         * years for a particular make from vehicles added 
         * 
         * @return list of the year from request of make and year
         */
        public function getVehicleVariant(Request $request)
        {
            $Variant = MakeModelYear::
            join('vehicles','vehicles.mmy_id','=','make_model_years.id')
            ->where('year',$request->VehicleYear)
            ->select('variant_name','vehicles.id')
            ->pluck('variant_name','vehicles.id');
            
            return view('vehicles.loadVehicleVariant',compact('Variant'));
        }

        /**
         * For storing the variant name 
         */

        //store new make model year
        public function storeMmy(Request $request)
        {
            // echo "<pre>";print_r($request->all());die;
            $year = explode(',',$request->year);
            $duplicate = -1;
            $k=0;
            $error = -1;
            foreach ($year as $key => $value) {
                if (strlen($value)==4) {
                    $mmy_id = MakeModelYear::where(
                        ['make' => $request->make,'year'=> $value , 'model' => $request->model]                        
                    )->count();
                        $k=$key;
                    if ($mmy_id > 0) {
                        $duplicate+=1;
                    }
                    else {
                        MakeModelYear::create(
                            ['make' => $request->make,'year'=> $value , 'model' => $request->model]                        
                        );
                    }
                }
                else {
                    $error+=1;
                }
            }

            if ($duplicate == $k) {
                echo "duplicate";
            }
            if ($error == $k) {
                echo "error";
            }
            
        }
        public function store(Request $request)
        {
            
            $toYear;
            if(isset($request->toyear))
            {
                $toYear = $request->toyear;
            }
            else
            {
                $toYear = date("Y");
            }
            
            $request->validate([
                'mmy_id' => 'required',
                'variant_name' => 'required'
            ]);
            
            $checkDuplicate = Vehicle::where('mmy_id',$request->mmy_id)
            ->where('variant_name',$request->variant_name)->count();    
    
            if($checkDuplicate > 0){
                return redirect()->route('vehicle.index')->with('duplicate_error', 'A Combination is already there.');
            }else{
                $vehicleArr = [];
                $vehicleArr['mmy_id'] = $request->mmy_id;
                $vehicleArr['variant_name'] = $request->variant_name;
                $vehicleArr['from_year'] = $request->year;
                $vehicleArr['to_year'] = $toYear;
                $Insert = Vehicle::create($vehicleArr);
               
                $vehicleSpecArr = [];
                $vehicleSpecArr['vehicle_id'] = $Insert->id;
                
                foreach ($request->sub_specification as $key => $value) {
                    $vehicleSpecArr['sub_specification_id'] = $value;
                    VehicleSpecification::create($vehicleSpecArr);
                }
                
                return redirect()->route('vehicle.index')->with('status','Vehicles added Successfully');
            }
        }

        public function get_vehicle_details($id)
        {
            $vehicleDetails = VehicleSpecification::select('v.variant_name','s.name','s.id as specs_id','make','model','year')
            ->leftjoin('vehicles as v','v.id','vehicle_specifications.vehicle_id')
            ->leftjoin('sub_specifications as ss','ss.id','vehicle_specifications.sub_specification_id')
            ->leftjoin('specifications as s','s.id','ss.specification_id')
            ->join('make_model_years','make_model_years.id','=','v.mmy_id')
            ->where('v.id',$id)
            ->distinct()
            ->get();

            $subSpecs = VehicleSpecification::select('ss.sub_name','ss.specification_id')
            ->leftjoin('sub_specifications as ss','ss.id','vehicle_specifications.sub_specification_id')
            ->where('vehicle_id',$id)
            ->distinct()
            ->get();

             return view('vehicles.vehicle_details',compact('vehicleDetails','subSpecs'));
        }

        public function edit($id)
        {
            $Make = MakeModelYear::groupBy('make')->pluck('make','make');
            $vehicle = Vehicle::select('make','mmy_id','year','variant_name','vehicles.id')
                        ->leftjoin('make_model_years as mmy','mmy.id','vehicles.mmy_id')
                        ->where('vehicles.id',$id)
                        ->first();
            $Year = MakeModelYear::where('make',$vehicle->make)->groupBy('year')->pluck('year','year');
            $Model = MakeModelYear::where('year',$vehicle->year)
            ->where('make',$vehicle->make)->pluck('model','id');
            $subSpecs = VehicleSpecification::select('sub_specification_id')->where('vehicle_id',$id)->pluck('sub_specification_id');
            
            return view('vehicles.edit',compact('Make','vehicle','Year','Model','subSpecs'));
        }

        public function update(Request $request,$id)
        {
            $request->validate([
                'mmy_id' => 'required',
                'variant_name' => 'required'
            ]);
            // echo '<pre>';print_r($request->all());
            // print_r($request->sub_specification);die;
            Vehicle::where('id',$id)->update(['mmy_id'=>$request->mmy_id,'variant_name'=>$request->variant_name]);
            VehicleSpecification::where('vehicle_id',$id)->delete();
            $subSpecs = array();
            
            foreach ($request->sub_specification as $key => $value) {
                VehicleSpecification::create([
                   'vehicle_id'=>$id,
                   'sub_specification_id'=>$value
               ]);
            }
            return redirect()->route('vehicle.index')->with('status','Vehicles updated Successfully');
        }
}
