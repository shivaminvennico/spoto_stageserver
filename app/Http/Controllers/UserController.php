<?php

namespace App\Http\Controllers;
use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    
	public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('permission:user-list');
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }
    
    public function index(User $model)
    {
        return view('users.index', ['users' => $model->where('id','!=',auth()->id())
        ->where('user_type','!=',1)
        ->paginate(10)]);
    }

    public function create()
    {
    	return view('users.create');
    }

    public function store(UserRequest $request, User $model)
    {
        $data = $request->all();   
        $user  = $data['user_type'];
        $role_type = '';
        $user_type = 0;
        $BackEndUser = 0;

        if($user == 1){
            $role_type = 'Manufacturer';
            $user_type = 2;
            
        }elseif($user == 2){
            $role_type = 'Importer';
            $user_type = 3;

        }elseif($user == 3) {
            $role_type = 'Distributor';
            $user_type = 4;

        }elseif($user == 4) {
            $role_type = 'EndUser';
            $user_type = 5;
        }

        $model = User::select('id','first_name')->where('email',$data['email'])->where('user_type',5)->first();
        if(isset($model->id)){
            $model->user_type = $user_type;
            $model->assignRole($role_type);
            $BackEndUser = $model->save();
            
        }else{
            $BackEndUser = User::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'user_type'=>$user_type,
            ]);
            $BackEndUser->assignRole($role_type);
            
        }
        return redirect()->route('user.index')->withStatus(__('User successfully created.'));
    }


    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    public function update(UserRequest $request, User  $user)
    {
        $hasPassword = $request->get('password');
        $user->update(
            $request->merge(['password' => Hash::make($request->get('password'))])
                ->except([$hasPassword ? '' : 'password']
        ));

        return redirect()->route('user.index')->withStatus(__('User successfully updated.'));
    }

    public function destroy(User  $user)
    {
        $user->delete();

        return redirect()->route('user.index')->withStatus(__('User successfully deleted.'));
    }

}
