<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Vehicle;
use App\TierLabel;
use App\Warehouse;
use App\Brand;
use App\Product;
use App\ProductVehicle;
use Auth;


class BundleProductController extends Controller
{
    public function add()
    {
        $ParentCategories = Category::orderBy('name')->pluck('name', 'id');
        $Make = Vehicle::select('make_model_years.make','vehicles.mmy_id')
        ->leftjoin('make_model_years','make_model_years.id','vehicles.mmy_id')
        ->pluck('make','make');

        $tierNames = TierLabel::firstOrCreate(['user_id'=>Auth::id()]); 
        if(!(isset($tierNames->tier_1)))
        {
            $tierNames = TierLabel::where('user_id',Auth::id())->first();
        }
        
        $Warehouse = Warehouse::where('user_id',Auth::id())->pluck('name','id');
        $Brands = Brand::where('user_id',Auth::id())->pluck('name','id');
        return view('bundleProducts.add',compact('ParentCategories','Make','tierNames','Warehouse','Brands'));
    }

    public function loadProducts(Request $request)
    {
        $Category = Category::where('name',$request->parent)->first();
        $Products = Product::select('products.id','product_name','b.name as brand_name','unit','unit_price','sku_number')->leftjoin('brands as b','b.id','products.brand_name')->where('created_by',Auth::id())->where('bundled_product',0)->where('products.product_name','like','%' . $request->parent . '%')
        ->orWhere('products.sku_number','like','%' . $request->parent . '%')->get();
        return view('bundleProducts.loadBundleProducts',compact('Products'));
    }
    public function store(Request $request)
    {
     
    }
    public function getMappedVehicles(Request $request)
    {   $mappedVehicles = [];
        $vehicles = [];
        foreach ($request->parent as $key => $value) {
            
            $mappedVehicles[$key] = ProductVehicle::select('mmy.make','mmy.model','v.variant_name','v.id')->leftjoin('vehicles as v','v.id','product_vehicles.vehicle_id')->leftjoin('make_model_years as mmy','mmy.id','v.mmy_id')->where('product_id',$value)->distinct()->get();
            if ($mappedVehicles[$key]  != '[]') {
                foreach ($mappedVehicles[$key] as $id => $value) {
                    
                        if (!in_array($value,$vehicles)) {
                            array_push($vehicles,$value);
                        }
                        
                }
            }
            
            
        }
        

        return $vehicles;
    }
}
