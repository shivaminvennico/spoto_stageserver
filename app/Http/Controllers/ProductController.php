<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use App\Vehicle;
use App\ProductVehicle;
use App\TierLabel;
use App\AffiliateProduct;
use Illuminate\Support\Facades\Storage;
use Auth;
use DB;
use App\Warehouse;
use App\ProductWarehouse;
use App\Brand;
use App\BundleProduct;
use App\User;
use App\Helpers\Email_sender;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('permission:product-list');
        $this->middleware('permission:product-create', ['only' => ['add','store','image','imageRemove','searchBar']]);
        $this->middleware('permission:product-edit', ['only' => ['edit','update','image','imageRemove','searchBar']]);
        $this->middleware('permission:product-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        
    }

    public function loadProducts()
    {
        $Product = Product::select('products.id','product_name','b.name as brand_name','unit','unit_price','sku_number','bundled_product')->leftjoin('brands as b','b.id','products.brand_name');

            $AffiliateProduct = AffiliateProduct::select('p.id','p.product_name','b.name as brand_name','p.unit','affiliate_products.unit_price','affiliate_products.sku_number','r.name as user_type',DB::raw("CONCAT(u.first_name,' ',u.last_name) as rec_name"),DB::raw("CONCAT(req.first_name,' ',req.last_name) as req_name"))->leftjoin('affiliate_connections as ac','ac.id','affiliate_products.affiliate_connection_id')->leftjoin('products as p','p.id','affiliate_products.product_id')->leftjoin('brands as b','b.id','affiliate_products.brand_name')->leftjoin('users as u','u.id','p.created_by')->leftjoin('users as req','req.id','ac.requester_id')->leftjoin('roles as r','r.id','u.user_type');

        if(Auth::id()!=1)
        {
            $Product->where('created_by',Auth::id());
            $AffiliateProduct->where('ac.requester_id',Auth::id());

        }
            $Products = $Product->paginate(10);
            $AffiliateProducts = $AffiliateProduct->paginate(10);

        return view('product.index',compact('Products','AffiliateProducts'));
    }

    public function loadProductDesc()
    {
        // $VehicleMakes = Vehicle::select('make_model_years.make','vehicles.mmy_id')
        // ->join('make_model_years','make_model_years.id','=','vehicles.mmy_id')
        // ->pluck('make','make');
        return view('product.product_desc');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $Warehouse = Warehouse::where('user_id',Auth::id())->pluck('name','id');
        $Brands = Brand::where('user_id',Auth::id())->pluck('name','id');
        if (count($Warehouse) == 0|| count($Brands)==0) 
        {
            return redirect()->route('settings')->with('error','Before adding products, you have to add warehouse and brand');
        }
        
    
        
        $ParentCategories = Category::orderBy('name')->pluck('name', 'id');
        $Make = Vehicle::select('make_model_years.make','vehicles.mmy_id')
        ->leftjoin('make_model_years','make_model_years.id','vehicles.mmy_id')
        ->pluck('make','make');

        $tierNames = TierLabel::firstOrCreate(['user_id'=>Auth::id()]); 
        if(!(isset($tierNames->tier_1)))
        {
            $tierNames = TierLabel::where('user_id',Auth::id())->first();
        }
        
       
        return view('product.add',compact('ParentCategories','Make','tierNames','Warehouse','Brands'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function image(Request $request)
    {
        
        if (isset($request->file))
        {
            $path = storage_path('tmp/uploads');
        
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
    
            $file = $request->file('file');
    
            $name = uniqid() . '_' . trim($file->getClientOriginalName());
    
            $file->move($path, $name);
            
            return response()->json([
                'name'          => $name,
                'original_name' => $file->getClientOriginalName(),
            ]);
        }
        if($request->req == 2)
        {
            $file_list = array();
            $files = array();
            $files [] = explode(',',$request->photos);
            
            foreach ($files[0] as $key => $value) {
                $exists = Storage::disk('s3')->exists('Products/'.$value);
                if ($exists) {
                    $stringFile = Storage::disk('s3')->url('Products/'.$value);
               
                    //    $fileContent = file_get_contents($stringFile);
                    //    $size = filesize($stringFile);
                       $file_list[] = array('name'=>$value,'path'=>$stringFile);
                       
                }
                
            }
            echo json_encode($file_list);
            exit;
           
        }
    }
    //Remove Image from dropzone
    public function imageRemove(Request $request)
    {
        if (isset($request->req)) 
        {
            Storage::disk('s3')->delete('Products/'.$request->fileName);
            
            return response()->json([
                'name'          => $request->fileName
                
            ]);
        }
        else
        {
            $path = storage_path('tmp/uploads/'.$request->fileName);
        
            if (file_exists($path)) {
                unlink($path);
            }
    
            
    
            return response()->json([
                'name'          => $request->fileName
                
            ]);
        }
        
    }
    public function store(Request $request)
    {
       
        // echo '<pre>'; print_r($request->mappedProducts);die;
        // print_r($request->file());die;
        // echo $request->tier_ten_price;die;
        $fileNames = array();
        $input_files = '';
        $request->validate([
            'category_id' => 'required|numeric',
            'sub_category_id' => 'numeric',
            'sub_sub_category_id' => 'numeric',
            'product_name' => 'required|max:100',
            'brand_name' => 'required',
            'sku_number' => 'required|unique:products,sku_number',
            'product_unit_price'=>'required|numeric',
            'product_purchase_price'=>'required|numeric',
            'product_tax' => 'required',
            'product_discount' => 'required',
            'local_pickup_shipping_cost' => 'required',
            'flat_rate_shipping_cost' => 'required',
            'qty' => 'required|array|min:1',
            'qty.*' => 'required',
            'warehouse' => 'required|array|min:1',
            'warehouse.*' => 'required',
            'qty_InHand' => 'required|array|min:1',
            'qty_InHand.*' => 'required',
        ]);

        $files = $request->input('document', []);
        if(isset($request->document))
        {
            foreach ($files as $file) {
                $name = $file;
                $filePath = 'Products/' . $name;
                Storage::disk('s3')->put($filePath, file_get_contents(storage_path('tmp/uploads/' . $file)));
                unlink(storage_path('tmp/uploads/'.$file));
                $fileNames[] = $name;
            }
            $input_files = implode(',',$fileNames);
        }
        
        $mmy_id = $vehicle_id = $vehicle_name = '';
        
        $data = $request->all();
        
        
        
        $createProduct = Product::create([
            'category_id' =>$data['category_id'],
            'sub_category_id' =>isset($data['sub_category_id']) ? $data['sub_category_id'] : 0 ,
            'sub_sub_category_id' => isset($data['sub_sub_category_id']) ? $data['sub_sub_category_id'] : 0 ,
            'product_name' =>$data['product_name'],
            'brand_name' =>$data['brand_name'],
            'unit' => '1',
            'sku_number'=>$data['sku_number'],
            'unit_price'=>$data['product_unit_price'],
            'purchase_price'=>$data['product_purchase_price'],
            'description'=>$data['product_description'],
            'tax'=>$data['product_tax'],
            'discount'=>$data['product_discount'],
            'free_shipping' =>isset($data['free_shipping']) ? 1 : 0 ,
            'local_pickup' => isset($data['local_pickup']) ? 1 : 0 ,
            'local_pickup_shipping_cost'=>$data['local_pickup_shipping_cost'],
            'flat_rate' => isset($data['flat_rate']) ? 1 : 0 ,
            'flat_rate_shipping_cost'=>$data['flat_rate_shipping_cost'],
            'photos'=>$input_files,
            'tier_1'=>$data['tier_one_price'],
            'tier_2'=>$data['tier_two_price'],
            'tier_3'=>$data['tier_three_price'],
            'tier_4'=>$data['tier_four_price'],
            'tier_5'=>$data['tier_five_price'],
            'tier_6'=>$data['tier_six_price'],
            'tier_7'=>$data['tier_seven_price'],
            'tier_8'=>$data['tier_eight_price'],
            'tier_9'=>$data['tier_nine_price'],
            'tier_10'=>$data['tier_ten_price'],
            'created_by'=>Auth::id()

        ]);
        
        if(isset($createProduct->id)){
            $proVehicleArr =[];
            $proVehicleArr['product_id'] = $createProduct->id;
            $vehicle_ids = isset($data['mapVehicleIds']) ? $data['mapVehicleIds'] : $data['vehicle_id'];
            foreach ($vehicle_ids as $key => $value) {
                
                $proVehicleArr['vehicle_id'] = $value;
                ProductVehicle::create($proVehicleArr);
            }
            $totalQty = 0;
            $proWarehoustArr = [];
            $proWarehoustArr['product_id'] = $createProduct->id;
            for ($i=0; $i < count($request->qty); $i++) { 
                $totalQty += $request->qty[$i];
                $proWarehoustArr['qty'] = $request->qty[$i];
                $proWarehoustArr['warehouse'] = $request->warehouse[$i];
                $proWarehoustArr['qty_in_hand'] = $request->qty_InHand[$i];

                ProductWarehouse::create($proWarehoustArr);
            }

            if (isset($data['mappedProducts'])) {
                $bundledProductsArr = explode(',',$data['mappedProducts']);
                foreach ($bundledProductsArr as $key => $value) {
                    BundleProduct::create(['product_id'=>$createProduct->id,'bundled_product'=>$value]);

                }
                Product::where('id',$createProduct->id)->update(['bundled_product'=>1]);
            }
            Product::where('id',$createProduct->id)->update(['unit'=>$totalQty]);
            return redirect()->route('product')->withStatus(__('Product successfully added.'));
        }else{
            return redirect()->route('product')->withStatus(__('There were some issues.'));
        }
    }

    public function get_product_details($id)
    {
       $AffiliateProducts = AffiliateProduct::select('sku_number')->where('product_id',$id)->first();
        if (empty($AffiliateProducts->sku_number)) {
            $product = ProductVehicle::select('p.product_name','b.name as brand_name','p.sku_number','p.unit_price','p.purchase_price','p.description','p.tax','p.discount','p.free_shipping','p.local_pickup','p.local_pickup_shipping_cost','p.flat_rate','p.flat_rate_shipping_cost','v.variant_name','p.tier_1','p.tier_2','p.tier_3','p.tier_4','p.tier_5','p.tier_6','p.tier_7','p.tier_8','p.tier_9','p.tier_10','p.photos','mmy.make','mmy.model','c.name','sc.sub_name','ssc.sub_sub_name','p.created_by')
            ->leftjoin('products as p','p.id','product_vehicles.product_id')
            ->leftjoin('vehicles as v','v.id','product_vehicles.vehicle_id')
            ->leftjoin('make_model_years as mmy','mmy.id','v.mmy_id')
            ->leftjoin('categories as c','c.id','p.category_id')
            ->leftjoin('sub_categories as sc','sc.id','p.sub_category_id')
            ->leftjoin('sub_sub_categories as ssc','ssc.id','p.sub_sub_category_id')
            ->leftjoin('brands as b','b.id','p.brand_name')
            ->where('product_vehicles.product_id',$id)
            ->distinct();
        }
        else{
            $product = ProductVehicle::select('p.product_name','b.name as brand_name','ap.sku_number','ap.unit_price','p.purchase_price','ap.description','p.tax','p.discount','ap.free_shipping','ap.local_pickup','ap.local_pickup_shipping_cost','ap.flat_rate','ap.flat_rate_shipping_cost','v.variant_name','p.tier_1','p.tier_2','p.tier_3','p.tier_4','p.tier_5','p.tier_6','p.tier_7','p.tier_8','p.tier_9','p.tier_10','p.photos','mmy.make','mmy.model','c.name','sc.sub_name','ssc.sub_sub_name','p.created_by')
            ->leftjoin('products as p','p.id','product_vehicles.product_id')
            ->leftjoin('affiliate_products as ap','ap.product_id','p.id')
            ->leftjoin('vehicles as v','v.id','product_vehicles.vehicle_id')
            ->leftjoin('make_model_years as mmy','mmy.id','v.mmy_id')
            ->leftjoin('categories as c','c.id','p.category_id')
            ->leftjoin('sub_categories as sc','sc.id','p.sub_category_id')
            ->leftjoin('sub_sub_categories as ssc','ssc.id','p.sub_sub_category_id')
            ->leftjoin('brands as b','b.id','ap.brand_name')
            ->where('product_vehicles.product_id',$id)
            ->distinct();
        }
        
        $products = $product->get();
        $inventory = ProductWarehouse::select('qty','w.name','qty_in_hand')->leftjoin('warehouses as w','w.id','product_warehouses.warehouse')->where('product_id',$id)->get();
        if (empty($products[0])) {
            $products = Product::select('products.product_name','b.name as brand_name','products.sku_number','products.unit_price','products.purchase_price','products.description','products.tax','products.discount','products.free_shipping','products.local_pickup','products.local_pickup_shipping_cost','products.flat_rate','products.flat_rate_shipping_cost','products.tier_1','products.tier_2','products.tier_3','products.tier_4','products.tier_5','products.tier_6','products.tier_7','products.tier_8','products.tier_9','products.tier_10','products.photos','c.name','sc.sub_name','ssc.sub_sub_name')
            ->leftjoin('categories as c','c.id','products.category_id')
            ->leftjoin('sub_categories as sc','sc.id','products.sub_category_id')
            ->leftjoin('sub_sub_categories as ssc','ssc.id','products.sub_sub_category_id')
            ->leftjoin('brands as b','b.id','products.brand_name')
            ->where('products.id',$id)
            ->get();
        }
        
        
        $tier_labels = TierLabel::leftjoin('users as u','u.id','tier_labels.user_id')->where('u.id',Auth::id())->first();
        $bundle = BundleProduct::select('p.product_name','b.name as brand_name','p.unit','p.sku_number','p.unit_price')->leftjoin('products as p','p.id','bundle_products.bundled_product')->leftjoin('brands as b','b.id','p.brand_name')->where('product_id',$id)->get();
        
        return view('product.product_details',compact('products','tier_labels','inventory','bundle'));
    }

    public function searchBar(Request $request)
    {
        $val = $request->searchVal;
        
        $products = ProductVehicle::select('mmy.make','mmy.model','v.variant_name','p.id','product_vehicles.vehicle_id')
                ->leftjoin('products as p','p.id','product_vehicles.product_id')
                ->leftjoin('vehicles as v','v.id','product_vehicles.vehicle_id')
                ->leftjoin('make_model_years as mmy','mmy.id','v.mmy_id')
                ->where('p.product_name','like','%' . $val . '%')
                ->orWhere('p.sku_number','like','%' . $val . '%')
                ->get();
                
        return empty($val) ? $val : $products;
    }

    public function fetchVehicle(Request $request)
    {
        $vehicles = Vehicle::select('make','model','year','vehicles.id')
                ->leftjoin('make_model_years as mmy','mmy.id','vehicles.mmy_id')
                ->leftjoin('product_vehicles as pv','pv.vehicle_id','vehicles.id')
                ->where('pv.product_id',$request->id)
                ->get();
        return $vehicles;
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::where('id',$id)->first();
        $ParentCategories = Category::orderBy('name')->pluck('name', 'id');
        $SubCategories = SubCategory::orderBy('sub_name')->where('category_id',$products->category_id)->pluck('sub_name','id');
        $SubSubCategories = SubSubCategory::orderBy('sub_sub_name')->where('sub_category_id',$products->sub_category_id)->pluck('sub_sub_name','id');
        $Make = Vehicle::select('make_model_years.make','vehicles.mmy_id')
        ->leftjoin('make_model_years','make_model_years.id','vehicles.mmy_id')
        ->pluck('make','make');

        $mappedVehicles = ProductVehicle::select('mmy.make','mmy.model','v.variant_name','v.id')->leftjoin('vehicles as v','v.id','product_vehicles.vehicle_id')->leftjoin('make_model_years as mmy','mmy.id','v.mmy_id')->where('product_id',$products->id)->get();
        
        $tierNames = TierLabel::firstOrCreate(['user_id'=>Auth::id()]); 
        if(!(isset($tierNames->tier_1)))
        {
            $tierNames = TierLabel::where('user_id',Auth::id())->first();
        }
        $Warehouse = Warehouse::where('user_id',Auth::id())->pluck('name','id');
        $Brands = Brand::where('user_id',Auth::id())->pluck('name','id');
        $mappedWarehouse = ProductWarehouse::select('qty','w.name','warehouse','qty_in_hand')->leftjoin('warehouses as w','w.id','product_warehouses.warehouse')->where('product_id',$id)->get();

        $bundle = BundleProduct::select('p.id','p.product_name','b.name as brand_name','p.unit','p.sku_number','p.unit_price')->leftjoin('products as p','p.id','bundle_products.bundled_product')->leftjoin('brands as b','b.id','p.brand_name')->where('product_id',$id)->get();
        
        return view('product.edit',compact('ParentCategories','SubCategories','SubSubCategories','Make','tierNames','products','mappedVehicles','Warehouse','mappedWarehouse','Brands','bundle'));

       
    }

    public function affiliateEdit($id)
    {        
        $products = Product::select('products.id','category_id','sub_category_id','sub_sub_category_id','products.product_name','products.brand_name','ap.sku_number','ap.unit_price','products.purchase_price','ap.description','products.tax','products.discount','ap.free_shipping','ap.local_pickup','ap.local_pickup_shipping_cost','ap.flat_rate','ap.flat_rate_shipping_cost','products.tier_1','products.tier_2','products.tier_3','products.tier_4','products.tier_5','products.tier_6','products.tier_7','products.tier_8','products.tier_9','products.tier_10','products.photos','created_by')->leftjoin('affiliate_products as ap','ap.product_id','products.id')->where('products.id',$id)->first();
        $ParentCategories = Category::orderBy('name')->pluck('name', 'id');
        $SubCategories = SubCategory::orderBy('sub_name')->where('category_id',$products->category_id)->pluck('sub_name','id');
        $SubSubCategories = SubSubCategory::orderBy('sub_sub_name')->where('sub_category_id',$products->sub_category_id)->pluck('sub_sub_name','id');
        $Make = Vehicle::select('make_model_years.make','vehicles.mmy_id')
        ->leftjoin('make_model_years','make_model_years.id','vehicles.mmy_id')
        ->pluck('make','make');

        $mappedVehicles = ProductVehicle::select('mmy.make','mmy.model','v.variant_name','v.id')->leftjoin('vehicles as v','v.id','product_vehicles.vehicle_id')->leftjoin('make_model_years as mmy','mmy.id','v.mmy_id')->where('product_id',$products->id)->get();
        
        $tierNames = TierLabel::firstOrCreate(['user_id'=>Auth::id()]); 
        if(!(isset($tierNames->tier_1)))
        {
            $tierNames = TierLabel::where('user_id',Auth::id())->first();
        }
        $Warehouse = Warehouse::where('user_id',$products->created_by)->pluck('name','id');
        $Brands = Brand::where('user_id',$products->created_by)->pluck('name','id');
        $mappedWarehouse = ProductWarehouse::select('qty','w.name','warehouse','qty_in_hand')->leftjoin('warehouses as w','w.id','product_warehouses.warehouse')->where('product_id',$id)->get();

        $bundle = BundleProduct::select('p.id','p.product_name','b.name as brand_name','p.unit','p.sku_number','p.unit_price')->leftjoin('products as p','p.id','bundle_products.bundled_product')->leftjoin('brands as b','b.id','p.brand_name')->where('product_id',$id)->get();
        
        return view('affiliateProducts.edit',compact('ParentCategories','SubCategories','SubSubCategories','Make','tierNames','products','mappedVehicles','Warehouse','mappedWarehouse','Brands','bundle'));

       
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo '<pre>';print_r($request->all());die;

        $request->validate([
            'category_id' => 'required|numeric',
            'sub_category_id' => 'numeric',
            'sub_sub_category_id' => 'numeric',
            'product_name' => 'required|max:100',
            'brand_name' => 'required',
            'sku_number' => 'required',
            'product_unit_price'=>'required|numeric',
            'product_purchase_price'=>'required|numeric',
            'product_tax' => 'required',
            'product_discount' => 'required',
            'local_pickup_shipping_cost' => 'required',
            'flat_rate_shipping_cost' => 'required',
            'qty' => 'required|array|min:1',
            'qty.*' => 'required',
            'warehouse' => 'required|array|min:1',
            'warehouse.*' => 'required',
            'qty_InHand' => 'required|array|min:1',
            'qty_InHand.*' => 'required',
        ]);
        
        $fileNames = array();
        $input_files = '';

        $files = $request->input('document', []);
        if(isset($request->document))
        {
            foreach ($files as $file) {
                $name = $file;
                $filePath = 'Products/' . $name;
                if (file_exists(storage_path('tmp/uploads/' . $file))) {
                    Storage::disk('s3')->put($filePath, file_get_contents(storage_path('tmp/uploads/' . $file)));
                    unlink(storage_path('tmp/uploads/'.$file));
                }
                $fileNames[] = $name;
            }
            $input_files = implode(',',$fileNames);
        }
        
        
        
        $data = $request->all();
        
        
        
        $update = Product::where('id',$id)->update([
            'category_id' =>$data['category_id'],
            'sub_category_id' =>isset($data['sub_category_id']) ? $data['sub_category_id'] : 0 ,
            'sub_sub_category_id' => isset($data['sub_sub_category_id']) ? $data['sub_sub_category_id'] : 0 ,
            'product_name' =>$data['product_name'],
            'brand_name' =>$data['brand_name'],
            'unit' =>'1',
            'sku_number'=>$data['sku_number'],
            'unit_price'=>$data['product_unit_price'],
            'purchase_price'=>$data['product_purchase_price'],
            'description'=>$data['product_description'],
            'tax'=>$data['product_tax'],
            'discount'=>$data['product_discount'],
            'free_shipping' =>isset($data['free_shipping']) ? 1 : 0 ,
            'local_pickup' => isset($data['local_pickup']) ? 1 : 0 ,
            'local_pickup_shipping_cost'=>$data['local_pickup_shipping_cost'],
            'flat_rate' => isset($data['flat_rate']) ? 1 : 0 ,
            'flat_rate_shipping_cost'=>$data['flat_rate_shipping_cost'],
            'photos'=>$input_files,
            'tier_1'=>$data['tier_one_price'],
            'tier_2'=>$data['tier_two_price'],
            'tier_3'=>$data['tier_three_price'],
            'tier_4'=>$data['tier_four_price'],
            'tier_5'=>$data['tier_five_price'],
            'tier_6'=>$data['tier_six_price'],
            'tier_7'=>$data['tier_seven_price'],
            'tier_8'=>$data['tier_eight_price'],
            'tier_9'=>$data['tier_nine_price'],
            'tier_10'=>$data['tier_ten_price'],
            

        ]);
        
            
            if (isset($data['vehicle_id']) || isset($data['editmapVehicleIds'])) {
                $proVehicleArr =[];
                $proVehicleArr['product_id'] = $id;
                
                $vehicle_ids = isset($data['editmapVehicleIds']) ? $data['editmapVehicleIds'] : $data['vehicle_id'];
                
                
                foreach ($vehicle_ids as $key => $value) {
                    
                    $proVehicleArr['vehicle_id'] = $value;
                    ProductVehicle::updateOrCreate($proVehicleArr);
                }
            }
            ProductWarehouse::where('product_id',$id)->delete();
            $totalQty = 0;
            $proWarehoustArr = [];
            $proWarehoustArr['product_id'] =  $id;
            foreach ($request->qty as $key => $value) {
                $totalQty += $value;
                $proWarehoustArr['qty'] = $value;
                $proWarehoustArr['warehouse'] = $request->warehouse[$key];
                $proWarehoustArr['qty_in_hand'] = $request->qty_InHand[$key];
                ProductWarehouse::create($proWarehoustArr);
            }
            
            BundleProduct::where('product_id',$id)->delete();
           
            if (isset($data['mappedProducts'])) {
                $bundledProductsArr = explode(',',$data['mappedProducts']);
                foreach ($bundledProductsArr as $key => $value) {
                    BundleProduct::create(['product_id'=>$id,'bundled_product'=>$value]);

                }
            }
            Product::where('id',$id)->update(['unit'=>$totalQty]);
            return redirect()->route('product')->withStatus(__('Product successfully updated.'));
        
        
    }

    public function affiliateUpdate(Request $request, $id)
    {
        // echo '<pre>';print_r($request->all());die;

        $request->validate([
            'brand_name' => 'required',
            'sku_number' => 'required',
            'product_unit_price'=>'required|numeric',
            'local_pickup_shipping_cost' => 'required',
            'flat_rate_shipping_cost' => 'required',
        ]);
        
        
        
        
        $data = $request->all();
        
        $checkUpdate = AffiliateProduct::where([
        'brand_name' =>$data['brand_name'],
        'sku_number'=>$data['sku_number'],
        'unit_price'=>$data['product_unit_price'],
        'description'=>$data['product_description'],
        'free_shipping' =>isset($data['free_shipping']) ? 1 : 0 ,
        'local_pickup' => isset($data['local_pickup']) ? 1 : 0 ,
        'local_pickup_shipping_cost'=>$data['local_pickup_shipping_cost'],
        'flat_rate' => isset($data['flat_rate']) ? 1 : 0 ,
        'flat_rate_shipping_cost'=>$data['flat_rate_shipping_cost'],
        ])->count();
        if ($checkUpdate == 0) {
            $update = AffiliateProduct::where('product_id',$id)->update([
                'brand_name' =>$data['brand_name'],
                'sku_number'=>$data['sku_number'],
                'unit_price'=>$data['product_unit_price'],
                'description'=>$data['product_description'],
                'free_shipping' =>isset($data['free_shipping']) ? 1 : 0 ,
                'local_pickup' => isset($data['local_pickup']) ? 1 : 0 ,
                'local_pickup_shipping_cost'=>$data['local_pickup_shipping_cost'],
                'flat_rate' => isset($data['flat_rate']) ? 1 : 0 ,
                'flat_rate_shipping_cost'=>$data['flat_rate_shipping_cost'],
            ]);

            $emails = User::select('users.email')->leftjoin('products as p','p.created_by','users.id')->where('p.id',$id)->first();
            $connectionEmail = ['message' => 'Requester changes some values in product you gave access.',
            'email' => $emails->email];

            Email_sender::sendConnectionEstablishedMail($connectionEmail);
                
            return redirect()->route('product')->withStatus(__('Affiliate product successfully updated.'));
        }
        
        return redirect()->route('product')->withStatus(__('Nothing to update'));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
