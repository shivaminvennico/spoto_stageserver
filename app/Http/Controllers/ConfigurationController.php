<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TierLabel;
use App\Warehouse;
use App\Brand;
use Auth;

class ConfigurationController extends Controller
{
    public function settings()
    {
        $labels = TierLabel::firstOrCreate(['user_id'=>Auth::id()]); 
        if(!(isset($labels->tier_1)))
        {
            $labels = TierLabel::where('user_id',Auth::id())->first();
        }
        $warehouses = Warehouse::where('user_id',Auth::id())->get();
        $brands = Brand::where('user_id',Auth::id())->get();
        return view('configs.settings',compact('labels','warehouses','brands'));
    }

    public function settingsUpdate(Request $request)
    {
        
       $data =  $request->all();
       
       TierLabel::where('user_id', Auth::id())
       ->update([
            'tier_1' => $data['tier_one_name'],
            'tier_2' => $data['tier_two_name'],
            'tier_3' => $data['tier_three_name'],
            'tier_4' => $data['tier_four_name'],
            'tier_5' => $data['tier_five_name'],
            'tier_6' => $data['tier_six_name'],
            'tier_7' => $data['tier_seven_name'],
            'tier_8' => $data['tier_eight_name'],
            'tier_9' => $data['tier_nine_name'],
            'tier_10' => $data['tier_ten_name']
        ]);

       return redirect()->route('settings')->withStatus(__('Label(s) successfully updated.'));
    }

    public function warehouseStore(Request $request)
    {
        Warehouse::create([
            'name' => $request->warehouse_name,
            'location' => $request->warehouse_location,
            'city' => $request->warehouse_city,
            'contact_no' => $request->warehouse_contact_no,
            'contact_person' => $request->warehouse_contact_person,
            'open_time' => $request->warehouse_operating_hrs_start,
            'close_time' => $request->warehouse_operating_hrs_end,
            'user_id' => Auth::id()
        ]);

        return redirect()->route('settings')->withStatus(__('Warehouse successfully added.'));
    }

    public function warehouseUpdate(Request $request)
    {
        Warehouse::where('id',$request->id)->update([
            'name' => $request->edit_warehouse_name,
            'location' => $request->edit_warehouse_location,
            'city' => $request->edit_warehouse_city,
            'contact_no' => $request->edit_warehouse_contact_no,
            'contact_person' => $request->edit_warehouse_contact_person,
            'open_time' => $request->edit_warehouse_operating_hrs_start,
            'close_time' => $request->edit_warehouse_operating_hrs_end
            
        ]);

        return redirect()->route('settings')->withStatus(__('Warehouse successfully updated.'));
    }

    public function brandStore(Request $request)
    {
        Brand::create([
            'name' => $request->brand_name_settings,
            'user_id' => Auth::id()
        ]);

        return redirect()->route('settings')->withStatus(__('Brand successfully added.'));
    }

    public function brandUpdate(Request $request)
    {
        Brand::where('id',$request->id)->update([
            'name' => $request->edit_brands_name
            
        ]);

        return redirect()->route('settings')->withStatus(__('Brand successfully updated.'));
    }
}
