<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MakeModelYear extends Model
{
    protected $fillable = ['make','year','model'];
    public $timestamps = false;
}
