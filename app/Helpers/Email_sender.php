<?php
namespace App\Helpers;

use Config;
use Mail;

class Email_sender
{

    public static function sendConnectionEstablishedMail($data = null){
        
        if ($data != null) 
        {
            $settings                 = [];
            $settings["subject"] = "Affiliate Products";
            $settings['to'] = $data['email'];
            $settings['sender'] = 'Spoto Team';
            $settings['receiver'] = '';
            $settings['service_message'] = $data['message'];
            $settings['txtBody'] = view('emails.sendRequest', $settings)->render();
            unset($settings['txtBody']);
            Self::sendEmail('emails.sendRequest', $settings);
        }
    }

    
    public static function sendEmail($view = null, $settings = null)
    {
        if (!empty($settings) && $view != null) {
            $sent = Mail::send($view, $settings, function ($message) use ($settings) {
                $message->to($settings['to'], $settings['receiver'])->subject($settings['subject']);
            });
        }
    }
}






?>