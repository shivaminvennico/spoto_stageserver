<?php
namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Validators\Failure;
use App\Vehicle;
use App\VehicleSpecification;
use App\SubSpecification;
use App\Specification;
use App\MakeModelYear;
use Illuminate\Support\Facades\Validator;
use DB;

class VehicleImport implements WithValidation, SkipsOnFailure, ToCollection
{
    use Importable, SkipsFailures;
    /**
     * @param Collection $collection
     */

    private $is_mandatory_specs = [];
    private $validation_messages = [];
    private $export_error_data = [];
    public function collection(Collection $collection)
    {
        $parentId = [];
        $checkDuplicate = 0;
        $new_mmy_id = 0;
        $mainSpecs = [];

        foreach ($collection[0] as $key => $value)
        {
            if ($key > 3)
            {
                $astrick = substr($value, 0, 1);
                if ($astrick == '*')
                {
                    $this->is_mandatory_specs[$key] = 'required';
                    $mainSpecs[$key] = substr($value, 1);
                    $this->validation_messages[$key . '.required'] = 'The ' . $mainSpecs[$key] . ' field is required';
                }
                else
                {
                    $this->is_mandatory_specs[$key] = 'nullable';
                }
            }
            else
            {
                $this->validation_messages[$key . '.required'] = 'The ' . $value . ' field is required';
            }

        }

        $lengthOfColumn = count($collection[0]);

        foreach ($collection as $key => $value)
        {
            $errorRequired = [];
            $this->export_error_data[$key] = $value->toArray();

            if ($key > 0)
            {
                $validator = Validator::make($value->toArray() , $this->rules() , $this->validationMessages());
                if ($validator->fails())
                {

                    foreach ($validator->errors()
                        ->messages() as $messages)
                    {

                        foreach ($messages as $error)
                        {
                            // accumulating errors:
                            $errorRequired[] = $error;

                        }
                    }
                    array_push($this->export_error_data[$key], implode(',', $errorRequired));

                }
                else
                {
                    $mmy_id = MakeModelYear::where('make', $value[0])->where('model', $value[1])->where('year', $value[2])->pluck('id');

                    if (isset($mmy_id[0]))
                    {

                        $checkDuplicate = Vehicle::where('mmy_id', $mmy_id[0])->where('variant_name', $value[3])->count();
                        $new_mmy_id = $mmy_id[0];
                    }
                    else
                    {
                        array_push($this->export_error_data[$key], "mmy_id is not found");

                        continue;

                    }
                    if ($checkDuplicate < 1)
                    {

                        $vehicleArr = [];
                        $vehicleArr['mmy_id'] = $new_mmy_id;
                        $vehicleArr['variant_name'] = $value[3];
                        $vehicleArr['from_year'] = $value[2];
                        $vehicleArr['to_year'] = $value[2];

                        DB::transaction(function () use ($vehicleArr, $lengthOfColumn, $parentId, $value, $key)
                        {
                            $Insert = Vehicle::create($vehicleArr);

                            $vehicleSpecArr = [];
                            $vehicleSpecArr['vehicle_id'] = $Insert->id;

                            for ($i = 4;$i < $lengthOfColumn;$i++)
                            {
                                if (($value[$i] != ""))
                                {

                                    $id = SubSpecification::where('specification_id', $parentId[$i][0])->where('sub_name', $value[$i])->first();
                                    if (!isset($id->id))
                                    {
                                        $errorRequired[] = $value[$i] . " not found in database";
                                    }
                                    else
                                    {
                                        $vehicleSpecArr['sub_specification_id'] = $id->id;
                                        VehicleSpecification::create($vehicleSpecArr);
                                    }

                                }
                            }
                            if(isset($errorRequired)){
                                array_push($this->export_error_data[$key], implode(',', $errorRequired));
                            }
                        });
                    }
                    else
                    {
                        array_push($this->export_error_data[$key], "Duplicate Vehicle");

                        continue;
                    }

                }

            }
            else
            {

                for ($i = 4;$i < $lengthOfColumn;$i++)
                {

                    if (isset($mainSpecs[$i]))
                    {
                        $parentId[$i] = Specification::where('name', $mainSpecs[$i])->pluck('id');
                    }
                    else
                    {
                        $parentId[$i] = Specification::where('name', $value[$i])->pluck('id');
                    }

                }

            }

        }

    }

    // this function returns all validation errors after import:
    public function getErrorsData()
    {
        return $this->export_error_data;
    }
    public function rules():
        array
        {

            for ($i = 0;$i < 4;$i++)
            {
                if ($i == 2)
                {
                    $this->is_mandatory_specs[$i] = 'required|max:4|min:4';
                    $this->validation_messages[$i . '.min'] = "Year must be 4 characters";
                    $this->validation_messages[$i . '.max'] = "Year must be 4 characters";

                }
                else
                {
                    $this->is_mandatory_specs[$i] = 'required|max:255';
                }

            }
            ksort($this->is_mandatory_specs);

            return $this->is_mandatory_specs;
        }

        public function validationMessages()
        {

            return $this->validation_messages;
        }
    }
    
