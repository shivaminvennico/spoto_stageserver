<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Validators\Failure;
use App\Product;
use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use App\Warehouse;
use App\ProductWarehouse;
use App\Brand;
use Auth;

class ProductImport implements WithValidation, SkipsOnFailure, ToCollection
{
    use Importable, SkipsFailures;
    /**
    * @param Collection $collection
    */

    private $is_mandatory_specs = [];
    private $validation_messages = [];
    private $export_error_data = [];
    public function collection(Collection $collection)
    {
        foreach ($collection[0] as $key => $value)
        {
            if ($key == 4) {
                $this->is_mandatory_specs[$key] = 'nullable';
            }
            elseif ($key == 5) {
                $this->is_mandatory_specs[$key] = 'required|unique:products,sku_number';
                $this->validation_messages[$key . '.required'] = 'The ' . $value . ' field is required';
                $this->validation_messages[$key . '.unique'] = 'The ' . $value . ' field is must be unique';
            }
            elseif($key == 6 || $key == 7 || $key == 8 || $key == 9 || $key == 11 || $key == 12 || $key == 13 || $key == 14 || $key == 16)
            {
                $this->is_mandatory_specs[$key] = 'required|numeric';
                $this->validation_messages[$key . '.required'] = 'The ' . $value . ' field is required';
                $this->validation_messages[$key . '.numeric'] = 'The ' . $value . ' field must be numeric';
            }
            else {
                $this->is_mandatory_specs[$key] = 'required';
                $this->validation_messages[$key . '.required'] = 'The ' . $value . ' field is required';
            }
            
        }
        

        foreach ($collection as $key => $value) {
            $errorRequired = [];
            $this->export_error_data[$key] = $value->toArray();

            if ($key > 0)
            {
                $validator = Validator::make($value->toArray() , $this->rules() , $this->validationMessages());
                if ($validator->fails())
                {

                    foreach ($validator->errors()
                        ->messages() as $messages)
                    {

                        foreach ($messages as $error)
                        {
                            // accumulating errors:
                            $errorRequired[] = $error;

                        }
                    }
                    array_push($this->export_error_data[$key], implode(',', $errorRequired));

                }
                else 
                {
                    $cat = Category::where('name',$value[2])->first();
                    if (empty($cat->id))
                    {
                        array_push($this->export_error_data[$key], $value[2].' is not match with database records');
                        continue;
                    }
                    if ($value[3]!="") {
                        $sub_cat = SubCategory::where('category_id',$cat->id)->where('sub_name',$value[3])->first();
                        if (empty($sub_cat->id))
                        {
                            array_push($this->export_error_data[$key], $value[3].' is not match with database records');
                            continue;
                        }
                    }
                    
                    if ($value[4]!="") {
                        $sub_sub_cat = SubSubCategory::where('sub_category_id',$sub_cat->id)->where('sub_sub_name',$value[4])->first();

                        if (empty($sub_sub_cat->id))
                        {
                            array_push($this->export_error_data[$key], $value[4].' is not match with database records');
                            continue;
                        }
                    }
                    
                    $brand = Brand::where('name',$value[1])->first();
                    if (empty($brand->id))
                    {
                        array_push($this->export_error_data[$key], $value[1].' is not match with database records');
                        continue;
                    }
                    
                        $createProduct = Product::firstOrCreate([
                            'category_id' =>$cat->id,
                            'sub_category_id' =>isset($sub_cat->id) ? $sub_cat->id : 0 ,
                            'sub_sub_category_id' => isset($sub_sub_cat->id) ? $sub_sub_cat->id : 0 ,
                            'product_name' =>$value[0],
                            'brand_name' =>$brand->id,
                            'unit' =>$value[14],
                            'sku_number'=>$value[5],
                            'unit_price'=>$value[6],
                            'purchase_price'=>$value[7],
                            'photos'=>0,
                            'description'=>$value[10],
                            'tax'=>$value[8],
                            'discount'=>$value[9],
                            'free_shipping' =>$value[11],
                            'local_pickup' => isset($value[12]) ? 1 : 0 ,
                            'local_pickup_shipping_cost'=>$value[12],
                            'flat_rate' => isset($value[13]) ? 1 : 0 ,
                            'flat_rate_shipping_cost'=>$value[13],
                            'created_by'=>Auth::id()
                
                        ]);
    
                    
                   
                        
                        $warehouse = Warehouse::where('user_id',Auth::id())->where('name',$value[15])->first();
                        if (empty($warehouse->id))
                        {
                            array_push($this->export_error_data[$key], $value[15].' is not match with database records');
                            continue;
                        }
                        
                            ProductWarehouse::firstOrCreate([
                                'product_id'=>$createProduct->id,
                                'qty'=>$value[14],
                                'warehouse'=>$warehouse->id,
                                'qty_in_hand'=>$value[16],
                            ]);
                    
                }

            }
            
        }
        
    }

    public function getErrorsData()
    {
        return $this->export_error_data;
    }

    public function rules():
        array
        {


            return $this->is_mandatory_specs;
        }

        public function validationMessages()
        {

            return $this->validation_messages;
        }
}
