<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'category_id','sub_category_id','sub_sub_category_id','mmy_id','vehicle_id','vehicle_name' , 'product_name', 'brand_name','unit', 'sku_number', 'unit_price','purchase_price','description','tax','discount','free_shipping','local_pickup','local_pickup_shipping_cost','flat_rate','flat_rate_shipping_cost','qty','warehouse','qty_in_hand','photos','tier_1','tier_2','tier_3','tier_4','tier_5','tier_6','tier_7','tier_8','tier_9','tier_10','created_by','bundled_product'
    ];
}
