<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVehicle extends Model
{
    protected $fillable = ['product_id','vehicle_id'];
}
