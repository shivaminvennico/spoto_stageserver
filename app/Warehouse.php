<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $fillable = ['name','location','city','contact_no','contact_person','open_time','close_time','user_id'];
}
