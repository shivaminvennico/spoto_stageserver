<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BundleProduct extends Model
{
    protected $fillable = ["product_id","bundled_product"];
}
