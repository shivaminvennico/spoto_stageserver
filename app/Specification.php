<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specification extends Model
{
    protected $fillable = ['name','is_mandatory'];

    public function subSpecification(){
        return $this->hasMany('App\SubSpecification', 'specification_id');
    }
}
