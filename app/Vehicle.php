<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'mmy_id','variant_name','photo','from_year','to_year'
    ];
}
