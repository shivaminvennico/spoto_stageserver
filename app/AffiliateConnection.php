<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffiliateConnection extends Model
{
   protected $fillable = ["requester_id","receiver_id","status","tier_price"];
}
