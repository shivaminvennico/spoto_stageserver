<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TierLabel extends Model
{
    protected $fillable = ['user_id','tier_1','tier_2','tier_3','tier_4','tier_5','tier_6','tier_7','tier_8','tier_9','tier_10'];
}
