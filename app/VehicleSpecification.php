<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleSpecification extends Model
{
    protected $fillable = [
        'vehicle_id','sub_specification_id'
    ];
}
