<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffiliateProduct extends Model
{
    protected $fillable = ["affiliate_connection_id","product_id","unit_price","description","sku_number","qty","free_shipping","local_pickup","loca_pickup_shipping_cost","flat_rate","flat_rate_shipping_cost"];
}
