<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class VehicleExport implements FromArray, ShouldAutoSize, WithEvents
{
    protected $invoices;
    protected $cellRange;
    protected $arr;
    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }
    public function array(): array
    {
        

        
        foreach ($this->invoices as $key => $value) {
            if ($key == 0) {
                $this->arr[$key] = $value; 
            }
            if (count($this->invoices[0]) < count($this->invoices[$key])){  
                $this->arr[$key] = $value; 
            }
        }
        
        
        return $this->arr;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                foreach ($this->arr as $key => $value) {
                   
                        $this->cellRange = 'A'.($key+2).':HZ'.($key+2);
                        $event->sheet->getDelegate()->getStyle($this->cellRange)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);;
                    
                 }
                
                
            },
        ];
    }

    
}
