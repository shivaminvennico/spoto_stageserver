<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table){
            $table->tinyInteger('user_type')->default(5)->after('remember_token');
            $table->string('documents', 255)->nullable()->after('user_type');
            $table->string('mobile_no', 50)->nullable()->after('documents');
            $table->string('country', 50)->nullable()->after('mobile_no');
            $table->string('zip_code', 20)->nullable()->after('country');
            $table->text('address_1')->nullable()->after('zip_code');
            $table->text('address_2')->nullable()->after('address_1');
            $table->tinyInteger('address_type')->default(1)->comment('1 is for office/commericial and 2 is for home by default it will be 1')->after('address_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['user_type', 'documents', 'mobile_no','country','zip_code','address_1','address_2','address_type']);
        });
    }
}
