<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnsFromProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('mmy_id');
            $table->dropColumn('vehicle_id');
            $table->dropColumn('vehicle_name');
            $table->dropColumn('qty');
            $table->dropColumn('warehouse');
            $table->dropColumn('qty_in_hand');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('mmy_id')->after('sub_sub_category_id');
            $table->integer('vehicle_id')->after('mmy_id');
            $table->string('vehicle_name',50)->after('vehicle_id');
            $table->integer('qty')->after('flat_rate_shipping_cost');
            $table->string('warehouse',150)->after('qty');
            $table->integer('qty_in_hand')->after('warehouse');
        });
    }
}
