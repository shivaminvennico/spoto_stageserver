<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProdtuctsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function($table){
            $table->integer('mmy_id')->after('sub_sub_category_id');
            $table->integer('vehicle_id')->after('mmy_id');
            $table->string('vehicle_name',50)->after('vehicle_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('mmy_id');
            $table->dropColumn('vehicle_id');
            $table->dropColumn('vehicle_name');
        });
    }
}
