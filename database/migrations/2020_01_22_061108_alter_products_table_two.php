<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProductsTableTwo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function($table){
            $table->float('tier_1',5,2)->after('photos')->nullable();
            $table->float('tier_2',5,2)->after('tier_1')->nullable();
            $table->float('tier_3',5,2)->after('tier_2')->nullable();
            $table->float('tier_4',5,2)->after('tier_3')->nullable();
            $table->float('tier_5',5,2)->after('tier_4')->nullable();
            $table->float('tier_6',5,2)->after('tier_5')->nullable();
            $table->float('tier_7',5,2)->after('tier_6')->nullable();
            $table->float('tier_8',5,2)->after('tier_7')->nullable();
            $table->float('tier_9',5,2)->after('tier_8')->nullable();
            $table->float('tier_10',5,2)->after('tier_9')->nullable();
            $table->integer('created_by')->after('tier_10');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('tier_1');
            $table->dropColumn('tier_2');
            $table->dropColumn('tier_3');
            $table->dropColumn('tier_4');
            $table->dropColumn('tier_5');
            $table->dropColumn('tier_6');
            $table->dropColumn('tier_7');
            $table->dropColumn('tier_8');
            $table->dropColumn('tier_9');
            $table->dropColumn('tier_10');
            $table->dropColumn('created_by');
        });
    }
}
