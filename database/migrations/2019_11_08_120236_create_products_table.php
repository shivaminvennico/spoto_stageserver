<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('category_id');
            $table->Integer('sub_category_id')->nullable();
            $table->Integer('sub_sub_category_id')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->string('product_name', 100);
            $table->string('brand_name', 20);
            $table->Integer('unit');
            $table->string('sku_number', 50);
            $table->decimal('unit_price',8,2);
            $table->decimal('purchase_price',8,2);
            $table->string('description',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
