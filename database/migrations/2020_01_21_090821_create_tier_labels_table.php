<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTierLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tier_labels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('tier_1')->default('Tier 1');
            $table->string('tier_2')->default('Tier 2');
            $table->string('tier_3')->default('Tier 3');
            $table->string('tier_4')->default('Tier 4');
            $table->string('tier_5')->default('Tier 5');
            $table->string('tier_6')->default('Tier 6');
            $table->string('tier_7')->default('Tier 7');
            $table->string('tier_8')->default('Tier 8');
            $table->string('tier_9')->default('Tier 9');
            $table->string('tier_10')->default('Tier 10');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tier_labels');
    }
}
