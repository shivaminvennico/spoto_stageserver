<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAffiliateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('affiliate_products', function($table){
            $table->string('unit_price')->after('product_id')->nullable();
            $table->string('description')->after('unit_price')->nullable();
            $table->string('sku_number')->after('description')->nullable();
            $table->string('qty')->after('sku_number')->nullable();
            $table->string('free_shipping')->after('qty')->nullable();
            $table->string('local_pickup')->after('free_shipping')->nullable();
            $table->string('local_pickup_shipping_cost')->after('local_pickup')->nullable();
            $table->string('flat_rate')->after('local_pickup_shipping_cost')->nullable();
            $table->string('flat_rate_shipping_cost')->after('flat_rate')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliate_products', function (Blueprint $table) {
            $table->dropColumn('unit_price');
            $table->dropColumn('description');
            $table->dropColumn('sku_number');
            $table->dropColumn('qty');
            $table->dropColumn('free_shipping');
            $table->dropColumn('local_pickup');
            $table->dropColumn('local_pickup_shipping_cost');
            $table->dropColumn('flat_rate');            
            $table->dropColumn('flat_rate_shipping_cost');
           
        });
    }
}
