<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function($table){
            $table->integer('tax')->after('description')->default(0);
            $table->integer('discount')->after('tax')->default(0);
            $table->integer('free_shipping')->after('discount')->default(0);
            $table->integer('local_pickup')->after('free_shipping')->default(0);
            $table->integer('local_pickup_shipping_cost')->after('local_pickup');
            $table->integer('flat_rate')->after('local_pickup_shipping_cost');
            $table->integer('flat_rate_shipping_cost')->after('flat_rate');
            $table->integer('qty')->after('flat_rate_shipping_cost');
            $table->string('warehouse',150)->after('qty');
            $table->integer('qty_in_hand')->after('warehouse');
            $table->string('photos')->after('qty_in_hand');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('tax');
            $table->dropColumn('discount');
            $table->dropColumn('free_shipping');
            $table->dropColumn('local_pickup');
            $table->dropColumn('local_pickup_shipping_cost');
            $table->dropColumn('flat_rate');
            $table->dropColumn('flat_rate_shipping_cost');
            $table->dropColumn('qty');
            $table->dropColumn('warehouse');
            $table->dropColumn('qty_in_hand');
            $table->dropColumn('photos');
        });
    }
}
