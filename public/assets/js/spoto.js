$('.parent-category').click(function() {
    var parent_id = $(this).attr('id');
    $('.parent-category').removeClass('active-category');
    $(this).addClass('active-category');
        $.ajax({
            url:"/get/sub_category",
            method:'POST',
            data:{parent:parent_id,"_token": $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#loadSub').hide().html(data).fadeIn('slow');
            }
        });   
});

$('.parent-category').mouseover(function() {
   
    $(this).children('i').addClass('nav-icon i-Pen-2 m-2 editIcon');
    });
    
    $('.parent-category').mouseout(function() {
        $(this).children('i').removeClass('nav-icon i-Pen-2 m-2 editIcon');
    });
    
    $(document).on('click','.editIcon',function() {
       var id = $(this).parent('li').attr('id');
       $('.parent-category').css('display','block');
       $('#'+id).css('display','none');
       $('.input-category').css('display','none');
       $('#div'+id).css('display','block');
    });
        
        
    // $(document).on('click','.btnSubmitCat',function() {
        function EditCategory(id) {
        // var inputId = $(this).children("span").data('id');
        var inputId = id;
        var inputValue = $('#input'+inputId).val();
        $.ajax({
            url:"/get/edit_cat_input",
            method:'POST',
            data:{parent:inputId,name:inputValue,"_token": $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                if(data == 'dublicate')
                {
                    toastr.warning(
                        "This category is in used", 
                    {
                        showMethod: "slideDown",
                        hideMethod: "slideUp",
                        timeOut: 2e3
                    });
                    $('.parent-category').css('display','block');
                    $('.input-category').css('display','none');
                }
                else
                {
                    $('#'+inputId).html(data['name'] + "<i></i>");
                    $('.parent-category').css('display','block');
                    $('.input-category').css('display','none');
                    toastr.success(
                        "Category name changed successfully.", 
                    {
                        showMethod: "slideDown",
                        hideMethod: "slideUp",
                        timeOut: 2e3
                    });
                }    
            }
            
        }).fail (function(message){
            toastr.error(
                message.responseJSON.errors.name, 
            {
                showMethod: "slideDown",
                hideMethod: "slideUp",
                timeOut: 2e3
            });
            $('.parent-category').css('display','block');
            $('.input-category').css('display','none');
    
        });   
    }

$('.parent-specification').click(function() {
    var parent_id = $(this).data('id');
    $('.parent-specification').removeClass('active-category');
    $(this).addClass('active-category');
        $.ajax({
            url:"/get/sub_specification",
            method:'POST',
            data:{parent:parent_id,"_token": $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#loadSubSpecs').hide().html(data).fadeIn('slow');
            }
        });   
});

$('.parent-specification').mouseover(function() {
   
    $(this).children('i').addClass('nav-icon i-Pen-2 m-2 editIcon');
    });
    
    $('.parent-specification').mouseout(function() {
        $(this).children('i').removeClass('nav-icon i-Pen-2 m-2 editIcon');
    });
    
    $(document).on('click','.editIcon',function() {
       var id = $(this).parent('li').attr('id');
       $('.parent-specification').css('display','block');
       $('#'+id).css('display','none');
       $('.input-specification').css('display','none');
       $('#div-'+id).css('display','block');
    });
        
        
    // $(document).on('click','.btnSubmitCat',function() {
        function EditSpecification(id) {
        // var inputId = $(this).children("span").data('id');
        var inputId = id;
        var inputValue = $('#input-specs'+inputId).val();
        var is_mandatory =  $('#edit_is_mandatory'+inputId).is(":checked");
        $.ajax({
            url:"/get/edit_specs_input",
            method:'POST',
            data:{parent:inputId,name:inputValue,mandatory:is_mandatory,"_token": $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                if(data == 'dublicate')
                {
                    toastr.warning(
                        "You can not update this specification", 
                    {
                        showMethod: "slideDown",
                        hideMethod: "slideUp",
                        timeOut: 2e3
                    });
                    $('.parent-specification').css('display','block');
                    $('.input-specification').css('display','none');
                }
                else
                {
                    $('#specs'+inputId).html(data['name'] + "<i></i>");
                    $('.parent-specification').css('display','block');
                    $('.input-specification').css('display','none');
                    toastr.success(
                        "Specification name changed successfully.", 
                    {
                        showMethod: "slideDown",
                        hideMethod: "slideUp",
                        timeOut: 2e3
                    });
                }    
            }
            
        }).fail (function(message){
            toastr.error(
                message.responseJSON.errors.name, 
            {
                showMethod: "slideDown",
                hideMethod: "slideUp",
                timeOut: 2e3
            });
            $('.parent-specification').css('display','block');
            $('.input-specification').css('display','none');
    
        });   
    }
    $(document).on('mouseover','.sub-specification',function() {
   
        $(this).children('i').addClass('nav-icon i-Pen-2 m-2 editSubSpecsIcon');
        $(this).children('i').attr('style','cursor:pointer');
        });
        
    $(document).on('mouseout','.sub-specification',function() {
        $(this).children('i').removeClass('nav-icon i-Pen-2 m-2 editSubSpecsIcon');
        $(this).children('i').attr('style','');
    });
        
        $(document).on('click','.editSubSpecsIcon',function() {
           var id = $(this).parent('li').attr('id');
           $('.sub-specification').css('display','block');
           $('#'+id).css('display','none');
           $('.input-sub-specification').css('display','none');
           $('#div-'+id).css('display','block');
        });
            
            
        // $(document).on('click','.btnSubmitCat',function() {
            function EditSubSpecification(id) {
            // var inputId = $(this).children("span").data('id');
            var inputId = id;
            var inputValue = $('#input-sub-specs'+inputId).val();
            $.ajax({
                url:"/get/edit_sub_specs_input",
                method:'POST',
                data:{parent:inputId,name:inputValue,"_token": $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    if(data == 'dublicate')
                    {
                        toastr.warning(
                            "You can not update this sub specification", 
                        {
                            showMethod: "slideDown",
                            hideMethod: "slideUp",
                            timeOut: 2e3
                        });
                        $('.sub-specification').css('display','block');
                        $('.input-sub-specification').css('display','none');
                    }
                    else
                    {
                        $('sub-specs#'+inputId).html(data['name'] + "<i></i>");
                        $('.sub-specification').css('display','block');
                        $('.input-sub-specification').css('display','none');
                        toastr.success(
                            "Sub Specification name changed successfully.", 
                        {
                            showMethod: "slideDown",
                            hideMethod: "slideUp",
                            timeOut: 2e3
                        });
                    }    
                }
                
            }).fail (function(message){
                toastr.error(
                    message.responseJSON.errors.name, 
                {
                    showMethod: "slideDown",
                    hideMethod: "slideUp",
                    timeOut: 2e3
                });
                $('.sub-specification').css('display','block');
                $('.input-sub-specification').css('display','none');
        
            });   
        }
    
$(document).on('mouseover','.sub-category',function() {
   
    $(this).children('i').addClass('nav-icon i-Pen-2 m-2 editIconSub');
    });
    
$(document).on('mouseout','.sub-category',function() {
    $(this).children('i').removeClass('nav-icon i-Pen-2 m-2 editIconSub');
});
    
    $(document).on('click','.editIconSub',function() {
       
       var id = $(this).parent('li').attr('id');
       $('.sub-category').css('display','block');
       $('#'+id).css('display','none');
       $('.input-sub-category').css('display','none');
       $('#div-'+id).css('display','block');
    });
    
function EditSubCategory(id) {
   
    var inputId = id;
    var inputValue = $('#input-sub'+inputId).val();
    
    $.ajax({
        url:"/get/edit_sub_cat_input",
        method:'POST',
        data:{parent:inputId,parentValue:inputValue,"_token": $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            if(data == 'dublicate')
            {
                toastr.warning(
                    "This category is in used", 
                {
                    showMethod: "slideDown",
                    hideMethod: "slideUp",
                    timeOut: 2e3
                });
                $('.sub-category').css('display','block');
                $('.input-sub-category').css('display','none');
            }
            else
            {
                $('#sub'+inputId).html(data['parentValue'] + "<i></i>");
                $('.sub-category').css('display','block');
                $('.input-sub-category').css('display','none');
                toastr.success(
                    "Sub Category name changed successfully.", 
                {
                    showMethod: "slideDown",
                    hideMethod: "slideUp",
                    timeOut: 2e3
                });
            }
            
        }
    }).fail (function(message){
        toastr.error(
            message.responseJSON.message, 
        {
            showMethod: "slideDown",
            hideMethod: "slideUp",
            timeOut: 2e3
        });
        $('.sub-category').css('display','block');
        $('.input-sub-category').css('display','none');

    });   
}
//sub sub cat
$(document).on('mouseover','.sub-sub-category',function() {
   
    $(this).children('i').addClass('nav-icon i-Pen-2 m-2 editIconSubSub');
    $(this).children('i').attr('style','cursor:pointer');
    });
    
$(document).on('mouseout','.sub-sub-category',function() {
    $(this).children('i').removeClass('nav-icon i-Pen-2 m-2 editIconSubSub');
    $(this).children('i').attr('style','');
});
    
    $(document).on('click','.editIconSubSub',function() {
       
       var id = $(this).parent('li').attr('id');
       $('.sub-sub-category').css('display','block');
       $('#'+id).css('display','none');
       $('.input-sub-sub-category').css('display','none');
       $('#div-'+id).css('display','block');
    });
    
function EditSubSubCategory(id) {
   
    var inputId = id;
    var inputValue = $('#input-sub-sub'+inputId).val();
    
    $.ajax({
        url:"/get/edit_sub_sub_cat_input",
        method:'POST',
        data:{parent:inputId,subParentValue:inputValue,"_token": $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            if(data == 'dublicate')
            {
                toastr.warning(
                    "This category is in used", 
                {
                    showMethod: "slideDown",
                    hideMethod: "slideUp",
                    timeOut: 2e3
                });
                $('.sub-sub-category').css('display','block');
                $('.input-sub-sub-category').css('display','none');
            }
            else
            {
                $('#sub-sub'+inputId).html(data['subParentValue'] + "<i></i>");
                $('.sub-sub-category').css('display','block');
                $('.input-sub-sub-category').css('display','none');
                toastr.success(
                    "Sub Sub Category name changed successfully.", 
                {
                    showMethod: "slideDown",
                    hideMethod: "slideUp",
                    timeOut: 2e3
                });
            }
        }
    }).fail (function(message){
        toastr.error(
            message.responseJSON.message, 
        {
            showMethod: "slideDown",
            hideMethod: "slideUp",
            timeOut: 2e3
        });
        $('.sub-sub-category').css('display','block');
        $('.input-sub-sub-category').css('display','none');

    });   
}

$(document).on("click", '.sub-category', function(event) {
    
    var sub_id = $(this).data('id');
    $('.sub-category').removeClass('active-category');
    $(this).addClass('active-category');
        $.ajax({
            url:"/get/sub_sub_category",
            method:'POST',
            data:{sub_parent:sub_id,"_token": $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $('#loadSubSub').hide().html(data).fadeIn('slow');
            }
        });   
});


//Show Add Vechicle Page
$(document).on("click", '#showWizardSpecs', function(event) {
    if($('#input-variant_name').val()!="" && $('#make').val() !="" && $('#makeYear').val()!="" && $('#mmy_id').val()!="")
    {
        $('#addVehicleForm').fadeOut('fast');
        $('#wizardSpecsForm').fadeIn('slow');
    }
    else
    {
        $("#formVehicle").valid();
        toastr.error(
            "All fields are required", 
        {
            showMethod: "slideDown",
            hideMethod: "slideUp",
            timeOut: 2e3
        });
    }
   
    
});

$(document).on("click", '#backbtnOnWizard', function(event) {
    $('#addVehicleForm').fadeIn('fast');
    $('#wizardSpecsForm').fadeOut('fast');
});



$(document).on('change','#categoryId', function() {
    
    var parent_id = this.value;
    $.ajax({
        url:"/get/sub_category",
        method:'POST',
        data:{product:true,parent:parent_id,"_token": $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            $('#loadSubCategoryForProduct').hide().html(data).fadeIn('slow');
        }
    });
  });
$(document).on("change", '#subCategoryId', function(event) {
    var sub_id = this.value;
    $.ajax({
        url:"/get/sub_sub_category",
        method:'POST',
        data:{product:true,sub_parent:sub_id,"_token": $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            $('#loadSubSubCategoryForProduct').hide().html(data).fadeIn('slow');
        }
    });
  });

function isNumberKey(evt, enabledot) {
    var evt;
    if (!enabledot) enabledot = 0;
    var charCode = (evt.which) ? evt.which : event.keyCode
    //alert(charCode)
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46 && enabledot == 1)
        return false;
    else if (charCode > 31 && (charCode < 48 || charCode > 57) && enabledot == 0)
        return false;

    return true;
}
$('#make').on('change', function() {
    var Make = this.value;
    var edit = 0;
    if ($(this).attr('data-id')) {
        edit = $(this).attr('data-id');
    }
    $.ajax({
        url:"/get/make_year",
        method:'POST',
        data:{make:Make,'edit':edit,"_token": $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            $('#loadYear').hide().html(data).fadeIn('slow');
            if (Make == 1) {
               
                $('#make').parent('div').html("<div class=\"form-group\"><input type='text' name='make' id='make'  class='form-control' placeholder='Make' required/></div>");
                $('#makeYear').trigger('change');
                $('#input-variant_name').parent('div').remove();
                $('#showWizardSpecs').replaceWith('<button type="button" class="btn btn-primary" id="addMmy">Save</button')
            }
        }
    });
  });
  
  $(document).on('click',"#addMmy",function(){
    if($('#make').val() !="" && $('#makeYear').val()!="" && $('#mmy_id').val()!="")
    {
        var make = $('#make').val();
        var year = $('#makeYear').val();
        var model = $('#mmy_id').val();

        $.ajax({
            url:"/store/mmy",
            method:'POST',
            data:{'make':make,'year':year,'model':model,"_token": $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                if (data == "duplicate") {
                    toastr.info(
                        "Duplicate Records", 
                    {
                        showMethod: "slideDown",
                        hideMethod: "slideUp",
                        timeOut: 2e3
                    }); 
                }
                else if(data=="error"){
                    toastr.error(
                        "Please enter full year", 
                    {
                        showMethod: "slideDown",
                        hideMethod: "slideUp",
                        timeOut: 2e3
                    });

                }
                else{
                    toastr.success(
                        "Record inserted in database", 
                    {
                        showMethod: "slideDown",
                        hideMethod: "slideUp",
                        timeOut: 2e3
                    });
                }
                
                window.setTimeout(function(){location.reload()},2000)
            }
        });

    }
    else
    {
        $("#formVehicle").valid();
        toastr.error(
            "All fields are required", 
        {
            showMethod: "slideDown",
            hideMethod: "slideUp",
            timeOut: 2e3
        });
    }
  });
  

  $(document).on("change", '#makeYear', function(event) {
    var makeYear = this.value;

    var selectedMake = $('#make').val();
    var edit = 0;
    if ($(this).attr('data-id')) {
        edit = $(this).attr('data-id');
        
    }
    $.ajax({
        url:"/get/year_model",
        method:'POST',
        data:{make:selectedMake,year:makeYear,'edit':edit,"_token": $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            if (makeYear == 1) {

                $('#makeYear').parent('div').html("<div class=\"form-group\"><input type='text' name='year' id='makeYear'  class='form-control' placeholder='Year' onkeyup='digits();' required/></div>");
                $('#input-variant_name').parent('div').remove();
                $('#showWizardSpecs').replaceWith('<button type="button" class="btn btn-primary" id="addMmy">Save</button')
            }
            $('#loadModel').hide().html(data).fadeIn('slow');
            
        }
    });
  });

  //Load permission when role change
  $(document).on("change", '#role', function(event) {
    var role = this.value;
    if (role !="") {
        $.ajax({
            url:"role/"+ role + "/edit",
            method:'get',
            
            success: function(data) {
                
                 $('#loadPermissions').hide().html(data).fadeIn('slow');
                
            }
        });
    }
    else{
        $('#loadPermissions').hide();
    }
    
  });


  function digits() {
    
        $('#makeYear').val( $('#makeYear').val().replace(/(\d{4})(?=(\d)+$)/g, '$1,') ); 
  
  }

  $(document).on("change", '#mmy_id', function(event) {
    var mmy_id = this.value;
    
            if (mmy_id == "new") {

                $('#mmy_id').parent('div').html("<div class=\"form-group\"><input type='text' name='mmy_id' id='mmy_id'  class='form-control' placeholder='Model' required/></div>");
                $('#input-variant_name').parent('div').remove();
                $('#showWizardSpecs').replaceWith('<button type="button" class="btn btn-primary" id="addMmy">Save</button')
            }

  });
  
  $('#makeId').on('change', function() {
    var make_name = this.value;
    $.ajax({
        url:"/get/vehicle_model",
        method:'POST',
        data:{makeName:make_name,"_token": $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            $('#loadVehiclModel').hide().html(data).fadeIn('slow');
        }
    });
  });
  $(document).on("change", '#makeModel', function(event) {
    var makeModel = this.value;
    $.ajax({
        url:"/get/vehicle_year",
        method:'POST',
        data:{ModelName:makeModel,"_token": $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            $('#loadVehiclYear').hide().html(data).fadeIn('slow');
        }
    });
  });
  $(document).on("change", '#VehicleYear', function(event) {
    var vehical_year = this.value;
    $.ajax({
        url:"/get/vehicle_variant",
        method:'POST',
        data:{VehicleYear:vehical_year,"_token": $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            $('#loadVehiclVariant').hide().html(data).fadeIn('slow');
            
            $('#vehicleId').multiselect({
                columns: 1,
                placeholder: "Select Variant",
                search:true,
                selectAll:true
            });
        }
    });
  });

  $(document).on("change", '#vehicleId', function(event) {
    if($(this).val()=="")
    {
        $('#submitFinish').css('display','none');
    }
    else{
        $('#submitFinish').css('display','');
    }
    
  });


  //On Click of vehicle's row
  $(document).on("click", '.vehicleRows', function(event) {
    var vehicleID = $(this).data('id');
    window.location = "/get/vehicle_details/"+vehicleID;
    
  });

  //On Click of product's row
  $(document).on("click", '.productRows', function(event) {
    var productID = $(this).data('id');
    window.location = "/get/product_details/"+productID;
    
  });
 
  //SearchBar inside add product ---- Mappping vehicle
  $(document).on("keyup", '#searchBarInput', function(event) {
     var searchVal = $(this).val();
     
      if(searchVal.length>4)
      {
        $.get({
            url:"/product/add/searchBar",
            data:{"searchVal":searchVal},
            success:function(res)
            {
                
                $("#searchBar").multiselect('loadOptions', []);
                $("#searchBar").multiselect('reload');
               
                if(res!="")
                {
                    $.each(res,function(index,val) {
                        $('#searchBar').append('<option value="'+val.vehicle_id+'" data-id="'+val.id+'">'+val.make+' '+val.model+' '+val.variant_name+'</option>');
                    });
                    $("#searchBar").multiselect('reload');
                    $("#searchBar").siblings("div.ms-options-wrap").addClass('ms-active');
                    
                }
                
            }
    
        });
      }
      
    
});

// map more vehicles inside product
var arrVehicleId = [];
var editArrVehicleId = [];
$(document).on('change','#searchBar',function(){
 
    if (editArrVehicleId.length === 0) 
    {
        for (var index = 0; index < $('#editMappedVehicleList').find('li').length; index++) {
           
            editArrVehicleId.push($('#editMappedVehicleList').find('li#'+index+'').children('input').val());
            
        }
        
    }
    $.each($('#searchBar').find('option:selected'),function(){
        if(($.inArray($(this).val(),arrVehicleId))== -1)
                {
                    var selText = $(this).text();
                    $('#mappedVehicleList').fadeIn();
                    $('#mappedVehicleList').find('ul').append('<li class="list-group-item">'+selText+'<button type="button" class="float-right btn btn-danger removeMappedVehicle"><i class="nav-icon i-Close-Window"></i></button><input type="hidden" name="mapVehicleIds[]" value="'+$(this).val()+'"/></li>');
                    arrVehicleId.push($(this).val());
                    $('#submitFinish').css('display','');
                   
                }
                
                if(($.inArray($(this).val(),editArrVehicleId))== -1)
                {
                    var selText = $(this).text();
                    $('#editMappedVehicleList').fadeIn();
                    $('#editMappedVehicleList').find('ul').append('<li class="list-group-item">'+selText+'<button type="button" class="float-right btn btn-danger editRemoveMappedVehicle"><i class="nav-icon i-Close-Window"></i></button><input type="hidden" name="editmapVehicleIds[]" value="'+$(this).val()+'"/></li>');
                    editArrVehicleId.push($(this).val());
                   
                }
      
    });
    
});

$(document).on('click','.removeMappedVehicle',function(){
    var inputField = $(this).siblings('input').val();
    arrVehicleId = $.grep(arrVehicleId,function(value)
    {
        return value != inputField;
    });
    
    
    $(this).closest('li').remove()
    if(arrVehicleId.length === 0)
    {
        $('#mappedVehicleList').fadeOut();
        $('#submitFinish').css('display','none');
    }
});

$(document).on('click','.editRemoveMappedVehicle',function(){
    var inputField = $(this).siblings('input').val();
    editArrVehicleId = $.grep(editArrVehicleId,function(value)
    {
        return value != inputField;
    });
    
    
    $(this).closest('li').remove()
    if(editArrVehicleId.length === 0)
    {
        $('#editMappedVehicleList').fadeOut();
    }
});

$(document).on('click', '#mapMoreVehicles', function(){  
    if ($("#makeId").val()){
        
        
        var flag = 0;
        $("#vehicleId option:selected").each(function () {
            var $this = $(this);
            if ($this.length) {
                
                
                if(($.inArray($this.val(),arrVehicleId))== -1)
                {
                    var selText = $this.text();
                    $('#mappedVehicleList').fadeIn();
                    $('#mappedVehicleList').find('ul').append('<li class="list-group-item">'+$('#makeId').val()+' '+$('#makeModel').val()+' '+selText+'<button type="button" class="float-right btn btn-danger removeMappedVehicle"><i class="nav-icon i-Close-Window"></i></button><input type="hidden" name="mapVehicleIds[]" value="'+$this.val()+'"/></li>');
                    arrVehicleId.push($this.val());
                }
                else
                {
                    flag = 1;
                }
                
                
               }
            
         });

         if(flag==1)
         {
            toastr.info(
                "This vehicle is already mapped", 
            {
                showMethod: "slideDown",
                hideMethod: "slideUp",
                timeOut: 2e3
            });
         }
         
    
        
    }
    
    
});


$(document).on('click', '#editMapMoreVehicles', function(){  
    if ($("#makeId").val()){
        if (editArrVehicleId.length === 0) 
        {
            for (var index = 0; index < $('#editMappedVehicleList').find('li').length; index++) {
            
                editArrVehicleId.push($('#editMappedVehicleList').find('li#'+index+'').children('input').val());
            }
            
        }
        
        var flag = 0;
        $("#vehicleId option:selected").each(function () {
            var $this = $(this);
            if ($this.length) {
                
                
                if(($.inArray($(this).val(),editArrVehicleId))== -1)
                {
                    var selText = $(this).text();
                    $('#editMappedVehicleList').fadeIn();
                    $('#editMappedVehicleList').find('ul').append('<li class="list-group-item">'+$('#makeId').val()+' '+$('#makeModel').val()+' '+selText+'<button type="button" class="float-right btn btn-danger editRemoveMappedVehicle"><i class="nav-icon i-Close-Window"></i></button><input type="hidden" name="editmapVehicleIds[]" value="'+$(this).val()+'"/></li>');
                    editArrVehicleId.push($(this).val());
                   
                }
                else
                {
                    flag = 1;
                }
                
                
               }
            
         });

         if(flag==1)
         {
            toastr.info(
                "This vehicle is already mapped", 
            {
                showMethod: "slideDown",
                hideMethod: "slideUp",
                timeOut: 2e3
            });
         }
         
    
        
    }
    
});

$(document).on('DOMSubtreeModified', '#product_edit_form', function(){
    $('#submitFinish').css('display','');
});

$(document).on('DOMSubtreeModified','div#full-editor',function(){
    if($(this).children('div').text()!=""){
        $('#product_description').val($(this).children('div').html());
    }
    
    
});


$(document).on('click', "#editWarehouseItem", function() {
    $(this).addClass('edit-item-trigger-clicked'); //useful for identifying which trigger was clicked and consequently grab data from the correct row and not the wrong one.

    var options = {
      'backdrop': 'static'
    };
    $('#editWarehouseModal').modal(options)
  });

  // on modal show
  $('#editWarehouseModal').on('show.bs.modal', function() {
    var el = $(".edit-item-trigger-clicked"); // See how its usefull right here? 
    var row = el.closest(".h5");

    // get the data
    var id = el.data('item-id');
    var name = row.children(".name").text().trim();
    var location = row.children(".location").text().trim();
    var city = row.children(".city").text().trim();
    var contact_no = row.children(".contact_no").text().trim();
    var contact_person = row.children(".contact_person").text().trim();
    var operating_time = row.children(".operating_time").text().split('-');
   
    // fill the data in the input fields
    $('#update_id').val(id);
    $("#edit_warehouse_name").val(name);
    $("#edit_warehouse_location").val(location);
    $("#edit_warehouse_city").val(city);
    $("#edit_warehouse_contact_no").val(contact_no);
    $("#edit_warehouse_contact_person").val(contact_person);
    $("#edit_warehouse_operating_hrs_start").val(operating_time[0].trim());
    $("#edit_warehouse_operating_hrs_end").val(operating_time[1].trim());

  });

  // on modal hide
  $('#editWarehouseModal').on('hide.bs.modal', function() {
    $('.edit-item-trigger-clicked').removeClass('edit-item-trigger-clicked')
    $("#editWarehouseItem").trigger("reset");
  });

//add multiple warehouse when add btn click
var warehouseField = 1;
var index = {};

var editWarehouse = true;
var divId="";
$(document).on('click','#addWarehouseBtn',function()
{
    
    divId= $(this).parent('div').parent('div').parent('div').attr('id');
   
        if ($('div#'+divId).find("#1").find('.warehouse').val() != "" ) {
        
            if(warehouseField==1)
            {
               
                $('div#'+divId).find("#"+warehouseField).find('.warehouse option').each(function() {
                   
                
                        if ($(this).val() == "") {
                            index[0] = ($(this).text());
                        }
                        else{
                            index[$(this).val()] = ($(this).text());
                        }
                    
                    
                });
                $('div#'+divId).find('#'+warehouseField).find('input[type=hidden]').remove();
                $('div#'+divId).find('#'+warehouseField).append('<input type="hidden" name="warehouse[]" value="'+$('div#'+divId).find("#"+warehouseField).find('.warehouse').val()+'"/>')

                if (typeof $(this).data('id') != "undefined" && editWarehouse == true) 
                {
                    warehouseField=$(this).data('id');
                    $('div#'+divId).find("#"+warehouseField).find('.warehouse option').each(function() {
                        
                    
                            if ($(this).val() == "") {
                                index[0] = ($(this).text());
                            }
                            else{
                                index[$(this).val()] = ($(this).text());
                            }
                        
                        
                    });
                    editWarehouse=false;
                    
                }
            }
        

        
                $html = $('div.addMultipleWarehouse').html();
                
                
                if ($('div#'+divId).find("#"+warehouseField).find('.warehouse').val() != "0") 
                {
                        if ($('div#'+divId).find("#"+warehouseField).find('.warehouse').val() != null) {
                            $('div#'+divId).find('#'+warehouseField).find('input[type=hidden]').remove();
                            $('div#'+divId).find("#"+warehouseField).append('<input type="hidden" name="warehouse[]" value="'+$('div#'+divId).find("#"+warehouseField).find('.warehouse').val()+'"/>');
                            $('div#'+divId).find("#"+warehouseField).find('.warehouse').attr('disabled',true);
                        }
                           
                            
                        
                            $('div#additionalWarehouse').append($html);
                            
                            $('div#additionalWarehouse').find("#1").attr('id',++warehouseField);
                            $('div#'+divId).find("#"+warehouseField).find('input[type=text]').val('');
                            $('div#'+divId).find("#"+warehouseField).find('.warehouse').attr('disabled',false);
                            $('div#'+divId).find("#"+warehouseField).find('.warehouse').val('');
                            $('div#additionalWarehouse').find('#'+warehouseField).append('<button type="button"  class="btn btn-danger removeAdditionalWarehouse">X</button>');
                            
                            $('div#'+divId).find(".row").find('.warehouse option:selected').each(function() {
                                    var v1 = ($(this).val());
                                    
                                   
                                        index = Object.keys(index).reduce((object, key) => {
                                            if (key != v1) {
                                            object[key] = index[key]
                                            }
                                            return object
                                        }, {})
                                   
                                        
                                    
                                });
                                
                            $('div#additionalWarehouse').find('#'+warehouseField).find('.warehouse option').remove();
                            $.each(index,function(i,val){
                                if (i !="") {
                                    $('div#additionalWarehouse').find('#'+warehouseField).find('.warehouse').append("<option value='"+i+"'>"+val+"</option>")
                                }
                            });
                    if(Object.keys(index).length < 2 )
                    {
                        
                        $('div#additionalWarehouse').find('#'+warehouseField).remove();
                        $('#addWarehouseBtn').attr('style','display:none');
                    }
                    
                }
                
       
        
            
        }        
    
    
        
    
});
$(document).on('change','.warehouse',function(){
    divId= $('#addWarehouseBtn').parent('div').parent('div').parent('div').attr('id');
    $('div#'+divId).find('#'+warehouseField).find('input[type=hidden]').remove();
    $('div#'+divId).find("#"+warehouseField).append('<input type="hidden" name="warehouse[]" value="'+$(this).val()+'"/>');
});

$(document).on('click','.removeAdditionalWarehouse',function(){
    divId= $('#addWarehouseBtn').parent('div').parent('div').parent('div').attr('id');

    if ($(this).parent().find('.warehouse option').length <= 2) {
        warehouseField = $(this).parent().attr('id');
        $('div#'+divId).find("#"+warehouseField).find('.warehouse option').each(function() {
            if ( $('div#'+divId).find("#"+warehouseField).next('.row').length == 0) {
                index[''] = '';
            }
            index[$(this).val()] = $(this).text();
        });
       
        $('div#additionalWarehouse').find("#"+(warehouseField)).attr('id',--warehouseField);
    }
    else if (typeof $('#addWarehouseBtn').data('id') != "undefined" && editWarehouse == true) 
    {
        warehouseField=$('#addWarehouseBtn').data('id');
        $('div#'+divId).find("#"+warehouseField).find('.warehouse option').each(function() {
           
        
                if ($(this).val() == "") {
                    index[0] = ($(this).text());
                }
                else{
                    index[$(this).val()] = ($(this).text());
                }
            
            
        });
        $('div#additionalWarehouse').find("#"+(warehouseField)).attr('id',--warehouseField);
        editWarehouse=false;
    }
    else{
        warehouseField = $(this).parent().attr('id');
        $('div#'+divId).find("#"+warehouseField).find('.warehouse option').each(function() {
        
            index[$(this).val()] = $(this).text();
        });
        
        var ind = warehouseField;

        for (var id = (ind); id <= $(this).parent().siblings('.row').length +1; id++) {
            $('div#additionalWarehouse').find("#"+(++ind)).attr('id',id);
           
            
        }       

        warehouseField = ind-1;
       
    }
 
    $(this).parent().remove();

    $('#addWarehouseBtn').attr('style','display:""');
    
    if ($('#additionalWarehouse').children().length == 0) {
        
            warehouseField = 1;
          
            $('div#'+divId).find("#"+warehouseField).find('.warehouse').attr('disabled',false);
    }
    
    
    
    
 
});


$(document).on('click', "#editBrandsItem", function() {
    $(this).addClass('edit-item-trigger-clicked'); //useful for identifying which trigger was clicked and consequently grab data from the correct row and not the wrong one.

    var options = {
      'backdrop': 'static'
    };
    $('#editBrandsModal').modal(options)
  });

  // on modal show
  $('#editBrandsModal').on('show.bs.modal', function() {
    var el = $(".edit-item-trigger-clicked"); // See how its usefull right here? 
    var row = el.closest(".brand");

    // get the data
    var id = el.data('item-id');
    var name = row.children(".brand_name").text().trim();
   
    // fill the data in the input fields
    $('#brand_update_id').val(id);
    $("#edit_brands_name").val(name);
  });

  // on modal hide
  $('#editBrandsModal').on('hide.bs.modal', function() {
    $('.edit-item-trigger-clicked').removeClass('edit-item-trigger-clicked')
    $("#editBrandsItem").trigger("reset");
  });

  //bundle Products filter btn
var bundle_product_id =  new  Array;
var counterBundledProducts = 1;
var unit_price = 0;
  $(document).on('click','#filterProductsBtn',function(){
    var searchProduct = $('#filterProductsInput').val();
    if(bundle_product_id.length == 0 && $('#mappedProducts').val() != "")
    {
        
        bundle_product_id = ($('#mappedProducts').val().split(','));
        counterBundledProducts = bundle_product_id.length + 1;
        bundle_product_id.forEach(element => {
        unit_price += parseInt($('#unit_price_bundle'+element).text());
        });
        if (editArrVehicleId.length === 0) 
        {
            for (var index = 0; index < $('#editMappedVehicleList').find('li').length; index++) {
               
                editArrVehicleId.push($('#editMappedVehicleList').find('li#'+index+'').children('input').val());
                
            }
            
        }
        $('#input-product_unit_price').val(unit_price);
        
    }
    
    $.ajax({
        url:"/bundle/product/add/loadProducts",
        method:'POST',
        data:{parent:searchProduct,"_token": $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            $('#addedBundleProducts').slideUp();
            $('#loadBundleProducts').hide().html(data).slideDown('slow');
            
        }
    });

  });

  $(document).on('click','.select-all-bundle',function(){
    var checkAll = this.checked;
    $('input[type=checkbox]').each(function() {
        this.checked = checkAll;
    });
});

$(document).on('click','#mapProducts',function(){
    
    $.each($("input[name='bundle_products[]']:checked"), function(index){
        if(($.inArray($(this).val(),bundle_product_id))== -1)
        {
            bundle_product_id.push($(this).val());
            var checkboxVal = $(this).val();
            $(this).val('');
            
            $('#addedBundleProductsRows').append('<tr class="MoveableRow">'+$(this).closest('tr').html()+'</tr>');
            $('#addedBundleProducts').find("input[name='bundle_products[]']").closest('td').siblings('th').text(counterBundledProducts++);

            var total_price = $('#addedBundleProducts').find("input[name='bundle_products[]']").closest('td').siblings('td.total_price').text().trim();
            $('#addedBundleProducts').find("input[name='bundle_products[]']").closest('td').siblings('td.total_price').html('<input type="number" class="form-control" min="1"  value="'+total_price+'" />');
            unit_price = unit_price + parseInt($('#addedBundleProducts').find("input[name='bundle_products[]']").closest('td').siblings('td').last('td').text());
            $('#addedBundleProducts').find("input[name='bundle_products[]']").closest('tr').append('<td class="h3"><span class="up_button mr-1" style="cursor:pointer;"><i class="nav-icon i-Arrow-Up-in-Circle"></i></span><span class="down_button" style="cursor:pointer;"><i class="nav-icon i-Arrow-Down-in-Circle"></i></span></td><td><button type="button"  class="btn btn-danger removeAdditionalBundleProduct" data-id="'+checkboxVal+'">X</button></td>')
            $('#addedBundleProducts').find("input[name='bundle_products[]']").closest('td').remove();
            
            
        }
          
    });

   

    $.ajax({
        url:"/bundle/vehicle/mapped/request",
        method:'POST',
        data:{parent:bundle_product_id,"_token": $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
           
            $.each(data,function(index,vehicle) {
                
                var id = (vehicle.id).toString();
                if(($.inArray(id,arrVehicleId))== -1)
                {
                    $('#mappedVehicleList').fadeIn();
                    $('#mappedVehicleList').find('ul').append('<li class="list-group-item" id="'+index+'">'+ vehicle.make +' '+ vehicle.model +' '+ vehicle.variant_name +'<button type="button" class="float-right btn btn-danger removeMappedVehicle"><i class="nav-icon i-Close-Window"></i></button><input type="hidden" name="mapVehicleIds[]" value="'+vehicle.id+'"/></li>');
                    arrVehicleId.push(id);
                    $('#submitFinish').css('display','');
                   
                }

                if(($.inArray(id,editArrVehicleId))== -1)
                {
                    $('#editMappedVehicleList').fadeIn();
                    $('#editMappedVehicleList').find('ul').append('<li class="list-group-item" id="'+index+'">'+ vehicle.make +' '+ vehicle.model +' '+ vehicle.variant_name +'<button type="button" class="float-right btn btn-danger editRemoveMappedVehicle"><i class="nav-icon i-Close-Window"></i></button><input type="hidden" name="editmapVehicleIds[]" value="'+vehicle.id+'"/></li>');
                    editArrVehicleId.push(id);
                   
                }
            });
            
        }
    });
    $('#mappedProducts').val(bundle_product_id);
    $('#loadBundleProducts').slideUp();
    $('#addedBundleProducts').slideDown();
    $('#input-product_unit_price').val(unit_price);
    

});

$(document).on('click','.down_button', function () {
    var rowToMove = $(this).parents('tr.MoveableRow:first');
    var next = rowToMove.next('tr.MoveableRow')
    if (next.length == 1) { 
        if(bundle_product_id.length == 0 && $('#mappedProducts').val() != "")
        {
            
            bundle_product_id = ($('#mappedProducts').val().split(','));
            counterBundledProducts = bundle_product_id.length + 1;
            bundle_product_id.forEach(element => {
            unit_price += parseInt($('#unit_price_bundle'+element).text());
            });

            $('#input-product_unit_price').val(unit_price);
            
        }
        // array value move as pr row.
        var tempIndex = bundle_product_id[parseInt($(this).parent().parent().find('th').text()) - 1];
        bundle_product_id[parseInt($(this).parent().parent().find('th').text()) - 1] = bundle_product_id[parseInt($(this).parent().parent().find('th').text())];
        bundle_product_id[parseInt($(this).parent().parent().find('th').text())] = tempIndex;
        $('#mappedProducts').val(bundle_product_id);
        
        next.after(rowToMove);
        $('tr.MoveableRow').css('background-color','');
        $(this).parent().parent().css('background-color','lightgray');
        
        // sr.no
        var temp = $(this).parent().parent().find('th').text();
        $(this).parent().parent().find('th').text((next.find('th').text()))
        next.find('th').text(temp);
        
    }
});
 
$(document).on('click','.up_button', function () {
    var rowToMove = $(this).parents('tr.MoveableRow:first');
    var prev = rowToMove.prev('tr.MoveableRow')
    if (prev.length == 1) {
        if(bundle_product_id.length == 0 && $('#mappedProducts').val() != "")
        {
            
            bundle_product_id = ($('#mappedProducts').val().split(','));
            counterBundledProducts = bundle_product_id.length + 1;
            bundle_product_id.forEach(element => {
            unit_price += parseInt($('#unit_price_bundle'+element).text());
            });

            $('#input-product_unit_price').val(unit_price);
            
        }
         var tempIndex = bundle_product_id[parseInt($(this).parent().parent().find('th').text()) - 1];
        bundle_product_id[parseInt($(this).parent().parent().find('th').text()) - 1] = bundle_product_id[parseInt($(this).parent().parent().find('th').text()) - 2];
        bundle_product_id[parseInt($(this).parent().parent().find('th').text()) - 2] = tempIndex;
        $('#mappedProducts').val(bundle_product_id); 
        
         prev.before(rowToMove);
         $('tr.MoveableRow').css('background-color','');
        $(this).parent().parent().css('background-color','lightgray'); 
         //sr.no
         var temp = $(this).parent().parent().find('th').text();
         $(this).parent().parent().find('th').text((prev.find('th').text()))
         prev.find('th').text(temp);
    }
});

$(document).on('click','.removeAdditionalBundleProduct',function(){

    if(bundle_product_id.length == 0 && $('#mappedProducts').val() != "")
    {
        
        bundle_product_id = ($('#mappedProducts').val().split(','));
        counterBundledProducts = bundle_product_id.length + 1;
        bundle_product_id.forEach(element => {
        unit_price += parseInt($('#unit_price_bundle'+element).text());
        });

        $('#input-product_unit_price').val(unit_price);
        
    }

    var inputField = $(this).attr('data-id');
    bundle_product_id = $.grep(bundle_product_id,function(value)
    {
        return value != inputField;
    });
    
    unit_price -= parseInt($('#unit_price_bundle'+inputField).text());

    var rowToMove = $(this).parents('tr.MoveableRow:first');
    var next = rowToMove.next('tr.MoveableRow');
    while(next.length == 1)
    {      
            // sr.no
       var temp = $(this).parent().parent().find('th').text();
       $(this).parent().parent().find('th').text((next.find('th').text()))
       next.find('th').text(temp);
       next = next.next('tr.MoveableRow');
       
    }
   
   

    counterBundledProducts--;
    $('#mappedProducts').val(bundle_product_id);
    
    $('#input-product_unit_price').val(unit_price);
    $(this).closest('tr').remove();

});


// Affiliate Connection by admin
$(document).on('change','#requester_partner_type',function(){
    var partner_type = this.value;
    $.ajax({
        url:"/affiliate/get/partner/name",
        method:'POST',
        data:{parent:partner_type,dType:'req',"_token": $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            $('#loadRequestPartnerName').hide().html(data).fadeIn('slow');
        }
    });
});

$(document).on('change','#receiver_partner_type',function(){
    var partner_type = this.value;
    $.ajax({
        url:"/affiliate/get/partner/name",
        method:'POST',
        data:{parent:partner_type,dType:'rec',"_token": $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            $('#loadReceiverPartnerName').hide().html(data).fadeIn('slow');
        }
    });
});


$(document).on('click','#establishConnectionBtn',function(){

    var req = $('#partner_namereq').val();
    var rec = $('#partner_namerec').val();
    
    if (typeof rec != "undefined" && typeof req != "undefined") 
    {
        if (req != rec) {
            $('#loadingScreen').addClass('loadscreen');
            $('#loadingScreen').children('div').addClass('loader spinner-bubble spinner-bubble-primary');
            $.ajax({
                url:"/affiliate/establish/connection",
                method:'POST',
                data:{'req':req,'rec':rec,"_token": $('meta[name="csrf-token"]').attr('content')},
                success: function() {
                    window.setTimeout(function(){location.reload()},1000)
                }
            });
        }
        else{
            toastr.error(
                "Requester and receiver can not be same user", 
            {
                showMethod: "slideDown",
                hideMethod: "slideUp",
                timeOut: 2e3
            });
        }
        

    }
    else
    {
        toastr.error(
            "Please select all fields", 
        {
            showMethod: "slideDown",
            hideMethod: "slideUp",
            timeOut: 2e3
        });
    }
    
});


function rejectConnectionRequest(event,rejectId)
{
    event.stopPropagation();
    var id = rejectId;

    $.ajax({
        url:"/affiliate/reject/connection/"+id,
        method:'get',
        success: function() {
            window.setTimeout(function(){location.reload()},500)
        }
    });
}
    


function acceptConnectionRequest(event,acceptId)
{
    event.stopPropagation();
    var id = acceptId;

    $.ajax({
        url:"/affiliate/accept/connection/"+id,
        method:'get',
        success:function(data) {
            
            for (var index = 1; index < 11; index++) {
                
                $('#tier_price_receiver').append('<option value="tier_'+index+'">'+data[0]["tier_"+(index)]+'</option>');
            }
            
            var options = {
                'backdrop': 'static'
                };
                $('#selectTierPrice').modal(options);

                
        }
    });
}




  // on modal hide
  $('#selectTierPrice').on('hide.bs.modal', function() {
    
    window.setTimeout(function(){location.reload()},500)
  });

  $(document).on('click','#setPriceBtn',function(){
    var tier_price_label = $('#tier_price_receiver').val();
    if (tier_price_label != "") 
    {
        $.ajax({
            url:"/affiliate/set/connection/default/price",
            method:'post',
            data:{'id':tier_price_label,"_token": $('meta[name="csrf-token"]').attr('content')},
            success: function() {
                // add toastr 
                window.setTimeout(function(){location.reload()},500)
            }
        });
    }
  });


  function giveAccessProducts(event,accessId)  
  {
    event.stopPropagation();
    var id = accessId;
        
    $.ajax({
        url:"/affiliate/give/access/products/"+id,
        method:'get',
        success: function(data) {
            $('#connectionAffiliateRequest').fadeOut('fast');
           
            
            $('#loadAffiliateProducts').hide().html(data).fadeIn('slow');
            $('#zero_configuration_table').DataTable();
            
        }
       
    });
  }
       
        
    
    
    
    

$(document).on('click','.select-all-affiliate',function(){
    var checkAll = this.checked;
    $('input[type=checkbox]').each(function() {
        this.checked = checkAll;
    });
});

$(document).on('click','#giveAccessAffiliateProducts',function(){
    var connection_id = $(this).attr('data-id');
    var affiliate_product_id = new Array();
    $.each($("input[name='affiliate_products[]']:checked"), function(){
        if(($.inArray($(this).val(),affiliate_product_id))== -1)
        {
            affiliate_product_id.push($(this).val());
           
        }
    });

    if (affiliate_product_id.length > 0) 
    {
        $.ajax({
            url:"/affiliate/products/give/access/requester",
            method:'post',
            data:{'id':connection_id,'product_arr':affiliate_product_id,"_token": $('meta[name="csrf-token"]').attr('content')},
            success: function() {
                window.setTimeout(function(){location.reload()},500)
            }
        });
    }
    

});

$(document).on('click','.affiliateProductsRow',function(){
    var userID = $(this).data('id');
    window.location = "/affiliate/get/requester_details/"+userID;
});

